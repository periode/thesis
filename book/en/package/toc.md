# table of contents

<!-- ## notes

The toc does not so much talk about the content then about the structure (_the sequence of the research should not be the sequence of the story!_). __Readers stay with a book as long as it promises to answer still unresolved questions__ (sense of crescendo).

ToC should be about two-three pages, single-spaced. Follow chapter titles with two paragraphs (_tell, then support: being with the point the chapter is trying to make, or the question with which it will grapple, then run through the materials_). No more than 8 chapters, plus introduction and epilogue (no need to explain the intro or epilogue).

- the first identifies the point of the chapter
- the second highlighting the materials that the chapter will marshal
- maybe three if needed

some thoughts:

1. every chapter should have a title (background chapter? making-a-point chapter?)
2. complex treatment, simple structure
3. three types of chapters: background or context, argument, break-narrative chapters -> start by just doing narrative/argument chapters
4. embed the interesting details
5. many good books are _layered_: as they unfold, they reveal a more complex, layered view of their analysis (text -> math -> architecture)
6. chapters are beats, or rhythms

-->

## Introduction

We start by discussing the beauty of technical artefacts, and the kinds of aesthetic experiences that tools and machines can elicit. We continue by discussing the main components of our argument: source code, aesthetics and ethics. We describe how source code is written, and how it becomes software, using very simple examples. We introduce aesthetics as a set of experiences, ideals and judgments, and illustrate it with two source code snippets: one that is aesthetically pleasing, and one that is not. We introduce ethics as a normative judgment on how one should behave, and show how they are multiple ways of writing the exact same program, opening up a fertile ground for judging which one would be best.

## 1. (expository) Code, prose and poetry

Code is a very particular kind of text, one that can be written and read by a human, by a machine, or by both. This chapter considers source code as a literary endeavor, and discusses issues of readability, authoriality and semanticity. Treating code as a work of prose presents it under the light of clarity, simplicity, transparency. We discuss Donald Knuth's claim of _literate programming_, and Yukihiro Matsumoto's defence of _code as an essay_, and we argue that code can offer two specific kind of _episteme_: one that considers code as a discourse, and one as a dialogue. We analyze an extract from the source code of _Moodle_, a learning management system whose readability and clarity allows for its technical expansion and reflects a dialogical, collaborative mode of writing.

We complement this ideal of code as clear, readable prose with that of code as expressive poetry. Through two case studies of code poems and the aesthetic experiences they elicit, we show that source code can be valued beyond its functional performance, by _brininging concepts within the realm of the thinkable_, all the while subverting mainstream ideals of good writing and good software.

## 2. (background) Programming with style

Code is a kind of text written with specific tools: programming languages. After explaining how a programming language differs from a natural language, we highlight a first paradox: all languages are Turing-complete (meaning they can all perform any computational operation interchangeably), but each programming language decides to offer their own idiosyncratic syntax. We discuss the ideal of _expressiveness_, as the ease of representing complex concepts with elemental constructs, and relate it to the strive for _elegance_, and how they manifest themselves in two languages: Go and Ruby.

Programming languages enable different styles of writing. We discuss programming styles in terms of sociality (individual style vs. collective style) and worldmakings (doing the same thing in different ways). As a radically stylistic example, we discuss two _esoteric languages_, Malbolge and Cree#, and how they underscore an aesthetic experience of puzzle-solving, and the ethical ideal of productivity and reliability in traditional machine languages.

## 3. (expository) Between mathematics and engineering

As expressive as they might be, programming languages are first and foremost technical languages, drawing from a mathematical tradition applied to engineering purposes. We discuss the aesthetic experiences and judgments that are inherited from traditional mathematical (illustrated by the example of the Scheme interpreter) and engineering artefacts (illustrated by source code for embedded hardware). We discuss this relationship between beauty, science and engineering, its role as a heuristic and how it reframes elegance as _efficiency_, whether cognitive or structural. We show how one approach values ideas, while the other values reality.

We describe historical tension between whether programming is a science or is an art, and recontextualize it in the development of programming as a profession and the accompanying shift in gender representation. We discuss soft epistemologies and their replacement by hard epistemologies.

## 4. (expository) Patterns, cathedrals and bureaucratic mazes

This chapter considers program texts, written in source code, as arrangements of structures, states and flows. If source code is _written_, software is _built_: as the result of design and construction processes, it is similar to architecture. We discuss how programming builds computational spaces, and how those spaces have unfamiliar spatio-temporal properties. We draw on research on the psychology of programming to support this view of programmers navigating computational spaces, and illustrate it with the example of the search engine of the Yandex platform, and how it can be experienced as a kafkaesque bureaucratic space.

We discuss the two opposite architectural approaches to building software, and their ethical implications. First, we describe the top-down model of a cathedral, illustrated by Universal Modelling Language diagrams, and Le Corbusier's work on "the house as a machine to live in", showing how the focus is on the soundness and immutability of the structure. Second, we describe the bottom-up model of pattern-finding, illustrated by the PiVideoLooper software, and Christopher Alexander's "A Pattern Language", showing how the focus is on the evolution and growth of the structures, and on making program text habitable spaces.

## 5. (background) Hacks and crafts

There is aesthetic pleasure in making things, and software is no exception. Software builders indulge in acts of production in two contingent ways: hacking and crafting. We discuss the famous `fast_inv_sqrt` hack, a function to quickly calculate the inverse square root. Through a comparative analysis of its technical and economical function, we show that the ideal of the hack is a testimony to the primacy of the machine and a disregard for the human: the most interesting limit is not human cognition, it is how far one can work with machine computation. We reconnect the technical hacks of the inverse square root to its broader impact on real-time, three-dimensional computer graphics, and its constant strive for _speed_.

We discuss how both of these aesthetic experiences of making things cement code as a _material_. Presenting excerpts of the source code of `ffmpeg`, we discuss how crafting code reveals itself as an ideal of careful composition of the matter at hand, of pride in doing things without abstraction nor automation, and of thoroughly knowing and mastering one's material and tools. We discuss the specifics of doing something well, when this something is a crucial piece of the global entertainment infrastructure.

## 6. (expository) Automated collective writing

Programmers take pride in both writing detailed code, and in building abstractions, straddling the line between individual expression and large-scale collaboration. While the tool-building has been an essential part of a programmer's aesthetic experience, the recent introduction of large language models in programming practices presents a shift in perspective.

We discuss the properties of the latest generation of code-generating systems, and discuss how they blur the line between tool and machine, and between function and productivity. As an illustration, we discuss the code generated by a large language model for a standard _To-do app_, a staple of programming as productive activity. Drawing on another example, that of a labyrinthic 12-line ternary statement, we show how the use of large language models forfeits the aesthetic standard of understandability for that of productivity.

## Conclusion

We recall our demonstration that source code, as an expressive medium, can be judged along different standards. Is it written for humans first, or for machine first? Is it focused on ideas, or on implementations? Is it meant to be fixed forever, or constantly evolving? Is it an individual stylistic creation or a group effort? Should it be effective, or efficient? Each of these decisions have both immediate aesthetic and ethical consequences when it comes to reading and writing source code, and transpire through software to affect our digital lives.
