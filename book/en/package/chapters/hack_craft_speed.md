# Hacks and crafts

<!--

12,500 words (20-ish pages)

## themes

Investigating the dual aesthetic ideals of the craftsman and the hacker reveal a gradient between the human and the machine, which will nonetheless bend towards the functional ideal of _speed_, aligning with the properties of the machine. Through the examples of two specific algorithms, we show how the aesthetic ideals and practices of software developers can have broader societal consequences in terms of _acceleration_.

And yet, circling around this mechanical property of speed also reveals some very human concerns of honesty, knowledge and collaboration, complicating this picture of a strictly machinic aesthetic.

## structure

What we're going to show here is how aesthetic ideals and judgments has, paradoxically, machine speed as an end, and human ingenuity as a means.

-->

## 1 - The Real Programmer and His Handmade Code - 3.5k

<!--
The first part sets up the scene: what do we intend by aesthetic ideal? What specific practices might strive for these ideals? Those practices can be understood in terms of experiences (of making a thing) and judgement (of evaluating a thing). -->

Software is written using symbols which trigger particular electronic configurations in the computer (e.g. verify a password login, synchronize email, change what is shown on the screen, save a file, play some music). However, programmers do not directly decide on those changes of electrical voltage. They use programming languages, technical languages made up of symbols which offer different levels of abstraction.

The symbols with the least abstraction are the 0s and 1s, directly representing variations in electrical voltage. These are then abstracted into bytes, groups of 0s and 1s that can represent numbers. For instance, `00001100` represents the number `12`. Each of these bytes correspond to pre-existing command the computer knows about (e.g. _add_, _write_, _copy_, etc.). These bytes are then abstracted into words, such as `ADD`, `SUB`, `JMP`, and this layer constitutes a family of languages called _Assembly_. One level up in abstraction brings us to languages like C, where one can write simple programs displaying a one-line message on a screen, like `printf("Hello, World!\n");`, all the way to very complex programs such as operating systems. Specific programs, called _compilers_, take on the task of translating something like `printf("Hello, World!\n");` all the way to its binary representation, in order to trigger a series of electrical circuits, ultimately re-arranging the pixels on the screen to show "Hello, World!".

Whether one should work at a relatively high-level of abstraction, or at a relatively low-level, has been a source of debate among programmers ever since the advent of the profession. While high-level programming is often considered more expressive, and more in tune with the broad concepts represented and manipulated by a program (a song, a user account, a notification), low-level programming is more precise, and more explicit about the realities of the computer's workings (bits, memory and operations). In this chapter, we zoom into the innards of the machine, and we consider the ideals and practices of writing low-level source code, looking both at _what_ programmers strives for, and at _how_ they do it, and _why_ it impacts us, the end-users.

### The story of Mel and programmer lore - 1.5k

<!-- Talking about the story, talking about the demographics of programmers, talking about other pieces of programmer lore which might relate to this (python zen, "make it work, make it correct, make it fast"), something about resources and scarcity (to talk about streaming later) -->

Written and published in 1983 on a Usenet board, the ancestors of Web forums, _The story of Mel, the Real Programmer_ recounts the tale of Mel Kaye, an individual who wrote software on the 1959 ACT-1 compiler and has become a recurring reference in programmer lore. The story focuses on Kaye’s tenure at the Royal McBee computer company, and his role in writing a program which would allow potential customers to play blackjack with the computer at trade shows, a marketing stunt meant to sell Royal MacBee's RPC-4000 computer. ability to write both excellently efficient and completely inscrutable code. Usually, code is expected to serve a dual purpose: it should be written in such a way that the computer can execute, and that software can perform its function, but it should also be written in such a way that humans can read and understand it. This was not the case of Mel's code. At a time when programmers were starting to move on from switching individual bits, and towards symbols that borrowed from English syntax, he preferred to write code in his own style.  In the words of Ed Nather, who first posted the tale a few decades later, it was"_raw, unadorned, inscrutable hexadecimal code_". It would have looked something like this:

```hex
e4 d9 01
d1 23 09
99 f4 ff
e4 d9 01
d1 23 09
99 f4 ff
e4 d9 01
d1 23 09
99 f4 ff
e4 d9 01
d1 23 09
99 f4 ff
e4 d9 01
d1 23 09
99 f4 ff
```

It was an obscure series of numbers, each indicating a particular action, or a particular value stored in a particular place in computer memory. Nather, who was his colleague at the time, does not hide his admiration. This was a "_Real Programmer_", one which did not rely on convenient abstractions nor on comfortable automations. One who eschewed compiler and other technical aids to assemble by hand the right series of numbers which would optimize the cycle of calculations done by the machine.

> I have often felt that programming is an art form, whose real value can only be appreciated by another versed in the same arcane art; there are lovely gems and brilliant coups hidden from human view and admiration, sometimes forever, by the very nature of the process. You can learn a lot about an individual just by reading through his code, even in hexadecimal. Mel was, I think, an unsung genius.

So what is it that made Mel's arcane work so fascinating to his peers? How could one appreciate "gems and brilliant coups hidden from view"? The answer lies in his perceived commitment to a particular kind of excellence.

First of all, Mel knows a lot about the machine that he is working with. The RCP-4000 was built at a time when hardware and software was hardly separable (today, it matters very little whether the underlying machine that a software runs on is a MacBook Air, a Lenovo Thinkpad, or a Dell Aspire). In the case of the RCP-4000, it would store information for a program on a drum roll cylinder, which would rotate to access the next instruction that the computer has to execute.

The RCP-4000 could be programmed using specific numbers, called _opcodes_ which would instruct this specific machine to do something in particular, such as reading some information from the memory storage, or switching to another instruction under a particular condition. Such opcodes would be chosen somewhat arbitrarily by each of the manufacturers, and programmers would often need to compose their programs by constantly perusing the reference manual that came with the machine that they were working on. It seems that Mel knew all of these opcodes by heart, and this familiarity allowed him to compose programs which fit to the machine's ways of functioning, instructing it at the right time to do the right thing. There was both theoretical knowledge and technical skills, applied in a way that mastery always is.

Furthermore, Mel would treat parts of his programs as if they were their own opcodes, referring to them as numbers rather than as words. It might seem at first more complicated, writing for instance `4f` instead of `INPUT`, but such a choice is consistent with Mel's familiarity with hexadecimal notation, and its desire to write efficient software—for a computer, numbers are faster than words.

As a result, Mel's code becomes particularly efficient, and particularly inscrutable, but his co-worker can only trust what he is doing, witnessing the efficiency of the running program, as an overwhelming testament to Mel's know-how.

One could master a skill, meaning that one might be able to perfectly apply what to do and how to do it in any given sitation. It takes a different kind of skill to be able to draw on that knowledge to perform the unexpected, to go beyond the boundaries of the known system in order to push the limits of what it is able to do. The success of the story of Mel comes from this capacity at performing the unexpected.

One of the basic things of programming is telling when a program is supposed to stop. In a blackjack program, there might be a set of instructions which repeatedly draw cards to be dealt to the user. In order for the game to proceed, this repeated draw needs to stop, at some point. Otherwise, the program would deal fifty-four cards to the player and, once there are no more cards to draw, would crash. But Mel's program never explicitly specified when such repeated actions should stop, much to his coworker's wonder. How could a program run so well, if it did not respect the most basic of these requirements?

This was Mel's stroke of genius. He used the physical properties of the cylinder on which the instructions were stored wrap around back to the beginning of the instructions list, rather than remaining stuck at its end. This was nowhere documented in the reference manual, and yet this was technically feasible, exploiting the unique features of Mel's medium, the RCP-4000, in order to make his program more efficient. This was Mel's great hack.

Fourty years later, Mel's story is still regularly quoted in programming circles: _Real Programmer_ is now a slang term, featuring in the esteemed _Jargon File_, an almanach of hacker stories and anecdotes around hacker culture. It refers to the kind of programmer who rejoices in the challenge of a complex problem, in the elegance of the solution, and in its efficiency. The product and its users are secondary, if considered at all. Mel is hailed as a model of expertise and efficiency, tinted with nostalgia of a time when the programmer did not face many layers of indirections betwen them and what their code did.

The story of Mel epitomizes two central ideals for programmers. The first is a conception of one's practice as a kind of _craft_. One should aim at writing code that is fitting to its material, crafting programs in ways that are best adapted to the technical environment in which such programs will be executed. The second is the ability to display instrumental excellence by deploying clever _hacks_. One can also excel at writing code by going beyond mere fitness and compliance by pushing the medium's limits, hacking conventions to achieve ever-better performance, at the cost of being the only one able to understand what is going on.

### Crafting hacks - 2k

<!--

Background on programming, historical for both hacks and crafts

Basically, at the beginning there isn't much in terms of organization, so it's really quite hackish

Then, once it gets organized, and industrialized, the reference to craft becomes more popular

 -->

Mel's story, and the emphasis on his knowledge of the machine, his attention to detail, and his creative technical undertakinds, show how crafting code and hacking code have historically been two of the dominant approaches programmers have to what it is that they are creating.

Computer programming came to be as an offshoot of computer science, perhaps best illustrated by the collaboration of Charles Babbage and Ada Lovelace on The Analytical Engine, the prototype of the modern computer. With Babbage acting as the overall designer, Lovelace was key in practically implementing some of the mathematical formulas which The Analytical Engine was built to solve. This organisation distinguished between design and conception on one side, and implementation and practice on the other side. These two approaches are present since the early days of programming, from 1950s until the 1970s, with programmers being clearly distinct from computer scientists, as told by Nathan Esmenger in his history of the profession. Programmers would rather write code on a terminal than write algorithms on a piece of paper or a blackboard; despite being trained in the sciences, they would rather lean towards experimental tinkering rather than sound conceptualization.

Martin Campbell-Kelly traces the development of a discipline through an economic and a technological lens, and he identifies three consecutive waves in the production of software (campbell-kelly_airline_2003). Starting in the 1950s, and continuing throughout the 1960s, software developers were contractors hired to work directly with a specific hardware. These mainframes were large, expensive, and rigid machines, requiring platform-specific knowledge of the corresponding Assembly instruction set, the only programming language available at the time. Two distinct groups of people were involved in the operationalization of such machine: electrical engineers, tasked with designing hardware, and programmers, tasked with implementing the software. While the former historically received the most attention (ross_personal_1986), the latter was mostly composed of women and, in a patriarchal society, not considered essential in the process (light_when_1999). Furthermore, programming remained hardware-dependent. As Edsger Dijkstra puts it:

> But most important of all, the programmer himself had a very modest view of his own work: his work derived all its significance from the existence of that wonderful machine. Because that was a unique machine, he knew only too well that his programs had only local significance and also, because it was patently obvious that this machine would have a limited lifetime, he knew that very little of his work would have a lasting value. Finally, there is yet another circumstance that had a profound influence on the programmer's attitude to his work: on the one hand, besides being unreliable, his machine was usually too slow and its memory was usually too small, i.e. he was faced with a pinching shoe, while on the other hand its usually somewhat queer order code would cater for the most unexpected constructions. And in those days many a clever programmer derived an immense intellectual satisfaction from the cunning tricks by means of which he contrived to squeeze the impossible into the constraints of his equipment.

Those cunning tricks bringing intellectual satisfaction are hacks. To hack, in the broadest sense, is to enthusiastically inquire about the possibilities of exploitation of technical systems. Computer hacking specifically came to proeminence as early computers started to become available in north-american universities, and coalesced around the Massachussets Institute of Technology's Tech Model Railroad Club (levy_hackers_2010). Computer hackers were at the time skilled and highly-passionate individuals, with an autotelic inclination to computer systems: these systems mattered most when they referenced themselves, instead of interfacing with a given problem domain. Early hackers were often self-taught, learning to tinker with computers while still in high-school (lammers_programmers_1986), and as such tend to exhibit a radical position towards expertise: skill and knowledge aren't derived from academic degrees or credentials, but rather from concrete ability and practical efficacy (a meritocratic stance which has been analyzed in further in  (coleman_aesthetics_2018)).

In particular, this group of computer enthusiasts described as hackers developed organizational features similar to their historical counterparts: work was being done on distinct topics and fields in different geographic locations (Stanford, MIT, Bell Labs) (raymond_cathedral_2001), emphasis was put on engagement with tools, inquiring into peers' work (levy_hackers_2010) and later formalized into bottom-up archives, the most famous of which is the Jargon File, later to be published as the The New Hacker's Dictionary. Additionally, little accountability was required when it came to design explicitness: hacking away meant figuring things out along the way.

The histories of hacking and of software development are deeply intertwined: some of the early hackers worked on software engineering projects—such as the graduate students who wrote the Apollo Guidance Computer programs under Margaret Hamilton, programs which participated in landing the first humans on the Moon—and then went on to profoundly shape computer infrastructure. Particularly, the development of the UNIX operating system by Dennis Ritchie and Ken Thompson is a key link in connecting hacker practices and professional ones. Generally speaking, an operating system is the layer of abstraction which allows a programmer to ignore the specifics of the machine they are working on, would shift the focus of the programmer from hardware to software, and solved the issue of portability—having a program run unmodified on different machines Developed from 1969 at Bell Labs, AT&T's research division, the UNIX operating system was a product at the intersection of corporate and hacker culture, built by a small team, circulating along more or less legal channels, and spreading its design philosophy of clear, modular, simple and transparent design across programming communities (raymond_art_2003), and ultimately reshaping the profession of programming by liberating it from hardware.

In the 1960s, hardware switched from vacuum tubes to transistors and from magnetic core memory to semiconductor memory, making them faster and more capable to handle complex operations.  On the software side, the development of operating systems as well as of higher-level programming languages, such as FORTRAN, LISP and COBOL, addressed the problem of expressivity—expressing a program text in a high-level, English-like syntax, rather than Assembly instruction codes, or even hexadecimal numbers, as Mel did. Programmers were no longer tied to a specific machine, could tackle more ambitious problems, and therefore acquired a certain autonomy, a recognition which culminates in the naming of the field of _software engineering_.

The formalization of the field of programming in the mid-1970s kickstarted inquiries into the relationship between craftsmanship and programming started to take place in the mid-1970s from an educational perspective (dijkstra_craftsman_1982), from an organizational perspective (brooks_mythical_1975) and an inter-personal perspective (weinberg_psychology_1998), and culminated with the publication of several trade books (martin_clean_2008,hendrickson_software_2002), explicitly connecting the craft of programming with previous craft activities, and emphasizing the need for intrinsic motivation and the aim of a job well-done (hoover_apprenticeship_2009,goodliffe_code_2007).

As the profession of software engineering became more industrialized, commercialized, and thus standardized, the specter of alienation was looming on programmers. The need to churn out code for start-ups in the midst of various hype cycles of consumerism gave rise to the derogatory nickname of _code monkey_, one whose professional purpose is to type away at one's keyboard, devoid of pleasure or creativity. Considering programmers as craftspeople, 25 years after Dijkstra first asked whether a programmer was a a craftsman or a scientist:

> For many a "puzzle-minded" virtuoso coder of the early sixties, the scientific development of the last decade has been most unwelcome. He feels like the medieval painter that could create a masterpiece whenever his experience enabled him to render proportion well, who suddenly found himself overtaken by all sorts of youngsters, pupils of Albrecht Dürer and the like, who had been taught the mathematical constructions that were guaranteed to surpass his most successful, but intuitive renderings.

In the 1980s, an economic downturn of the IT industry was brought about by the abundance of buggy and broken software. The answer to this mismatch between the need to produce more software and the lack of existing guidelines to produce better software. Structured programming involved setting best practices and standards for the writing of source code within an explicitly commercial context. The further development of this commercial context, illustrated by the golden professional prospects of software engineers, and the rise of the technology industry as a driver of economic growth, also had some drawbacks. While Henry Ford standardized labor to fit the processes of machines on the assembly line, structured programming, along with forcing a certain kind of reliance became a new sort of project management through the disciplining of processes and know-hows.

Escaping the alienation of commercial exploitation involves reconnecting with the processes of creation, and reconstructing an identity of detailed crafting, rather than standardized production.

## 2 - Handcrafted assemblies - 3.5k

Now that we've looked at the social and imaginary aspect to craftsmanship, let's dive a bit closer to what exactly looks like, first for craftsmanship in the context of commercial/industrial software.

### The case of ffmpeg - 1.5k

We first explain ffmpeg, where it comes from, what it does.

We discuss languages as tools, assembler as the lowest languages, the physical moving of memory areas, the manipulations of streams of data.

We discuss the ambiguity between craft and industry

### Knowing one's material - 2k

<!--

We discuss what these tools work with: is it concepts? is it implementations? in the case of crafting, it involves implementation, languages, data streams.

 -->

Craftsmanship in our contemporary discourse seems most tied to a retrospective approach: it is often qualified as that which was _before_ manufacture, and the mechanical automation of production (thompson_study_1934), preferring materials and context to technological automation. Following Sennett's work on craftsmanship as a cultural practice, we will use his definition of craftsmanship as _hand-held, tool-based, intrinsically-motivated work which produces functional artefacts, and in the process of which is held the possibility for unique mistakes_ (Sennett, 2009).

At the heart of knowledge transmission and acquisition of the craftsman stands the _practice_, and inherent in the practice is the _good practice_, the one leading to a beautiful result. The story of Mel, and the pride of the FFMPEG developers hints at an appreciation of code beyond its formalisms and rationalisms, and towards its materiality.

A traditional perspective is that motor skills, with dexterity, care and experience, are an essential feature of a craftsman's ability to realize something beautiful (Osborne, 1977), along with self-assigned standards of quality (Pye, Sennett). These qualitative standards which, when pushed to their extreme, result in a craftsperson's _style_, gained through practice and experience, rather than by explicit measurements (Pye, 2008). See Pye's account of craftsmanship, and his intent to make explicit the question of quality craftsmanship and _"answer factually rather than with a series of emotive noises such as protagonists of craftsmanship have too often made instead of answering it."_ (Pye, 2008). Two things are concerned here, supporting the final result: tools and materials. According to Pye, a craftsperson should have a deep, implicit knowledge of both, what they use to manipulate (chisels, hammers, ovens, etc.) as well as what they manipulate (stone, wood, steel, etc).

The knowledge that the craftsman derives, while being tacit, is directed at its tools, its materials, and the function ascribed to the artefact being constructed, and such knowledge is derived from a direct engagement with the first two, and a constant relation to the third. Finally, any aesthetic decoration is here to attest to the care and engagement of the individual in what is being constructed—its dwelling, in Heideggerian terms.

This relationship to tools and materials is expected to have a relationship to _the hand_, and at first seems to exclude the keyboard-based practice of programming. But even within a world in which automated machines have replaced hand-held tools, Osborne writes:

> In modern machine production judgement, experience, ingenuity, dexterity, artistry, skill are all concentrated in the programming before actual production starts.  (Osborne, 1977)

He opens here up a solution to the paradox of the hand-made and the computer-automated, as programming emerges from the latter as a new skill. This very rise of automation has been criticized for the rise of a Osborne's "soulless society" (Osborne 1977), and has triggered debates about authorship, creativity and humanity at the cross-roads between artificial intelligence and artistic practice. One avenue out of this debate is human-machine cooperation, first envisioned by Licklider and proposed throughout the development of Human-Computer Interaction. If machines, more and more driven by computing systems, have replaced traditional craftsmanship's skills and dexterity, this replacement can nonetheless suggest programming as a distinctly 21st-century craftsmanship, as well as other forms of cratsmanship-based work in an information economy.

Beautiful code, code well-written, is an integral part of software craftsmanship. More than just function for itself, code among programmers is held to certain standards which turn out to hold another relationship with traditional craftsmanship—specifically, a different take on form following function.

A craftsman's material consciousness is recognized by the anthropomorphic qualities ascribed by the craftsman to the material, the personalities and qualities that are being ascribed to it beyond the immediate one it posseses. Clean code, elegant code, are indicators not just of the awareness of code as a raw material that should be worked on, but also of the necessities for code to exist in a social world, echoing Scruton's analysis that architectural aesthetics cannot be decoupled from a social sense: _"it is the aesthetic sense which can transform the architetct's task from the blind pursuit of an uncomprehended function into a true exercise of practical common sense._" (Scruton, 2013). As software craftsmen assemble in loose hierarchies to construct software, the aesthetic standard is _the respect of others_, as mentioned in computer science textbooks (Abelson & Sussman, 1979).

Another unique feature of software craftsmanship is its blending between tools and material: code, indeed, is both. This is, for instance, represented at its extreme by languages like LISP, in which functions and data are treated in the same way. In that sense, source code is a material which can be almost seamlessly converted from information to information-_processing, and vice-versa; code as a material is perhaps the only non-finite material that craftspeople can work with—along with words (this disregards the impact of programming languages, the hardware they run on, and the data they process on the environment).

Code, from the perspective of craft, is not just an overarching, theoretical concept which can only be reckoned with in the abstract, but also the very material foundation from which the reality of software craftsmanship evolves. An analysis of computing phenomena, from software studies to platform studies, should therefore take into account the close relationship to their material that software developers can have. As Fred Brooks put it,

> The programmer, like the poet, works only slightly removed from pure thought-stuff. He builds his castles in the air, from air, creating by exertion of the imagination. Few media of creation are so flexible, so easy to polish and rework, so readily capable of realizing grand conceptual structures. (Brooks, 1975)

So while there are arguments for developing a more rigorous, engineering conception of software development (ensmenger_computer_2012), a crafts ethos based on a materiality of code holds some implications both for programmers and for society at large: engagement with code-as-material opens up possibilities for the acknowledgement of a different moral standard\footnote{Writing about resilient web development, Jeremy Keith echoes this need for material honesty: "_The world of architecture has accrued its own set of design values over the years. One of those values is the principle of material honesty. One material should not be used as a substitute for another. Otherwise the end result is deceptive_.". As Pye puts it:

> [...] the quality of the result is clear evidence of competence and assurance, and it is an ingredient of civilization to be continually faced with that evidence, even if it is taken for granted and unremarked. (Pye, 2008)

Code well-done is a display of excellence, in a discipline in which excellence has not been made explicit. If most commentators on the history of craftsmanship lament the disappearance of a better, long-gone way of doing things, before computers came to automate everything, locating software as a contemporary iteration of the age-old ethos of craftsmanship nonetheless situates it in a longer tradition of intuitive, concrete creation.

<!-- the relationship with industrial production -->

A key aspect of the craftsmanship of this time is the relationship that they maintained with explicit, formalized standards. While various crafts did include specific lexical fields to describe the details of their trade (bassett_craftsman_2008), usually compiled into glossaries, the standards for quality were less explicit. Cennino Cennini, in his \emph{Libro dell'arte}, one of the first codexes to map out technical know-how necessary to a painter in the early Renaissance, lays out both practical advice on specific painting techniques, but does not explicitly lay out how to make something \emph{good} (cennini_craftsmans_2012). Further work, at the eve of the Industrial Revolution, continued on this intent to formalize the practice of craftsmanship (pannabecker_diderot_1994). In this sense, quality work is rooted in implicit knowledge: a good craftsman knows a quality work when they see it (sennett_craftsman_2009).

Another component of craftsmanship is its alleged incompatibility with manufacture (ruskin_seven_1920),sturt_wheelwrights_1963}. However, studies have shown that the craftsmanship, rather than standing at the strict opposite of the industrial (jones_reckoning_2016), has been integrated into the process of modern industrialization. The practice of the craftsman, then, integrates into the design and operation of machines and industrial-scale organizations, informing ways of making in our contemporary world (gordon_who_1988,mcgee_craftsmanship_1999).

## 3 - The fast inverse square root - 3.5k

An extreme case of craftsmanship takes place in hacking: it's no longer about knowing one's material, it's about _exploiting it_ in clever, insightful, ingenious ways.

### The case of the inverse square root - 1.5k

Again, context, explanation

### Breaking limits - 2k

> HACKER [originally, someone who makes furniture with an axe] n. 1. A person who enjoys learning the details of programming systems and how to stretch their capabilities, as opposed to most users who prefer to learn only the minimum necessary. 2. One who programs enthusiastically, or who enjoys programming rather than just theorizing about programming (Dourish, 1988)

The compression of abstraction and concreteness, the traces of human commentaries, the circulation of information, code, and science.

## 4 - Programming in a flash 3k

From aesthetics to poetics, from _what_ you create to _how_ you create.

### Material speed - 1.5k

The computer is only useful if it's fast, the computer operates at the speed of electricity

The need for optimization and benchmarks, as software becomes more and more commercial (from _reliable_ to _fast_)

### Social speed 1.5k

The human applications running in computer time do not run in human time.
