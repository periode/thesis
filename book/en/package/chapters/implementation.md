# sample chapter - implementation: from mind to matter

## themes

this chapter focuses on the stretch that software has to do between the world of ideas and the reality of the machines

about implementation, from ideas to reality, between tangible and intangible, poetry as the thing that renders thinkable

poetry as the bringing into prose of complex ideas
architecture as the wicked problem of implementation
mathematics and the beautiful proof as a bridge between starting point and result?

## examples

- code poems (ruby, black perl)
- scheme interpreter
- fast inverse square root (merging ideas and reality)

## structure

### Code poetry, and making thinkable

first start with the example of the code poem, and of code poetry, and discuss how poetry brings ideas into the realm of the thinkable (5p)

### Beautiful entities, beautiful proofs

however, this manifestation of ideas is not the exclusivity of poetry: we also find it in mathematics, in the way that a theorem (an idea) is formulated into different proofs (a reality). to support this point, we show two things: first, the scheme interpreter and the parallel with the maxwell equations. second, the process of refactoring (of making it look nice) using aesthetics towards truth.

Conclude on the fact that implementation is always uglier in reality. A Lisp interpreter is tautological, it is of no use except to interpret itself, but it is elegant

this is also the problem of architecture, insofar as it can help us discuss the problem of __implementation__

### The collusion of all

Beautiful code is able juggle between all of these requirements. we conclude on the combination of idea and reality: the fast inverse square root.

maybe this is not the best example here. maybe works better in the other chapter
