# sample chapter - craft and automation

12,500 words (20-ish pages)

## themes

looking at the aesthetic ideal of the craft and of the hack, we highlight the underlying existence of code as material whose main properties include _speed_.

__main argument__: aesthetics depend on tools and on materials, and on the understanding of both. There is an ambivalent relationship between craft and automation: building your own tools to automate your work.

__sub arguments__: tools can also hold particular values, such as efficiency, standardization: "the aesthetic of the ingenieur"? Tools and craft ask questions about what is the human part in the machine vs. the machine part in the human

This chapter is about the technical interfaces (the tools, including languages) which affect our perception of reading and writing code, which entails technology-assisted cognition (leroi-gourhan)

(it has a good angle for developing tension), relationship to tools and to style, IDEs and LLMs

## structure

### Tool-assisted aesthetic experience (6p - 3.5k)

<!-- 

This subchapters sets out what we mean by aesthetic experience. It introduces to concept: tool-assisted aesthetic experience, and surface-level vs. depth level experience

-->

#### Aesthetic experience (1k)

<!-- this part disambiguates what we mean by art, and sets up the approach to craft -->

Programming has been considered an art since the 1970s, which coincide with the evincing of women from the profession, when it moved from the (less-valued) handwork towards the (more-valued) headwork, as per the words of Nathan Ensmenger. In 1974, Donald Knuth begins the publication of what would become his major work, _The Art of Computer Programming_. Out of the six hundred pages of the first volume, only the introduction is dedicated to discussing programming as an art; far from laying out an extensive aesthetic theory, Knuth nonetheless uses this device of "programming-as-art" as a means to suggest that, while it might not be a science, it should still be taken seriously. We follow his advice here, and start from the assumption that programming, and the source code resulting from it, can be sources of aesthetic experience.

When we talk about aesthetic experience, we do not talk about art, and much less about fine arts. In the words of John Dewey, the aesthetic experience is that which elicits a particular pleasure, engagement or satisfaction in the engagement with a particular kind of creation, from a bouquet to a engine or a neatly-arranged folder. This interest in the every, in the ordinary, allows us to highlight what are the particular _stances_ that someone might take with respect to a human creation.

Could programming-as-art be a shorthand to suggest that the results of this practice—source code, or program texts—might be able to elicit an aesthetic experience? Indeed, high art is the only form of art. We can go back to the 15th century to see the beginning of a schism between low-brow and high-brow creation take place. The arts, then connected to the _artesan_, would encompass any skillful (and one might say technical) realisation into an external object, an _artefact_.

<!-- craft -->

#### Layout and readability (1k)

Talking about the IDE, the colors, the psychology of programming and the need to comprehend. From handwork to headwork.

__Plot-twist__: tool-assisted beauty has been a thing since IDEs (beautify, uglify). Brings in the question of __obfuscation__ (2p), (IOCCC -> underhanded C code)

#### Models all the way down

We move from surface-level to deep-level, from the flatness of code to its depth.

### Personal styles and machine styles (6p - 3.5k)

Now let's move from the surface level to the deeper level: how programming languages have a style (java bureaucracy vs. ruby community vs. the deviance of malbolge)

__include?__ ugly ternary spaghetti of LLMs (could involve a re-write of that ternary)

#### Programming language styles

And yet, LLMs also hint at languages being tools, and therefore opens up a discussion and a tension between rigorous machine languages and contextual natural languages (wittgenstein). So one of the points of making beautiful things in programming is to navigate between machine understanding and human understanding. (3p)

#### Automated style

Working alone, working in groups. From linters to LLMs

### Crafted code, fast software and black boxes (6p - 3.5k w)

<!-- knowing what you work with helps you reach the pinnacle of software: being fast. this should probably be in the last part: the material of source code is computation (memory+processing)-->

Software is written using symbols which activate particular electronic configurations in the computer (e.g. verify a password login, synchronize email, change what is shown on the screen, save a file). These symbols operate along a main axis of _abstraction_. The symbols with the least abstraction are the 0s and 1s representing variations in electrical voltage. These are then abstracted into bytes, groups of 0s and 1s that can represent numbers (e.g. `00001100` represents the number `12`). Each of these bytes correspond to pre-existing command the computer knows about (e.g. _add_, _write_, _copy_, etc.). These bytes are then abstracted into words, such as `ADD`, `SUB`, `JMP`, and this layer constitutes a family of languages called _Assembly_. One level up in abstraction brings us to languages like C, where one can write things like `printf("Hello, World\n");` all the way to the MacOS operating system. In computer programming, languages are tools. These are tools which can all do the same thing, since they all work with the same material of electronic circuits, and yet all do it differently. Appreciating a piece of code, as we show in this chapter, is intrinsically linked to the materials and the tools of the trade.

Written and published in 1983 on a Usenet board, _The story of Mel, the Real Programmer_ recounts the tale of Mel Kaye, an individual who wrote software on the 1959 ACT-1 compiler and has become a recurring reference in programmer’s lore. The story focuses on Kaye’s ability to write both excellently efficient and completely inscrutable code, extremely fast, extremely optimized, knowing by heart all the inner workings of the machine. The software that Mel was writing in the story was a simple blackjack game, made to impress potential customers in the code that Mel wrote would have looked something like this:

```txt
55
48 89 e5
c7 45 f4 0a 00 00 00
c7 45 f8 02 00 00 00
8b 45 f4
2b 45 f8
89 45 fc
b8 00 00 00 00
5d
c3
```

While his colleagues would have written something like this:

```asm
push   %rbp
mov    %rsp,%rbp
movl   $0xa,-0xc(%rbp)
movl   $0x2,-0x8(%rbp)
mov    -0xc(%rbp),%eax
sub    -0x8(%rbp),%eax
mov    %eax,-0x4(%rbp)
mov    $0x0,%eax
pop    %rbp
ret
```

Such a story, like every piece of foklfore, is as much apocryphal as it is illustrative: Mel does great things with a proto-computer language, eschewing the use of any letters, in order to write the kinds of instructions which only him and the computer can understand. He does great things because he is highly-skilled, and this skill creates awe and bewilderement in his peers. In the words of Ed Nather, who first posted the tale:

> I have often felt that programming is an art form,
> whose real value can only be appreciated
> by another versed in the same arcane art;
> there are lovely gems and brilliant coups
> hidden from human view and admiration, sometimes forever,
> by the very nature of the process.
> You can learn a lot about an individual
> just by reading through his code,
> even in hexadecimal.
> Mel was, I think, an unsung genius.

There is a particular kind of aesthetic standard which we can devise here. That which requires skill to execute, and to comprehend, is awe-inspiring. It is an example of _aesthetic of technics_.

<!--

    talk about how much mel knew his tool and his material

    but what is the material? talk about sennett and craft

    and about how much the tradeoff between effectiveness and expressiveness
    
-->

Time has passed ever since Mel wrote software by using only numerical constants (today, software is usually written with letters as well). Code which only its writer can read, considered as model programming work and informing ideals of programmers, slowly began to phase out with commercial software. Such a depiction of genius might therefore be valued in artistic cricle, where avant-garde thinking has been praised and renewed through figures such as Picasso, John Cage or Andrej Tarkovski. And yet, the story of Mel is understood today as a cautionary tale, one in which ego and hubris cannot be redeemed by skill alone. Software, like most objects around us, have a social dimension, they are made by people, used by people, and maintained by people. It is on this level of maintenance that Mel fails, as his code is so obscure that no one can understand it, nor dares to touch it. If anything happens to Mel, all of this work might as well be thrown out the window (or at least deleted from the computer memory) and start anew. Still, 30 years later, the story still fascinate

An other underlying aspect is the _need for speed_: Mel is impressive not because he can make the computer do things (since Turing, we know that the computer can have near-universal applications), but because he can make the computer do things _fast_. This is the trifecta of creating software: making it work, making it fast, making it nice.

Then we look at a real-life example of FFMPEG assembly: hand-crafted code. (2p). FFMPEG is an audio and video converted. Or rather, it is _the_ audio and video converter. Every fragment of multimedia content circulating through computers, from WhatsApp to Youtube, has a good chance of going through FFMPEG's pipeline. So being fast matters.

<!-- and then the contra example of code poems: all_the_names_of_god.pl, speed but crash -->

<!-- conclude that somehow, the aesthetic experience of code is constrained byfunction, speed and usability, but only usability in the sense of maintenance by others -->

### Conclusion

The aesthetic experience of such a technical system as source code is always going to be mediated. The elusive nature of software (is it an idea? is it electricity? is it on my computer? is it on everyone's computer?) makes it that we can only read and write it through an interface: integrated development environments, debuggers, languages, metaphors and diagrams. This tool-assisted aesthetic experience can involve an emotional relationship to one's tools (half-jokingly, half-seriously), stuck between personal expression and collective communication, specific use and universal computation.

What's more, tools can themselves affect the perception of what is nice (as we have seen in our discussion of the Java programming language in relation with the Sapir-Whorf hypothesis), as our the communities of practice in which we operate (as we saw in our discussion of OOP).

If so many programmers relate to craft as a referential for their practice, we've discussed the medium of source code: computation, understood as memory+processing. Drawing on the concept of fitness (from Aristotle to Senett and Pye), we've shown that the aesthetics of source code are centered on _speed_, whether positively (ffmpeg) or negatively (all the names of god).

Ultimately, we conclude on the recursive relationship of tools and automation: crafting one's own tools can lead to automate everyone's work, which creates more information and needs to have more crafted solutions, as our discussion on LLM-generated code shows.
