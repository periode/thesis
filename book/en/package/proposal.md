# Machine Language Beauties - Proposal

## Brief Description

<!-- _In one or two paragraphs, describe the work, its rationale, approach and pedagogy._ -->

<!-- tldr: when people consider that something is nice, they help us better understand what this thing is  -->

This book offers a study of the aesthetics of source code, of the ways it is idealized, experienced, and judged. By critiquing the kinds of beauties written in machine languages, we provide a better understanding of _code as material_ and its involvement in allowing humans to represent the world with, and through, computers.

The largest and most significant texts of our era are written in programming languages; hidden below running software, disappearing into voltage, these writings nonetheless answer to certain aesthetic standards. But why do programmers care about beautiful code, if that code is primarily meant to be transformed into 0s and 1s, consumed by a machine? What kinds of ideals, standards and practices are involved in writing _"good code"_? This book will argue that there is a fundamental relationship between function, knowledge and beauty in the context of computer programming, and that such a relationship sheds a new light on the process of translating the world into computer-understandable terms. Working through examples of real excerpts of source code, and commentaries of programmers themselves, I will show how the aesthetics of source code are manifold, a blend of literature, mathematics and architecture, and yet always centered around the need to understand its functional structures. The people who read and write code indeed have their preferences, idiosyncracies and stylistic allegiances, and yet are always involved in the same task: recasting various problems in terms that can be understood, and acted upon, by both computer and humans. Ultimately, we recast aesthetic judgment as _ethics_ judgment, laying bare what is good and what is bad code, what is acceptable and not, from the perspective of those who write it. We show that there is not a single, monolithic conception of what code should be, or of what code can be; rather we highlight the paradox that source code program texts may create and embody a variety of _worlds_, all the while bound by the particular medium that is their own: computation.

This book cuts across disciplines, yet is rooted in a humanist approach, applying aesthetic philosophy, cognitive sciences and comparative studies in order to shed a light on the making of the invisible texts which structure our lives. It draws on these diverse approaches in order to show what it entails to communicate and function _well_ between humans and machines. The structure of the book will have each chapter centered around differents excerpts of source code, taken from programmer lore, open-source projects or artificial intelligences, to show how source code enables, through craft, poetry, engineering, science or hacks, the construction of the complex semantic structures that represent and act on different aspects of our world.

## Outstanding Features

<!-- _List briefly what you consider to be the outstanding, distinctive or unique features of the work._ -->

There are four specific features that this book offers. First, I have conducted extensive research to gather and analyze specific pieces of source code, which will be deployed throughout my argument, in order to show, as much as tell, what aesthetically-valued source code is. Beyond specifically "artistic" programs, this book therefore offers a peek into the actual writings of the corporate, hacker, and scientific software. Rather than talking abstractly about "code" or "algorithms", I highlight the diverse constellations of machine language writers and readers.

Second, the book proposes to study of the aesthetic aspects of those technical artifacts by comparing them to the fields of literature, architecture and mathematics, and showing the specificity of source code at their overlap. This comparative approach, promising a diversity of perspectives to the reader, as well as multiple points of entry, will prove to be both pedagogical and enlightening. The book will therefore explore the possibilities to consider code as a kind of (literary) text, code as a kind of (mathematical) idea, or code as a kind of (architectural) structure.

Third, this book offers an aesthetic analysis of the realities of software engineering. Rather than focusing on "beautiful" code, this book encompasses all instances of an aesthetic experience. We consider that anytime someone, somewhere, thought "oh! that's pleasant" is worthy of our attention, insofar as it involves a value judgment. By including the possibility of such encounter, this book also suggests a broadening of perspective into "the aesthetics of the everyday", while reframing it via the lense of functionality. As such, it will allow the reader to reconnect with other daily practices which, even though they might be overlooked, nonetheless possess certain qualities.

Finally, this book reconnects aesthetics to ethics in a practical context. By highlighting the implicit standards against which judgments of value are bestowed, it will argue that the act of creation, by shaping matter, sharing ideas and involving others (programmers and users), is an essentially altruistic, situated, and therefore political act.

## Competition

<!-- _Consider the existing books in this field and discuss their strength and weaknesses, individually and specifically. This material is written for reviewers and not for publication, so please be as frank as possible. You should describe how your book will be similar to, as well as different from, the competition in style, topical coverage, and depth. If significant books are now available, you should explain why you choose to write another book in this area. Please mention all pertinent titles, even if they compete only with a part of your book._ -->

Within the last couple of decades, there has been an increase of books concerned with the analysis of software, either from a humanistic perspective, from an artistic perspective, or from a social science perspective. Conversely, some publications in computer science have also addressed the role of art and beauty in the process of writing and reading code. This book proposes to fill the gap of an interdisciplinary and empirical exposition of the relationship between beauty and code.

From an artistic perspective, books such as Geoff Cox and Alex McLean's _Speaking Code_ (MIT Press, 2013) focuses on code as a performative, enacting perspective. While they recast mechanical writing as a form of political expression, they do so in the very specific context of live-coding and artistic practices, leaving aside the overwhelming majority of functional code; similarly, Florian Cramer, in _Words Made Flesh: Code, Culture, Imagination_ (Media Design Research, 2005) discusses source code's historical roots in magical thought, tracing thoughtful connections with other kinds of human inscriptions, but without ever pausing on the specificities and diversities of contemporary source code writing. More recently, Cox and Soon's _Aesthetic Programming_ (Opne Humanities Press, 2020) is a welcome pedagogical introduction to the political implications of code, touching up its semantic load; however, while they discuss how code can express, they do not dwell on how code in specific, or aesthetics in general, _render intelligible_.

<!-- - Words Made Flesh: Code, Culture, Imagination, Rotterdam: Media Design Research, Piet Zwart Institute, Willem de Kooning Academy Hogeschool Rotterdam, 2005, 140 pp.
- Cox, G., & Soon, W. (2020). Aesthetic Programming: A Handbook of Software Studies. Open Humanities Press.
- Cox, G., & McLean, C. A. (2013). Speaking Code: Coding as Aesthetic and Political Expression. MIT Press. -->

From a humanities perspective, several authors have recently examined the specificities of source code, mostly in the light of discursive practices. Brock's _Rhetorical Code Studies_ (University of Michigan Press, 2019) and Marino's _Critical Code Studies_ (MIT Press, 2020) both argue for code as an expressive linguistic system, but has not yet touched upon the dynamics of how programmers make sense of what they do, how different coding styles and languages can involve different epistemologies, despite asserting the political value of source code. As exploratory work, they have nonetheless laid out the essential foundations upon which this books gratefully builds. From a more social perspective, Montfort et. al. developed, in _10 PRINT CHR$(205.5+RND(1));: GOTO 10_ (MIT Press, 2014), a crucial cornerstone in presenting code as a cultural artefact, while Mackenzie's _Cutting Code: Software and Sociality_ (Peter Lang, 2006) uncovers the social interplays in the creation of code, something which Hayles' _My Mother Was A Computer_ (University of Chicago Press, 2010) develops further by inscribing it into the history of cognition and inscription. All of these works thus establish source code as a cultural, social and epistemological artefact, by tracing high-level parallels, but remain limited in deploying precisely _how_ source code creates meaning beyond that which the computer understands. In this book, I propose to build on those works in order to highlight the diversities and specificities of source code as an epistemological device, including the linguistic communities touched upon by Mackenzie and Hayles, the cultural significance higlighted by Marino et. al. and the aesthetic perspective of Cox et. al., yet applied to a much broader corpus. In this, I complement the existing literature by providing both a broader theory of practical code beauty and a swath of examples to support this theory.

<!-- - Brock, K. (2019). Rhetorical Code Studies: Discovering Arguments in and around Code. In Open Research Library. Open Research Library. https://doi.org/10.3998/mpub.10019291
- Marino, M. C. (2020). Critical Code Studies. MIT Press.
- Montfort, N., Baudoin, P., Bell, J., Bogost, I., & Douglass, J. (2014). 10 PRINT CHR$(205.5+RND(1));: GOTO 10 (Illustrated edition). The MIT Press.
- Hayles, N. K. (2010). My Mother Was a Computer: Digital Subjects and Literary Texts. University of Chicago Press.
- Mackenzie, A. (2006). Cutting Code: Software and Sociality. Peter Lang. -->

This book is not only destined to a social science or humanities audience. By thoroughly discussing details and implications of syntax, semantics, implementation and linguistic paradigms, its audience also involves computer scientists. Indeed, computer science literature has been long interested in the question of the aesthetic pleasure and appearance of writing and reading code, every since Donald Knuth's magnum opus _The Art of Computer Programming_ (Addison-Wesley, 2011). Most recently, Oram and Wilson's edited volume _Beautiful Code_ (O'Reilly Media, 2007) offers a series of examples of what leading experts consider beautiful code, showing and discussing actual code snippets. What we see here is the contrapoint to the works published in the social sciences and humanities: specific and empirical (albeit focused on one particular kind of software engineering), yet without a broader theoretical scaffolding in epistemology to connect to. As such, this book complements the discussion of engineering practices with a philosophical approach.

<!-- - Oram, A., & Wilson, G. (Eds.). (2007). Beautiful Code: Leading Programmers Explain How They Think (1st edition). O’Reilly Media.
- Fishwick, P. (Ed.). (2006). Aesthetic Computing. MIT Press. <https://doi.org/10.7551/mitpress/1135.001.0001>
- Goodliffe, P. (2007). Code Craft: The Practice of Writing Excellent Code. No Starch Press.
- Martin, R. C. (2008). Clean Code: A Handbook of Agile Software Craftsmanship. Pearson Education.
- Knuth, D. (2011). The Art of Computer Programming: Combinatorial Algorithms, Volume 4A, Part 1 (1st edition). Addison-Wesley Professional.
- Kernighan, B. W., & Plauger, P. J. (1978). The Elements of Programming Style, 2nd Edition (2nd edition). McGraw-Hill. -->

Finally, we can also point to several other works, from autobiographical fiction (_Geek Sublime: The Beauty of Code, the Code of Beauty_, Vikram Chandra, No Starch Press, 2014) to poetry (_Code {Poems}_, ed. Ishaac Bertram, Impremta Badia, 2012) and satire (_If Hemingway Wrote JavaScript_, A. Croll, No Starch Press, 2014) to highlight the diversity of productions around source code, and thus a lively demand from broader audiences, from general-interest readers to enlightened amateur programmers.

<!-- - Chandra, V. (2014). Geek Sublime: The Beauty of Code, the Code of Beauty. Graywolf Press.
- Croll, A. (2014). If Hemingway Wrote JavaScript. No Starch Press. <https://nostarch.com/hemingway> -->

<!-- From a foreign perspective,

- Lévy, P. (1992). De la programmation considérée comme un des beaux-arts. Éd. la Découverte.
- Paloque-Bergès, C. (2009). Poétique des codes sur le réseau informatique. Archives contemporaines. -->

This book complements the current works available in the market in three ways. First, it bridges the two cultures of the humanist and the scientific by having them meet on the hybrid field of machine languages. It provides technical details through various code snippets to the humanist audience, and epistemoligical and aesthetic-philosophical background to the scientific audience. Second, it differs in style by its comparative approach: instead on focusing exclusively on source code in itself, the book approaches it through the lense of _source code as..._, and with a noted new take on _source code as architecture_. As a result, the book provides multiple points of entry for diverse audiences to dive into what makes source code and software such unique artefacts, all the while re-contextualizing the writing and reading of source code as activities with aesthetic potential, just like other human activites such as literature, mathematics, or architecture. Third, it is the first book which concludes on a _spatial_ depiction of source code texts, arguing that understanding and appreciating source code is understanding and appreciating the computational structures that it renders sensible.

## Apparatus

<!-- _Will the book include examples, cases, questions, problems, glossaries, bibliography, references, appendices, etc.? Do you plan to provide supplementary material (solutions, answers, workbook, laboratory manual or other material) to accompany the book?_ -->

Beyond the inline listings of source code acting as examples, included to support the argument made, there will be no additional material.

## Audience

<!-- _For whom is the book intended (the lay public, professionals, students, etc.)? In what discipline or disciplines? Is it primarily descriptive or quantitative, elementary or rigorous, etc.? Prerequisites, if any (mathematical level, any applicable)?_ -->

Thanks to its interdisciplinary nature, this book is aimed at a variety of readers. The kinds of people who will buy the book can be broadly categorized into two: people with a technical background, who would want to engage with an aesthetic theory of programming, and people with a humanities background, who would want to see how aesthetic theories can be applied to technical artefacts.

Humanists and literary theorists would be interested in the concrete manifestations of source code as specific meaning-making techniques will be able to find a detailed study of how code makes sense, and how these meanings relate to a broader poetics of code, as well as with the aesthetics of other human activities. Similarly, this book will also be of interest to artists and art theorists. By separating aesthetics from the artwork, and focusing on its functional purpose, we suggest that one can think through beauty and artworks not as ends, but as means to accomplish things that formal systems of explanation might not be able to achieve. An aesthetics of source code would therefore argue the existence and purpose of functional beauty, a function which can subsequently have societal implications.

Programmers and computer scientists will find an exhaustive formalization of something they have known implicitly ever since they started writing and reading code. The approach offered by the book of programming languages as poetics and structure might help them think through these aspects in order to write perhaps more aesthetically pleasing, and thus perhaps better, code. What they would get from this book is a way to articulate the various ways in which form and function operate specifically in their domain of activity, as well as a perspective on the various ways in which other programmers can establish different aesthetic standards than their own, doing different things with the same material.

Finally, general readers would be exposed to how an object as obscure and obfuscated as source code exists concretely. The comparative approach to the book (establishing similarities and differences between source code and literature, source code and mathematics, source code and architecture), will engage them in a wider discussion on form and function while being centered around one of the most important kinds of texts of our era. Anyone engaged seriously in an activity which involves a creative process could find here a rigorous study of what goes on into a specific craft, asking how their own practice engages with tools and modes of knowledge, and how they approach the communicative function of their work as an aesthetic endeavour. Conversely, anyone engaged in interactions with software would gain new perspectives on the underlying structures of what the software can do, and can make us do.

## Author

<!-- _Provide relevant information about your research, professional qualifications, or other aspects of your background. What makes you the right person (or the right author team) to write about this topic?_ -->

I have been a lecturer and researcher in media studies, programmer and artist for more than a decade, with a background in political studies from Sciences Po, game design from NYU and literature from Sorbonne-Nouvelle. As such, my approach is fundamentally interdisciplinary and my expertise reaches across fields: I have written software for the Whitney Museum of American Art, the Washington Post and the Open Society Foundations, have exhibited digital artworks in Paris, Berlin, Abu Dhabi and New York, and have lectured at CUNY, NYU, Sciences Po, and Humboldt Universität. And yet, this versatile and multi-faceted approach remaines focused on the emerging field of software studies, and specifically on how software translates the world, representing natural entities into computational equivalents.

My doctoral thesis, under the direction of Nick Montfort (MIT) and Alexandre Gefen (Paris-3/CNRS), focused on the relationship between beauty, function and knowledge in the reading and writing source code, and provided the backdrop for this book. Departing from the aesthetics of code, my current research focuses on the poetics of code—that is, the kinds of spaces and simulations which enact a certain version of the world, and a certain way of understanding and interacting with the world. My research has been published in peer-reviewed academic journals and edited books in the U.S., in France and beyond.

My interdisciplinary background and my career in academia has allowed me to hone my pedagogical skills when introducing complex concepts, ranging from computer science to social science and the humanities, to diverses audiences of students, non-specialist researchers, as well as in mainstream media publications. While source code and programming might be perceived as esoteric and intriguing by the general reader, I have spent my whole career facilitating and contextualizing programming artefacts and practices to non-specialist audiences.

## Market Considerations

<!-- _What kind of person will buy the book, and why? What new information will the book give them to justify its cost? What is your estimate of the total market for the book? If you are aware of professional organizations or mailing lists that would be useful in promoting the book, please mention them._ -->

I anticipate that the first market share for the book will be made up of programmers and computer scientists, and they will find in this book a holistic presentation of the various ways in which programming involves different kinds of styles, translations and practices. Beyond a simple trade book aimed at writing better code, this book re-contextualizes programming as part of other creative activities, combining both technical function and aesthetic experience. The book thus proposes a reflective approach towards writing and reading software. In terms of communication, HackerNews is the leading bulleting board for programmers, ACM and IEEE are the leading research assocations in computer science, which both review monographs.

The second share would be made up of digital humanities scholars and teachers who would both read it for their own research, contributing to the field of software studies through unique case studies, and to the field of critical code studies through the presentation of the argument of programming as an attempt to translate the world into computational structures. These readers would also assign the book to classes in order to explain certain aspects of what programming is, how it can be understood in terms of other practices, and thus provide a foundation to further discuss the social and cultural aspects and implications of software.

## Status of the Book

<!-- _What portion of the material is now complete? When do you expect to have your manuscript completed? What is the planned length of the book in words? How many and what figures (drawings, half-tones, charts, etc.) do you plan to include?_ -->

Two chapters are complete, and the rest is in draft phase. However, most of the content has been articulated in one way or another: after completing a doctoral dissertation on the topic, I have published multiple articles, both in research and in mainstream outlets on and around that topic, and therefore have extensive background research already done.

## Reviewers

<!-- _We may use reviewers of our own choice, but we will also try to include some whose opinion you feel will be valuable. Please suggest a few and clarify your relationship (if any) with each person suggested. If the book has several distinct markets, try to recommend at least one reviewer for each. Naturally, we do not reveal the names of our reviewers without their permission. If you desire, we will submit the material to the reviewers anonymously._ -->

- Nick Montfort (PhD co-supervisor)
- Winnie Soon (Member of the PhD jury)
- Geoff Cox
- Brian Kernighan
- David M. Berry
- Katherine Hayles
