# table of contents

## notes

The toc does not so much talk about the content then about the structure (_the sequence of the research should not be the sequence of the story!_). __Readers stay with a book as long as it promises to answer still unresolved questions__ (sense of crescendo).

ToC should be about two-three pages, single-spaced. Follow chapter titles with two paragraphs (_tell, then support: being with the point the chapter is trying to make, or the question with which it will grapple, then run through the materials_). No more than 8 chapters, plus introduction and epilogue (no need to explain the intro or epilogue).

- the first identifies the point of the chapter
- the second highlighting the materials that the chapter will marshal
- maybe three if needed

some thoughts:

1. every chapter should have a title (background chapter? making-a-point chapter?)
2. complex treatment, simple structure
3. three types of chapters: background or context, argument, break-narrative chapters -> start by just doing narrative/argument chapters
4. embed the interesting details
5. many good books are _layered_: as they unfold, they reveal a more complex, layered view of their analysis (text -> math -> architecture)
6. chapters are beats, or rhythms

## toc

### Introduction

Code as the new material.

### 1. (expository) Code as text

<!-- the point is to show that code can be like text, but also support various kinds of expressive practices. it's not just a quirk of the artists, nor of the engineers (q: what does source code have to do with prose and poetry?) -->
Because source code looks like prose, it is often mistaken, or assimilated as such. In this first chapter, I highlight two extreme aspects of the relation between computer languages and human languages. On one side, I offer a literary analysis of _code poetry_, a practice through which programmers express themselves artistically through programming languages, engaging the reader in the somersaults of word-play, introducing the reader to some of the expressive power of source code. On the other side, I discuss the engineering attempts at _literate programming_, an approach which treats source code as a well laid-out document to be read as a part of technical documentation. The distance between these two distinct approaches to the same computer language hints at the diversity of reading and writing code, from artists to engineers.

<!-- the material is code poetry, literate programming, IOCC -->
I discuss the similarities and differences between computer language and natural language, reminding the reader of the difference between syntax and semantics in both, and offering a reminder on how source code operates at an introductory level. In so doing, I introduce some of the questions that have been raised, and yet not conclusively answered, about software: what is it really? where does it exist? is it an idea, or is it a variation in voltage? This chapter concludes on the role of metaphor to make sense of things, and notes that _code as text_ is but one of the several metaphors we will encounter in our journey.

### 2. (background) The practice of programming: craft, hacks and knowledge

<!-- the point is to give some context as to what kind of activity/practice programming is (q: what do programmers actually do, on average?). it shows that it actually has little to do with literature, but rather craft and science -->
In this chapter, I take a step back from what is software, and focus instead on how is software made. I tell the story of the first programmers, who had to build up their own tools and programs from _ad hoc_ practices. I recall the shift from informal cobbling-together to an engineering discipline, in the context of an ever increasing quest towards _expressivity_ in programming methods and languages.

<!-- the material will be historical, from the hackers, and contemporary, from the craft, taking the productivity and business literature seriously -->
In the spirit of historical contextualization of a specific practice, I trace the portrait of two stereotypes which accompany the figure of the programmer: the hacker and the craftsman. While programmers are often represented in popular culture as hackers, I show how their self-representation has evolved towards the figure of the craftsperson around the 2000s, threading a relation between skill, know-how and material which was already present in the hacking days of yore. This chapter provides context to understand the relationship that programmers have with what they create, the kind of excellence that they strive for. I conclude the chapter on a specific relation to knowledge that programmer have: in-between making and thinking.

### 3. (expository) Code as formula

<!-- the point is to consider programming beauty from a scientific perspective, both theoretical and applied (q: does code get its beauty from mathematics? -->
Going back to the roots of what computation, I lead the reader into the realm of software as mathematical formulas, where we find an unexpected grail of craft: _elegance_. This notion of elegance is first introduced as a question of ratio, between effort and result, between syntax and semantics. This offers us the possbility for a detour into the conception that mathematicians and their applied counterparts, engineers, have of "beauty".

<!-- the material will be taken from research in beauty in math, and from scientific studies of elegance -->
This central place that mathematicians and engineers ascribe to elegance in their work allows me to further develop on the relationship between form and function. By showing how the beauty of a proof acts as a heuristic, as a guide towards correctness, we touch upon the question of design, of form-giving in to best represent a given content.

### 4. (expository) Cosmogony as concrete abstractions and the artistic problem of implementation

<!-- the point is to go deeper into this problem of implementation, and make a parallel with the arts: from idea to realization -->
Starting from the debates in mathematics as to whether they depict reality, or are exclusively tautological, and coupled with the applied nature of software engineering, this chapter argues for the _worldmaking_ powers of the programmer. By implementing ideas in a dynamic medium, the writing of source code seems to be akin to magic, as programming keywords always _do what they mean_. Through the process of implementation, of turning ideas into worlds, we show how the programmers engage in a form of cosmogony, by building a whole and coherent model.

Here, we rely on excerpts from object-oriented source codes, from the gaming simulation _The Sims_ to the learning management system _Moodle_, and artistic code poems in order to highlight how they, respectively, write down and embody concepts of consumerism, pedagogy and community building in terms that the computer can understand.

### 5. (background) styles and programming languages

<!-- the point is to talk about tools and notations: programming languages -->
This is a background on the tools of programming: langauges. It first provides an overview of what kinds of languages exist, and desired properties, and the kinds of people who use them, demonstrating a social use of languages, rather than a strictly functional one. It also introduces the ability of aesthetics to serve as a system of expression, discussing the idea of _best fit_ as an aesthetic ideal. I provide the example to this approach by suggesting that some programming languages are better suited to certain problems, and some programming texts are bettter suited to certain groups of people. This chapter concludes on a first sketch of aesthetics as ethics: the values of clarity, transparency, legibility and humility suggest a particular mode of living-together.

The material used for this chapter is both taken from sociology and epistemology, particular Simmel and Hacking's studies on style, as well as from my research on the stylistic on programming languages. Contrasting the Zen of Python with Go standard formatting, automated style assistants with idiosyncratic hacks, this chapter answers the question of whether style is a group phenomenon or an individual endeavour, and of its value in a technical environment.

### 6. (expository) Code as structure

<!-- the point is to show that syntax is only building blocks, and that science is about linking theory and practice: here we show how it all comes together in software structures -->
Recalling our discussion on craft, we now turn to the fact that most programmers refer interchangeably to _writing_ and _building_ software: as such, our last metaphor is that of the architecture. I will start by showing that programming has its roots in an anti-modern architecture project. Code poems can be seen as kinds of follies and pavillions, while massive housing is a literate programming; the architect's floorplan needs to be translated into an actual material structure, which never goes according to plan; a programmer needs to adapt and fit to the context,to the material and tools that are at their disposal.

Most importantly, the architecture metaphor implies that code is not so much read as _dwelled into_. When reading it, the programmer inhabits it, attempts to trace the multiple paths of execution, the different levels of abstraction, and various dimensions of the real-world problem represented in code. The implication we draw from such conclusions is that programmers are not the only ones to be living within code, albeit they do it in a less-mediated. We argue that end-user interactions are not so much with software, but with software structures, digital environments which are also more or less habitable, more or less idiosyncratic, more or less ethical.

### 7. (expository) Machine-generated code from IDEs to LLMs

<!-- the point is to show that software tools are particular, and they affect our aesthetic judgment. Considering writing code as an aesthetic practice, LLMs reformulate a few questions of style, authorship and sustainability -->
This chapter opens up on the automation of source code writing and reading; such automated processing of language, whether natural or machine, has indeed been a long-stated goal of computer science. While we acknowledge that all creation is always tool-assisted, if only by one's body, what happens when your tool start to create for you? Is there an essential difference between human-made and computer-made? What is at stake between this re-balancing of agency in creation and execution?

After showing that source code writing has always been mediated by technical tools, this chapter offers some suggestions on the qualitative change that is introduced by large language models (LLM), a particular kind of algorithm which can write source code. I touch upon the question of closeness to tools (and whether you make them or not), whether an LLM is fundamentally computer language or natural language, the problem of blackbox understanding in functional environment, and the insertion of extractive, corporate logic within creative practices. I wrap up by answering whether a LLM can write beautiful code.

### Conclusion

Source code beauty resides on the "three perspective problems": how does one condense and compress what the computer can read, what the human can translate and what the world can express. Through each acts of translations, one must find compromises and inaccuracies: we conclude on sketching out some of those poetic/political consequences.

<!--
    - one chapter about the psychology of programming?

    possibly switch first on implementation and second on mathematics
-->
