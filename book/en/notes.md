# book publishing

## todo

- [ ] start with the argument, then with the topic
- [ ] check for typos
- [x] review each section to make more skimmable
- [x] sample chapter topics
- [ ] review toc chapters (develop more on the material, and shorten phrasing and make skimmable)
- [x] check out [ethical programs](https://www.jstor.org/stable/j.ctv65swg4)
- [x] review the submission in the light of the questions from "thinking like your editor"
- [x] table of contents (how many chapters usually?)
- [x] competition (in progress)
- [x] read sample chapter in book

## advice

not just a re-write, but a real scholarly communication

> for your thesis, i see there would be quite a lot of rework to take out all Phd related elements. it usually requires quite a bit of effort to rework to a more general audience. for MIT press, i would recommend you look into <http://computationalculture.net/software-studies-revisited/>, and try to address the editor's concern in your sample writing chapter and proposal. See here too: <https://mitpress.mit.edu/submitting-book-proposal/> and usually two sample chapters are needed. If you choose MIT press, i suggest you drop some of the chapters and develop 1-2 chapters that are related to machine learning (your last concluding chapter that you point to) - Winnie

From the MIT Software Studies revisited:

The revamped series will publish books that focus on __software as a site of societal and technical power__, by responding to the following questions:

- [x] How do we see, think, consume, and make software?
- [ ] How does software—from algorithmic procedures and machine learning models to free and open-source software programs—shape our everyday lives, cultures, societies, and identities?
- [x] How can we critically and creatively analyze something that seems so ubiquitous and general—yet is also so specific and technical?
- [ ] How do artists, designers, coders, scholars, hackers, and activists create new spaces to engage computational culture, enriching the understanding of software as a cultural form?

We are especially interested in contributions that move beyond broad statements about software and integrate a wide range of disciplines—from mathematics to critical race theory, from software art to queer theory—to understand the social and cultural implications of software.

Ultimately, we seek to explore the vast possibilities, histories, relations, and harms that software encompasses.

__IMPORTANT__ I should add something about codework and queerness, and in general about the roles of race and gender-based effects.

How can this book series help to extend insights from software studies to inform critical making and design of software? __ADD SOMETHING ABOUT AESTHETICS ABOUT VALUES__ the social infrastructures of code are also the different groups: academics, engineers, hackers, etc.

Theory from its very beginning was performative or productive, but also troublingly exclusive. To move beyond its elitism, we need to engage both what’s been excluded—aesthetics—and communities that have been excluded as forms of witnessing and event-making.

## wordcount

- 50-80,000 words (max 100,000)
- 4-5 chapters (without intro, conclusion)

## presses

- [University of Iowa Press](https://uipress.uiowa.edu)
- [Stanford University Press - Sensing Media Series](https://www.sup.org/books/series/?series=SENSING%20MEDIA:%20AESTHETICS,%20PHILOSOPHY,%20AND%20CULTURES%20OF%20MEDIA)
- [The MIT Press](https://mitpress.mit.edu/submitting-book-proposal/), try to address points in [software studies revisited](http://computationalculture.net/software-studies-revisited/)
- Chicago
- [University of Minnesota Press](https://www.upress.umn.edu/information/for-prospective-ump-authors/book-proposal-submission-guidelines) (they are a great press but do not have as hard a technical edge), it would be on the [film & media series](https://www.upress.umn.edu/disciplines/film_media), they also have the [manifold]
- [Oxford University Press](https://academic.oup.com/pages/authoring/books/submitting-a-proposal)
- [Bloomsbury](https://www.bloomsbury.com/uk/discover/bloomsbury-academic/authors/)
- [Amherst](https://acpress.amherst.edu/authors)
- [Punctum](https://docs.punctumbooks.com/books/about-submitting-your-manuscript) (full manuscript between 01.05 and 31.07)
- [Open Humanities Press](http://www.openhumanitiespress.org/submit/book-submissions/)
- [Peter Lang](https://www.peterlang.com/for-authors/) (published cutting code)
- [Counterpath Press](https://counterpathpress.org/contact)
- [Amsterdam University Press](https://www.aup.nl/en/series/digital-studies)

standard length (50-80,000 word)

## proposal

Please provide a brief outline of the project’s scope, content and rationale, including what makes it distinctive and/or interventional. What makes the project suited to a digital plus print format? Include 2–5 selling points or key benefits of your project.

Proposed content including chapter outlines (at least half a page per chapter) and main argument. You may wish to submit some sample material if it is available.

Include principle and secondary readership, academic associations and details of courses for which the book would be required or recommended reading. Please give full details of publications that are comparable with your project and specifically those it will be competing with. Indicate the benefits of your project for key target readerships.

Previous publication and copyright:
Please indicate whether any of the material has been published previously and whether you will be using third party material that requires copyright clearance.

Illustrations and audio visual content:
Detail any illustrations (ie, photographs, diagrams, graphs) and audio visual material you plan to include in the print and digital version of the book.

---

one way of looking at it is the difference between writer and reader of source code: one has a very intimate relationship and can be joyfully idiosyncratic, the other has a very altruistic posture and prefers agreed styles
