# Publication française

Publier un essai, de forme un peu plus courte.

Comment le code raconte le monde, _le fascisme des langages machines_

Le code, l'art et la manière

quoi / comment / pourquoi

Mettre à jour les styles et les modes d'écriture, c'est mettre à jour comment le code conçoit le monde. C'est mettre à jour les pratiques de conception et d'implémentation. C'est mettre à jour les idéaux qui guident les modes d'écriture dominants, et les alternatives qui suggèrent différentes manières de faire. C'est mettre à jour la complexité de l'écriture des programmes, et les imaginaires qu'on y projette.

C'est réveler au grand public les textes qui structurent leur vie, la spécificité des langages, de leur façon de formuler le monde.

## recommandations

synopsis résumant le propos et l'originalité.

probablement 3-4 pages maximum.

## maisons d'éditions

- [c&f éditions](https://cfeditions.com/public/)
- [zones](https://www.editions-zones.fr/nous-contacter/)

> Le fil conducteur de son catalogue est la résistance à l’oppression, qu’il s’agisse d’en décrire les nouvelles formes, d’en retracer l’histoire, d’en révéler le fonctionnement et les techniques, mais aussi d’esquisser, à travers le récit des anciennes luttes et des conflits du présent, d’ici et d’ailleurs, le visage d’une nouvelle gauche de combat et d’ouvrir la voie à des alternatives.
> Zones publie des ouvrages d’intervention critique sur l’actualité, des textes d’introduction militants, des livres issus des mouvements sociaux, des classiques oubliés, des enquêtes sociales, des récits de luttes, des textes de théorie critique, avec l’objectif de promouvoir une recherche ouvertement située et militante.

- [b42](https://editions-b42.com/a-propos/)
- [presses de sciences po](https://www.pressesdesciencespo.fr/en/)
- [premier parallèle](http://www.premierparallele.fr/collection/generale)
- [champ vallon](https://www.champ-vallon.com/manuscrits/) (uniquement manuscrits)
