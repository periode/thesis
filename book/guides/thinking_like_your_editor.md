# thinking like your editor

main questions the editor looks to answer:

- does this project have a self-selecting book-buying audience?
- if yes, who makes up that audience?
- what will this book say of significance to those within this audience?
- will this audience, once made aware of this book, go out and buy it?

## how to write a proposal

communicates why a manuscript is being written, what is the statement it will be making? to do this, we need to answer the __big five questions__ (this also shows you're a proper writer):

1. What is this book about? -> beautiful machine texts
2. What is this book's thesis (the argument) and what's new about it? -> beauty and function
3. Why are you the person to write this book?
4. Why is now the time to publish this book?
5. Who will make up the core audience of the book, and why will they find it appealing?

### 1. what is this book about?

describe the topic of the book, _inform and seduce_

if it's a complicated topic, or one that takes time to describe, the ability to ground the readers into the specifics (e.g. technical terms),  becomes important (about your skill as a writer).

> Focus on what is grabbing you and let it grab us as well

### 2. What is this book's argument/thesis and what's new about it?

__Do not topic cover__, but provide a thesis. This means that it starts with "This book will argue that...". The proposal should be clear about exactly what you intend to contribute to the debate. You suggest the material you will bring in to support the argument.

The more you provoke editors with a broadly significant thesis, the more interested they will be.

> Every work of serious non-fiction begins with a question the author has about the topic and ends with an answer the author wants to provide

(1) Begin by going over in your own mind how you first became interested in your topic, in story form: why did this stop me, and why did I start wondering about it? This will explain _why the topic is worth writing about_.

e.g. _"This book will ask and answer the question: 'Why does X and Y... ? Was it because of this and that?'_

> How well you communicate these last two pieces of information (the question gnawing at you and why you find it compelling enough to write a book to answer it)_ will strongly determine the reception your project receives. The most useful clues they have are your book's question and your explanation for why it intrigues you.

The answer to the thesis should be thought of as `thesis + 'and so?' = answer`. __An answer draws out the implications of the author's thesis__. My answer would be: "and so how you do something matters almost as much as what you do"

### 3. Why are you the person to write this book?

Wrote a thesis, wrote a lot of source code, _mastery of intersdisciplinary studies_. wrote a phd about it. wrote some code poetry. been on radio shows, wrote some vulgarization articles, and academic articles as well. I was advised by some leading experts in the field which allowed me to develop a this theory.

add a cv

### 4. What is now the time to publish this book?

What is the thing that should be on everyone's lips in the future? How does the author's contribution answer it?

Thinking no longer about code about text or mathematics, but about architecture, from a "structural point of view"

### 5. Who makes up the core audience for the proposed book, and why will they find it appealing?

Don't lay it on too thick. Highlight the dedicated readers (small but passionnate audience)

Distinguish it from other books (a lot of competition shows a strong market)

## table of contents

The toc does not so much talk about the content then about the structure (_the sequence of the research should not be the sequence of the story!_). __Readers stay with a book as long as it promises to answer still unresolved questions__ (sense of crescendo).

ToC should be about two-three pages, single-spaced. Follow chapter titles with two paragraphs (_tell, then support: being with the point the chapter is trying to make, or the question with which it will grapple, then run through the materials_). No more than 8 chapters, plus introduction and epilogue (no need to explain the intro or epilogue).

- the first identifies the point of the chapter
- the second highlighting the materials that the chapter will marshal

some thoughts:

1. every chapter should have a title (background chapter? making-a-point chapter?)
2. complex treatment, simple structure
3. three types of chapters: background or context, argument, break-narrative chapters -> start by just doing narrative/argument chapters
4. embed the interesting details
5. many good books are _layered_: as they unfold, they reveal a more complex, layered view of their analysis (text -> math -> architecture)
6. chapters are beats, or rhythms

## sample chapter

more of a writing sample than a chapter (takes the best of all chapters and packages them together), about 15 pages

the format is __argumentative__ (not so much new information, but also new analysis). showcase data that is compelling, varied and bookworthy, and simulatneously show that the author has something beyond the data to contribute

take one aspect of your subject and develop it into the sample. not a bad idea to start with a dramatic story

length:

- about 60k-90k words (250-350 pages)
- chapters between 2k and 5k words (2-30 pages)

cf. chap 5., fairness in argument

### writing argument

Argument is what you use to pull everything together into a self-supporting whole that has meaning and coherence. Good arguments draw readers through the very thought processes that brought the author to the positions he holds. __Communicates more than what it says, and stimulates readers to think in new ways__.

It is the intellectual process which carefully marshals and positions these facts in support of a point. Two steps:

1. In early pages, focus on presenting material in coherent and compelling fashin (setting up the story)
2. But then start to add a running commentary (relates to some idea or experience familiar to the reader)

-> "Okay, I see the individual pieces of the picture. Now let's go somewhere with all of this. What does it all add to?". This is when the author gives their independent interpretation of the facts (not afraid to give have their own voice, no longer in student mode)

"~~all that research just to say this?~~"

You have to __deal with competing interpretations__ (what are they, in my case?), and you have to make the best case for the other side's position. switch positions, think in their shoes.

The transition from data to read is like a funnel: the argument condenses it into a compact thesis.

Anecdotes are good! They act as data and can contribute to a point.

All the conclusions must develop from the fact given in the pages: argument is the data-arraying process that makes your conclusions inevitable: __the impression should be of isolated bits being brought in only if and when they support the conclusion__
