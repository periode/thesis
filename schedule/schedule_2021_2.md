# schedule 2021 - 2

## september

- write intro

## october

- catch up on all resources
- - from `notes.md`
- - from [[schedule_2021_1]]
- mathematical beauty (e.g. data scientists/academics)

## november

- end of corpus gathering
- corpus interpretation
- - unix
- - latex
- - ???

## december

- patterns and materiality
