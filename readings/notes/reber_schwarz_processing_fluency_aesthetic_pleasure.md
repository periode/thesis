# Processing Fluency and Aesthetic Pleasure: Is Beauty in the Perceiver's Processing Experience?

## Rolf Reber, Norbert Schwarz and Piotr Winkielman

### Personality and Social Psychology Review 2004

---

three approaches:

- objectivist (beauty is in the object, in proportions, symmetry, etc.)
- subjectivist (beauty is in the person, in how it is appreciated, social construction)

they focus on a third way, the interactionist, in which they develop on patterns of relation between people and objects

> We suggest that aesthetic experience is a function of the perceiver's processing dynamics: The more fluently the perceiver can process an object, the more positive is his or her aesthetic response.

> Features that facilitate fluent processing include all the core features identified in the objectivist tradition, like goodness of form, symmetry, figure-ground contrast, as well as variables that have not received attention in traditional theories of aesthetic pleasure, like perceptual and conceptual priming procedures.

They focus on perceptual fluency, but applies just as well to conceptual fluency.

> Objectively simpler stimuli are not always easier on the cognitive system (higher redundancy, for instance)


---

This study is then updated by [Processing Fluency and Aesthetic Pleasure: Is Beauty in the Perceiver's Processing Experience?](https://sci-hub.mksa.top/10.1207/s15327957pspr0804_3), corrborating the theory of rebert et. al. by engaging into the relationship between beauty and truth, and with processing fluency being a cornerstone of both aesthetic and and epistemic judgments.