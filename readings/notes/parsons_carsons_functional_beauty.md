# functional beauty

## glenn parsons and allen carlson

### oxford university press, 2008

---

the authors make a point about the need for a functional approach to beauty, seeking to re-assert it from a philosophical perspective.

---

#### 1. functional beauty in the aesthetic tradition

in classical western philosophy, socrates says that beauty is being well adapted to some end: _fitness_ is only one kind of beauty among others. it is about _looking_ fit for function, rather than _being_ fit for function (and not just about a sense of harmony in proportions). it is about not making too sharp a distinction between senses and reason.

for hume, the feeling of beauty arises when we perceive an object (what kind of object is software??) that we know to serve a useful purpose, or that raises in us an idea of utility -> beauty is produced by an object conveying the idea of some benefit or utility, not just by appearing fit. thus, the observer has to be aware of the function.

ref:

- hogarth, analysis of fitness
- bishop berkley, alciphron
- smith, theory of moral sentiments

and then burke and kant show up with the idea of the sublime, of disinterestedness (really, fitness is not the only sort of beauty, but it can conjugate with other forms of beauty -> all the names of god, montfort). kant also says that beauty is separate from reason. according to kant, the pure judgment of taste "affords absolutely no cognition (not even a confused one) of the object". nonetheless, __one must know the end of the object to be able to emit a judgment about its perfection__ -> this is __adherent__ beauty (function is not a source of beauty, but a constraint on it, or beauty is merely aligned with the function) (p.24)

function and beauty as congruent

one of the question is: given we cannot always know the function, a priori, does learning about the function alter our perception? aka create aesthetic pleasure.

as a response to the autonomy of art: code can hardly be separated from what it does

ref: benedetto croce, aesthetic: a science of expression and general linguistic: he defines beauty in terms of expression, by which he means a particular mental process which results in a clear, distinct and particular idea.

#### 2. functional beauty in contemporary aesthetic theory

the decline of disinterestedness and the rise of cultural theories of appreciation. these cultural theories are _cognitively rich_ insofar as they also depend on knowledge about the object of appreciation to be an essential component of its appropriate identification and appreciation. this sets the stage for more knowledge in aesthetic appreciation. for instance, in the appreciation of nature, there is a thing called "scientific cognitivism" in which the appreciation of the beauty of nature varies with knowledge of its function.

overall, there is a disdain for functional things as objects of aesthetic inquiry

louis sullivan, modernism -> architecture must be 'sincere', 'honest', 'authentic' (in functional beauty) in architecture, functionalism is inherently good, valuable (even though the function of a building is often contested)

1. problem of translation: how does awareness of function alter or influence aesthetic judgment? if something is functionally beautiful, something about its function should translate in its appearance (shifts in priorities and attention?). syntax highlighting semantics? __how is function translated in form?__ perhaps in affordance. why is the arch looking better at holding than, say a scaffoding? (i'd say because less material, less space taken) finally, it is also about the fact that we are aware that the object is signalling that our needs are going to be met by its function. (it looks nice cause we know it's going to do the job well)

2. problem of indeterminacy: how do we know what the function of an object is? they say it depends not on the designer but on "the market" (aka the designation of function by broad masses, p.68). psychologically, we decide what the function is, how it fits our purpose. but normatively, it is a bit more complex to define the one true function. socrates comes back and says that yes, functional beauty can be relative, that's fine.

important: __comprehensiveness is a virtue in aesthetic theory__

#### 3. indeterminacy and the concept of function

code has a specific function, computing, dictated by its medium. but it also has different application domains. function related to effectiveness (is it bug-free?)

it lifts function from the exclusive realm of intention, and into the broader realm of use.

definition i like:

> an artefact function is any role played by an artefact in a use plan that is justified and communicated to prospective users (w.n. houkes, p.e. vermaas, action versus function: a plea for an alternative metaphysics of artefacts)

-> aka intention + effects, highghlighting that there are some functions that are more "proper" than others.

this also relates to hacking, détournement de la fonction

_reflective equilibrium_ rawls and goodman on how to balance intuition with inquisition

#### 4. function and form

how does knowledge of function affect form perception?

> by employing a category in perceiving an artwork, then, we implicitly impose a kind of structure upon its various perceptual qualities.

understanding the function of an object could mean

1. the understanding of the identity of an object's function (what the function is)
2. the understanding of _how_ or in what way, does the object perform this function

according to Best, functional entities appear graceful when they are free of features that are extraneous, or irrelevant in relation to their function.

it becomes obvious that, if we were to look at the same object with a different functional framework, it would look different.

appearing economical or efficient (more unified), is a matter of degree, rather than kind.

there is also _visual tension_: the fact that an object might not look the part, but actually is doing such part (example of the graceful but powerful crane)

john hospers:

> we enjoy not merely the black and shininess of the automobile, but also these surfaces and forms as expressing life-values, adaptation to certain life-purposes

the above ^ means that adaptation of purpose is a legitimate object of aesthetic experience.

shift of the definition of beauty:

> [...] we are describing a pleasure that is not grounded in the look or appearance of the object, but in something else, and 'beauty' means no more than good, or excellent. (p.103)

because things can be gratifying and pleasurable, on other grounds than aesthetic ones! (example of swiss watch shows we can take pleasure merely in the perception of a thing looking fit)

and let us not forget the aesthetics of dysfunction!

#### 5. architecture and the built environment

buildings, like people and nature, are more than their surface, consisting of much that is not revealed, _and actually obscured by their sensuous surfaces_ (that's actually the challenge/double edged sword). there is an ethical problem in that, because, while art mystifies, things that are functional have a bigger/worst consequence when they mystify (cf. geoff scott)

as gordon graham puts it, the goal of architecture is not simply to design buildings, but to 'create a conception of living'. the modernists thought of function as engineering function: airflow, heat exchange, etc., rather than that of the occupants, such as eating, sleeping, etc.

re: problem of indeterminacy. the changes in proper function of a building takes place only as our collective behaviour changes over time.

re: problem of translation: experience teaches us that many forms can equally fit a given function; there are multiple ways for function to translate into perceptible form. (it's not about picking one form, but about highlighting these multiple ones). there are also ways you can change form and not affect function (or detract, actually)

> the style of a building should so correspond its use that the public can at once grasp the purpose for which it was erected. pugin, contrasts: a parallel between noble edifices.

gordon graham uses the example of the gothic spires, which reach into the heavens, as an expression of function through form.

these translations of function into appearance are:

- looking fit (different from "structurally honest", an interesting distinction to explore)
- being graceful, elegant
- being contra-standard, creating visual tension

functional beauty accords:

- the cold modernity of bauhaus office
- the post modern playfulness of playgrounds
- the sheer power of the stronghold

> _things are functionally beautiful where their perceptual appearances are altered by our knowledge of their function_

(ruins can charm the eye, but not awe the mind)