# A  General Critical Discourse Analysis Framework for Educational Research

## Diana Mullet

### <https://sci-hub.mksa.top/10.1177/1932202X18758260>

---

CDA rests on the notion that the way we use language is purposeful, regardless of whether discursive choices are conscious or unconscious. I would rather use Discourse Analysis, rather than the Critical plug-in (critical focuses on power relation, which is not the main topic here). It's mostly inductive.

analysis process is:

- systematic
- interpretive
- descriptive
- explanatory

makes use of:

- analysis of semiotic data
- situation of the discourse in time and space (context)
- inductive reasoning
- pragmatics (context)
- speech-act theory
- systemic functional linguistics (???) (word order, topic choice, lexical field)

steps:

1. select the discourse
2. locate and prepare data sources
3. explore the background of each text (social and historical)
4. code texts and identify overarching themes (open/inductive coding, axial/deductive coding)
5. analyze the external relations of the texts (inter-discursivity, comparative approach)
6. analyze the internal relations of the text (headlines, vs. body, metaphors, contrasts, omitted details, imperative statements)
7. interpret the data

socio-cognitive approach: influenced by external beliefs and internal knoweldge

"the work that language performs in society"

keywords: knowledge, belief, understanding, ideologies, norms, attitudes. values, plans.

Discourse has been defined as the creative use of language as a social practice

According to Weedon (1987), discourse refers to
> ways of constituting knowledge, together with the social practices, forms of subjectivity and power relations which inhere in such knowledges and relations between them. (p. 108)

Discourse consists of talk, text, and media that express ways of knowing, experiencing and valuing the world (McGregor 2004)

CDA sees discourse as constitutive, in other words, discourse sustains and reproduces the status quo (Wodak & Meyer, 2009). I don't see so much of a CDA, but it's definitely constitutive in terms of _value-making_
