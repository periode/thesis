# a history of software engineering

## niklaus wirth

---

__software engineering is the construction of systems by large groups of people__

the history of software is the history of growth in complexity

in the 1950s is when computers broke out of the closed guardianship of electrical engineers and programmers became needed

cobol was made by the US DoD for business applications

> Programming was known to be a sophisticat ed task requiring devotion and scrutiny, and a love for obscure codes and tricks.

That's why we created programming languages

PL/I was invented to unite scientfic and business communities, under the motto "Everybody can program, thanks to PL/I" (1964)

in 1968 the term software engineering appeared at a conference organized by NATO in Garmisch-Partenkirschen

in academia, it was Djikstra and Hoare who said that it was tricky, and it should be formalized, and in his [[Dijkstra_Notes_Structured_Programming.pdf]] djikstra says that it should be considered a discipline and not a craft. and Hoare published an important paper on data structuring. for another perspective on programming as craft in the 1960s, see [A critical review of the state of programming art](https://sci-hub.mksa.top/https://doi.org/10.1145/1461551.1461574) (COBOL as a "narrative-style" language written in "pidgin English'")

so they (DoD) started Ada.

_meanwhile_, unix and c. the problem he sees is that this was called a "high-level" programming language, but wasn't one cause it is so close to the metal

> computers are machines of incredible complexity, which can be mastered by one tool only: abstraction

> The trouble was that its rules could easily be broken, exactly what many programmers cherished. [...] an invitation to use tricks that were necessary in the early days of computing, but now made systems error-prone and hard to maintain.

The propagation of software engineering then appeared in _schools and homes_, via micro-computers

he argues computing went public with those cheap chips (e.g. Commodore) and cheap compiles (e.g. the P-code Pascal compiler by Ken Bowles).

The craft of programming turned to hacking (insert the moment when microsoft and apple showed up)

there was some mathematical formalization of computer programs (provability, by Floyd, and Hoare logic). but these developments went unnoticed outside of academia (i.e. the business and home sectors). cause the problems of these sectors were way too big

in 1972, liskov invented modules (as in `import a.js`) such that you only need to know the interface.

in 1975, the _Alto_ pioneered fast, interactive workstations. programming has never been the same since then.

then networked technologies became connected to the computer, and in 1990s something called Open Source appeared