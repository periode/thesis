# conceptual blending, form and meaning

## gilles fauconnier, mark turner

### https://tecfa.unige.ch/tecfa/maltt/cofor-1/textes/Fauconnier-Turner03.pdf

---

---

**we can bring two things together mentally in various ways. blending is one of them**

> Conceptual blending is a basic mental operation that leads to new meaning, global insight, and conceptual compressions useful for memory and manipulation of otherwise diffuse ranges of meaning.

this is related to metaphor

conceptual blending is facilitated by a *generic space* which connects both (**IDENTIFY THOSE IN METAPHORS FOR ARRAYS, STREAMS, TYPES**, or maybe types already are generic spaces)

**FORMS ARE MENTAL ELEMENTS** (p.72) (but then they really focus more on signs and units, and it seems to be more applicable to codespeak than to source code)

one of the most familiar human scenes is **caused motion** -> throwing an error, catching an exception, passing an argument

**double-scope**: "essential frame and identity properties  are  brought  in  from  both  inputs.  Double-Scope  Blending can resolve clashes between inputs that differ fundamentally in content  and  topology.  This  is  a  powerful  source  of  human  creativity." (p.60) this is what ricoeur says when he specifies that both target and origin domains of the metaphor influence each other.
**compression**: Compression  maximizes  and  intensifies the set of essential relations shared between two things. **Blending can perform massive compressions and express them in simple forms**. How is that beautiful? It's beautiful when it starts to have secondary meangings (insight as to XXX, problem domain, computer hardware domain, the next one coming, etc.)

> it is always about driving blends in the direction of familiar, human-scale structure; and  it  readily  anchors itself on existing material objects (p. 85)

re: this anchoring on material objects; the only material objects at hand are textual ones.

---

other references

J. GOGUEN,  "An  Introduction  to  Algebraic  Semiotics,  with  Application  to  User Interface Design.", in C. NEHANIV (ed.), Computation for Metaphor, Analogy, and Agents,  A  volume  in  the  series  “Lecture  Notes  in  Artificial  Intelligence”,  Berlin, Springer-Verlag,  1999,  pp. 242-291.
T. VEALE,  "Pragmatic  Forces  in  Metaphor Use:  The  Mechanics  of  Blend  Recruitment  in  Visual  Metaphors",  in  C. NEHANIV, op. cit., pp. 37-51.