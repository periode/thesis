# du mode d'existence des objets techniques

## gilbert simondon

### aubier, 2012

---

#### introduction

prise de conscience de l'objet technique: l'homme est *parmi* les machines, plutot qu'opposé aux machines

pour retrouver la culture, il faut replacer la nature de la machine. on a des sociologues, il faut des *mécanologues* ou des *technologues*

l'objet technique *concrétise* et *surdétermine*, il va de l'abstrait au concret p.24

#### processus de concrétisation

l'artisanat est le premier stade, le stade le plus abstrait, qui détient plusieurs possibles en même temps, parce qu'il n'y a pas encore de cohérence interne. cela se fait par différenciation et concrétisation

#### les deux modes fondamentaux de relation de l'homme au donné technique

minorité:

- objet d'usage
- autour duquel on grandit et se forme
- implicite, non réfléchi, continu
- caractère intuitif, secret, inévolutif de la connaissance

majorité:

- prise de conscience, opération réfléchie
- apprentissage par les moyens de connaissance rationels mis à sa disposition par la science
- caractére rationnel, théorique, scientifique de la connaissance, cf. *L'Encyclopédie*

c'est par ces deux modes que l'objet technique s'incorpore à la culture

condition de connaissance des techniques c'est que l'homme ne soit ni au-dessus ni en-dessous de ces dernières: une relation sociale en quelque sorte

pp135-149 -> section sur l'encyclopédisme

de l'encyclopédisme *éthique*, puis *technique*, on passe à l'encyclopédisme *technologique*,  qui donne à l'individu une possibilité du retour au social, et devient l'objet d'une construction organisatrice au lieu d'être acception d'un donné valorisé ou combattu.

> l'objet technique est au point de concours d'une multitude de données et d'effets scientifiques provenant des domaines les plus variés, [...] coordonnés partiquement dans le fonctionnement de l'objet technique

aka code au centre de multiples influences

#### pensée technique et pensée esthétique

en gros, l'esthétique relie le technique (particulier) et le religieux (totalisant): la pensée esthétique est ce qui maintient le souvenir de l'unité (pas juste une seule unité, mais unité de *réseau*, cf. oeucuménisme plus bas)

> un objet technique peut être beau comme un geste religieux peut être beau, lorsqu'il y a insertion dans le monde en un point et un moment remarquable

c'est le pont entre ces deux pôles qui résulte de **l'abandon de la pensée magique**

l'oeuvre esthétique dépasse son propre domaine pour insinuer la perfection d'autres accomplissements de la pensée dans d'autres domaines (sentiment de perfection) p. 248

quand il parle de la fonction de totalisation du caractère esthétique, peut-être veut-il dire poétique, selon mes termes?

l'esthétique relie donc ces deux pôles, et cela au sein de *la culture* -> *la tendance esthétique est l'œcuménisme de la pensée*.

la statue (ou l'objet esthétique) vient renforcer un point-clef mais ne le crée pas: il s'agit de s'insérer dans le monde et de le surligner. l'oeuvre esthétique apporte des structures construites, mais sur des fondations faisant parties du réel et insérées dans le monde. **l'impression esthétique est alors relative à l'insertion** (de qui? de quoi?)

dans ce contexte, ca ne peut etre basé que sur de la perception: il faut de l'éducation technique: **il faut que la fonction de l'objet soit comprise pour que sa structure, et le rapport de cette structure au monde, soient correctement imaginés, et esthétiquement sentis.**

l'esthétique combine donc les structures figurales avec les structures de fond (cf. `deep structure vs. surface structure`). ce couplage de deux fonds et formes dans deux réalités peut se passer à travers la métaphoe (p.261)

**aspect social**: c'est par rapport à la vie humaine qu'un objet peut être beau. en ce sens, l'objet esthétique est autant objet (perception que sujet (participation), en qu'il est entre savoir et vouloir.

intermédiaire entre structures objectives et monde subjectif

---

l'art reconstitue l'univers

l'art traduit dans d'autres unités temporelles et situationelles

---

côté social, il dit que l'esthétique agit aussi comme médiation entre les représentations techniques spécialisées de différents groupes.

trouver tous les modes sans sortir d'un mode, c'est **l'interpréteur LISP**

(volonté de l'être particulier de dépasser son mode et de réaliser tous les modes), une recherche de la continuité réelle sous la fragmentation arbitraire des domaines

#### pensée technique et pensée philosophique

technique totalisante:
> la pensée théorique issue des techniques est celle au sein de laquelle il est possible de penser d'une manière de nouveau homogène et cohérente la totalité des conditions de l'opération

elegance:
> il n'y a pas de lien analytique entre facilité et efficacité, et pourtant c'est une valeur pour une action d'être à la fois simple et efficace

le réel est la synthèse du virtuel (totalisant) et nécessaire (spécifique) (relation a l'implémentation)

si l'esthétique relie la pensée technique et religieuse, la pensée philosophique relie la pensée théorique et pratique. (cf. [rokeby](http://www.dichtung-digital.de/2003/issue/3/Rokeby.htm))

le mode de rattachement commun, sur lequelle se reposent aussi bien l'opération de la pensée esthétique et l'opération de la pensée philosphique est **la culture**, traduisant le sens des religions et des techniques en contenu culturel: elle examine les résultats de ces convergences. cette examination passe par un ralentissement philosophique pour approfondir le sens et le rendre plus fécond. la culture doit incorporer les techniques (et non vice-versa), pour pouvoir régler la vie humaine d'apres ces ensembles techniques

> les éléments figuraux représentent les attitudes en éléments structuraux

ce que la pensée esthétique est pour le monde naturel (connection subjectif/objectif), la philosophie l'est pour le monde humain. -> **la pensée esthétique peut-elle édifier une relation stable et concrète entre les techniques de l'homme et les pensées sociales et politiques.

deux manières dont c'est possible:

1. conserver la pensée esthétique "pour que les techniques du monde humain puissent rencontrer les fonctions de totalité de ce monde dont la préoccupation anime les pensées sociales et politiques"
2. utiliser la pensée philosophique, une fois qu'on a dédoublé le problème en mode théorique et mode pratique.

mais je pense que l'esthétique permet de manifester cette superposition du technique (différenciant) avec le religieux et polique (totalisant)

ces points-là, ce sont justement les endroits ou techniques et religion se retrouvent, se superposent singulièrement, constituant une réalité culturelle (et possiblement esthétique)

**LA TECHNIQUE A UNE DIVERISITÉ DES MÉTIERS, MAIS QUI COMPORTENT DES SCHÈMES ANALOGUES** (extraction, invisibilisation, etc.)

chacun de ces schèmes sont insérés dans un milieu spécifique, et créent une relation figure/fond

ce qui explique qu'il y ait une composante immédiate, opératoire, voisine d'une composante théorique, normative:

> la pensée qui reconnait la nature de la réalité technique est celle qui, allant au-delà du séparé, découvre l'essence et la portée de l'organisation technique. (p.303)

les point-clefs techniques correspondent aux point-clefs politiques et sociaux (avec des gouts de mcluhanisme dans leur possible déterminisme). d'ailleurs, le socio-politique est la relation de la totalité virtuelle par rapport à la partie actuelle.

en termes de perception, seul un objet peut etre completement percu: or la technique est aussi sujet, correpondant à une épreuve d'existence, une mise en situation.

il considère que l'artiste ne peut que invoquer une intuition: ceci n'est vrai que dans le cas ou le récepteur n'a pas les clés de compréhension (telles qu'il mentionne les clés de savoir technique). goodman et moi ne sommes pas d'accord. cependant, il marque un point quand il dit que la pensée esthétique réfracte, mais ne réfléchit pas (p.323)

deux types de connaissance:

1. la connaissance opératoire, qui donne la possibilité de construire son sujet, qui implique des processus d'abstraction et de généralisation (le "réel" est conséquence de ceci)
2. la connaissance contemplative, qui ne fait que saisir un object, "posé devant"