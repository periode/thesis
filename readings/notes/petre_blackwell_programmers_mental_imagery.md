# A glimpse of expert programmers' mental imagery

## marian petre, alan blackwell

### [link](https://dl.acm.org/doi/10.1145/266399.266409)

---

when asked to construct software solutions to a series of real problems, they always talk about:

- a structure of information
- how it works

the issue is that they do not necessarily map onto data structures coded in programming languages (including oop)

very muhc thunking abstractly and strategically

there are also multiple ways to encore a mental image

it seems that "those with experience of declarative langueages did not design significantly different solutions from those without"

__caveat__: they were not asked to implement the solution as code.

#### results

- dancing symbols ("text with animation")
- mentals discussion
- auditory imagery
- all of the experts described a sort of dynamic mental simulation, a 'machine in the mind'
- dynamic states, a physical machine/an implementation which can be manipulated or simulate transformations.

##### strongly spatial

mathematically-oriented imagery of 'solution surfaces'. it's like reducting the multi dimensional aspect of the problem to a two dimensional surfaces of drops and bumps, where height indicates _goodness_ of a solution

##### landscapes

solutions are spread all over the landscape

##### purely abstract

entities that exist just as symbols, standing for themselves

#### common elements

- stoppable dynamic (they're moving, but at a rate which controlled)
- dynamic resolution of the solution landscape, based on the shifting attention "complete things perceived incompletely"
- many dimensions
- accounts for incompleteness, provisionality, variability
- __everything is labelled__: "the name business is absolutely necessary to understand things."

the relationship between imagery and explanation warrants further explanation