# computers can be understood

## nelson elhage

## [link](https://blog.nelhage.com/post/computers-can-be-understood/)

---

> You will never understand every detail of the implementation of every level on that stack; but you _can_ understand all of them to some level of abstraction, and any specific layer to essentially any depth necessary for any purpose.

you end up reading dependencies as well

>  If the documentation is lacking in some way, you can always go to the source and look at the implementation for an authoritative answer.

building mental models is not just getting languages, apis and libraries as collections of rules and behaviours, but getting a smaller model of core primitives and the rules and principles that generate behaviour in the larger system

he also has [debug war stories](https://nelhagedebugsshit.tumblr.com/)