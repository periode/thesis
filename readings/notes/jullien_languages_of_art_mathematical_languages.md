# From the languages of Art to mathematical languages and back again

## Caroline Jullien

### Enrahonar 49, 2012

---

> The characteristics of beauty are thus useful properties that yield an optimal perception of the object they apply to. [...] Men can understand what is ordered, measured and delineated far better than what is chaotic, without clear boundaries, etc.

She takes the dual analysis of Poincaré, and quotes him to lay out the fact that there is a functional role to aesthetics. In order to investigate this, she turns to `LA`.
