# Learning a Metric for Code Readability

## Raymond P.L. Buse; Westley R. Weimer

### https://ieeexplore.ieee.org/document/5332232

---

---

readability is separate from complexity. you can have readable code and complex code (i.e. it's properly indented but wtf is this doing)

took 120 people and asked them to rate (1-5) the readability of code.

> This analysis seems to confirm the widely held belief that humans agree significantly on what readable code looks like, but not to an overwhelming extent. One implication is that there are, indeed, underlying factors that influence readability of code.

important ones are:

- line length
- identifier (could be more precise?)
- indentation
- keywords (could be more precise?)
- numbers (assuming magic numbers)
- ....and so on. see table on p.550 for full results


this is confirmed by [this study](https://dl.acm.org/doi/abs/10.1145/3196321.3196347) which proves that using shitty lexicon is indeed increasing cognitive load.