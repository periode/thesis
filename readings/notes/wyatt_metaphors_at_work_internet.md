# Danger! Metaphors at Work in Economics, Geophysiology and the Internet

## Sally Wyatt

### [link](https://www.exeter.ac.uk/media/universityofexeter/internationalexeter/documents/iss/Wyatt_danger-metaphors_(3).pdf)

---

internet described in _Wired_ magazine, and metaphorical themes are:

- revolution
- evolution
- salvation
- progress
- universalism

Arthur Miller 1996 about metaphors in the creative aspect of science

> Truths are a moveable host of metaphors, metonymies, and anthropomorphism; in sort, a sum of human relations , which have been poietically and rhetorically intensified, transferred, and embellished (Nietzsche, in Breazeale 1979)

basically, yes, the internet has metaphors, and they might not really be adequate, but they say something of how we'd like to _see_ things