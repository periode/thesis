# Comprehension of computer code relies primarily on domain-general executive brain regions

## ivanova, srikant, sueoka, et. al.

### doi: 10.7554/eLife.58906

---

in a nutshell, this means that literature isn't the only frame through which to appreciate aesthetics in code (given that they exist). so the question of the thesis becomes: what are other aesthetics?

---

systems tested: 

- multiple demand system (math, logic, problem solving)
	- the MD system is also associated with visual search [link](https://psycnet.apa.org/record/2021-28382-001)
- language system (usually activated when reading stuff)

> We found that the MD system exhibited strong bilateral responses to code in both experiments, whereas the language system responded strongly to sentence problems, but weakly or not at all to code problems. Thus, the MD system supports the use of novel cognitive tools even when the input is structurally similar to natural language.

> By code comprehension, we refer to a set of cognitive processes that allow programmers to interpret individual program tokens (such as keywords, variables, and function names), combine them to extract the meaning of program statements, and, finally, combine the statements into a mental representation of the entire program.

The above is very similar to van dijk and kintsch

The previous research is on the field of computational thinking (and not just the wolfram kind, but i should check before dismissing—"breaking down a problem in smaller units"). she quotes **wing 2006** and **wing 2011**

but code comprehension is yet another beast, and somewhat separate from computational thinking, because there are two things:

- reading the computer code
- mentally simulating what that code does

seymour papert is actually one of the guys who assumed that code could be similar to natural

the assumption is also that the brain systems involved are the same across PL (from Python to Scratch)

the MD system is used, but not just left-sided (the side of math and logic). it is *bilateral*

language, not so much, but still a bit *and* higher in python than in scratch

These [motherfuckers](https://doi.org/10.1109/icse.2019.00053) (with all due respect) link bilateral MD use to spatial reasoning (not specifically spatial reasoning as in geometrical, or 3D, but generally spatial (sequential, connected, interdependent, requiring *mental rotation*), spatial as in *tree*-parsing—this is what [this study](https://ieeexplore.ieee.org/document/8812086) says. that study also says that with bigger codebase the cognitive load is much higher)

CAVEAT FOR THE LANGUAGE: this study is done on super simple programs. i wonder how it would fare on larger code bases. because what they also say is that language system *might* have a role to play when it comes to the learning process: there's learning code, and there's learning a codebase

For instance, 

> Such results indicate that the language system and/or the general semantic system might play a role in learning to process computer code, especially in children, when the language system is still developing.

This means that the metaphor (is that related to the language system?) might be of use in learning to process (novel) computer code

> Further investigations of the role of the language system in computational thinking have the potential to shed light on the exact computations supported by these regions.

language still has a role to play in the computational thinking?

metaphors themselves are primarily conceptual, and only secondarily linguistic. so there could still be aesthetics (pleasing manifestations because they sustain a goal) but only textual "at the surface level"