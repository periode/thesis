# From Airline Reservations to Sonic the Hedgehog: A History of the Software Industry

## Martin Campbell-Kelly

---

3 groups in the book:

- software contractors (w/ mainframes, as soon as 1950s) (FORTRAN)
- producers of corporate softw are (w/ business applications, in the 1960s) (COBOL)
- producers of mass-market software

he states a cutoff date of 1995, which is when the web arrived, and that ushered in a whole new era of computing (casual computing? amateur computing?)

very business-oriented, but quite relevant on this specific topic