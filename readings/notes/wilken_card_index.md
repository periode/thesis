# the card index as creativity machine

## wilken

### [link](https://culturemachine.net/wp-content/uploads/2019/01/373-604-1-PB.pdf)

---

mostly about barthes and his use of index card

index cards as co-creation, similarity with the concept of the fragment. invention, in the derrida sense is the oscillation between performative and constative

the scriptible text as the one open to multiple meanings

german media studies considers that anything which allows or prevents the processing of data is their object of research

stiegler: tertiary memory (after perception and imagination, storage)