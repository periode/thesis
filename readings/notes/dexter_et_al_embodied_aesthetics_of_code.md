# on the embodied aesthetics of code

## scott dexter, melissa dolese, angelika seidel, aaron kozbelt

### [link](https://culturemachine.net/wp-content/uploads/2019/01/8-Embodied-438-895-1-PB.pdf)

---
> Are there meaningful relationships between the founding metaphors of a programming language, the metaphors programmers use to frame and solve problems in that language?

> While the focus of the ‘digital humanities’ on using computers as analytic/diagnostic tools for such aesthetic objects as may be reduced to digitized data, we propose the use of code as a vehicle by which we may decouple the machine from cognition, thereby coming to a deeper understanding of the phenomena which undergird meaning
---

distinction between function and style

they argue that the artifacts made by programmers are not material objects (but they're nonetheless called __artefacts__ !)

so their argument is that the "nerds" live in a different space time. related to the speed of execution in [[mckenzie_cutting_code]]

references for aesthetics in technical domains by scholars Curtin, Tauber, Wechsler
 
 Hadamard (1954) collected aesthetic accounts from mathematicians which can be useful for [[understanding_beauty#understanding in mathematics]], along with writings by Dirac, Heisenberg and Chandrasekhar (1990)
 
  > What is intelligible is also what is beautiful (Chandrasekhar)

> If you write a program well, it’s very elegant; it  sings, it’s well built. I enjoy it from an engineering point of view (C. Wayne Ratcliffe)

a lot of different domains in the above!!

> An object is ‘functionally beautiful’ to the extent that its aesthetic properties contribute to its overall performance–the functional beauty of an object enhances its fulfilling its primary function. (Davies, 2006)

aesthetics is for appreciation, repair or modification of programs

as expected, it's easier to find ugly code than beautiful code, and then the hardest is to find the "correct code"

there is a flow state, and they base themselves on this to argue for embodied aesthetics. i don't think it's a feature of aesthetics. i think it's a feature also of the toolchain?

another study of programming is Perkins, 1981 [[understanding_code]]

they say that programmers use terms like _flow_, _balance_, _flexible_, and say it's the domain of the embodied, but these can also apply to structure

embodied cognition is always __spatialized__ cognition

stroustrop: java and c++ are close to the metal because they are working with objects of fixed size, while functional languages like LISP operate in an abstract world

> At its roots, software is the supreme act of metaphor

aesthetic judgements of code can be targeted at: (re: [[ideals]])

- algorithm being implemented
- stylistic concerns
- textual formatting
- naming conventions (how is this not a stylistic concern?)

then an example of C code that is considered beautiful by its author (Warren, 2007)

check Otte & Schmidt (2007) for an example of a network service written in C++ and OOP that they find beautiful for the use of "OOP techniques to balance key domain forces such as patterns, etc" -> abstraction on the `wait_for_multiple_connections()` function and the use of templating. names of patterns are acturally very architectural (visitor, faced, wrapper)

Dybvig (2007) has another example of LISP, that requires LISP-specific knowledge

Johnson (2007) argued that everything we deem aesthetic involves an experience in which we have the capacity to make and experience meaning, and that we do so through our visceral, embodied connections with the world