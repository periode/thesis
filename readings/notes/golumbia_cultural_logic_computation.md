# the cultural logic of computation

## david golumbia

---

The Cultural Politics of the Computer Model of the Mind (and of the World)

---

the book is about computation as a theory which influences/creates socio-political phenomena. computers are cultural phenomena first and foremost

> To a greater degree than do some of those earlier concepts, computing overlaps with one of the most influential lines in the history of modern thought, namely the rationalist theory of mind.

chap 1. (chomsky, functionalism) and 2. (language and computation) seem to be the most relevant

> Programming languages, as Derrida knew, are codes: they have one and only one correct interpretation (or, at the absolute limit, a determinate number of discrete interpretations). Human language practice almost never has a single correct interpretation. Languages are not codes; programming “languages” like Basic and FORTRAN and scripting “languages” like HTML and JavaScript do not serve the functions that human languages do. The use of the term language to describe them is a deliberate metaphor, one that is meant to help us interact with machines, but we must not let ourselves lose sight of its metaphorical status, and yet this forgetting has been at stake from the first application of the name. (p.19)

> it is instead because we are material beings embedded in the physical and historical contexts of our experience, and it turns out that what we refer to as “thought” and “language” and “self” emerge from those physical materialities. (p.20)

### chomsky and computation

chomsky's view is that language and thought can be reduced to computation. his whole point seems to be that chomsky was a convenient author-function for the noe-liberal values and policies of the US at the time.

> Computers invite us to view languages on their terms: on the terms by which computers use formal systems that we have recently decided to call languages—that is, programming languages. p. 84

Illegitimate analogy between code and language in Weaver memorandum. He states that code and language are kinda the same, and the author disagrees, since there was no formal knowledge of linguistics amongst CS fathers. He also mentions SHRDLU, could be useful as a reference for the AI paper.

there's also a skeptical part about the semantic web (because it's about data, and not text, and because data is profitable)