# On Knowledge In Architecture and Science

## Peter Dowton

###  http://anzasca.net/wp-content/uploads/2014/08/ANZASCA-1998-Downton-Knowledge.pdf

---

-> in order to establish a scientific basis to design, to figure out formally who one creates.

rorty: "to know is to represent accurately what is outside the mind"

but he's really just ranting about the social construction of science. and he's mad because that corresponds to a top-down projection of normative knowledge on the craft of architecture.

tl;dr: rational technicality isn't always the path to knowledge. designers also have it, but it's much more *internalized* (or not yet formalized, which might be the same thing).

yes because if design knowledge is personal, the problem of programming is communicating that personal knowledge