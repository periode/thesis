# tractatus logico-philosophicus

## ludwig wittgenstein

### gallimard, 1993

---

The point of of including the tractatus is to highlight the correlation between understanding and meaning, in the context of formal systems.

Within source code, meaning is always coupled to function: what it means is what it does. incorrectness is when two kinds of meaning appear: what it _should_ mean and what it _actually_ means.

So the tractatus is a good entry point into elucidating meaning as an exclusively formal matter: there are no problems in philosophy, there are only misunderstandings.