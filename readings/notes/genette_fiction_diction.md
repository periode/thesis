# fiction et diction

## gerard genette

### 1991, seuil, [archive](https://archive.org/details/GrardGenetteFictionEtDictionSeuil1991)

---


---

la littérarité est l'aspect esthétique la pratique littéraire, laissant temporairement ses côtés psychologiques et idéologiques.

jakobson: étudier la poétique (ce qui fait d'un message une oeuvre d'art) c'est aussi étudier ce qui sépare l'art de X des autres arts (e.g. l'art du code source de l'art pictural).

qu'est-ce qui différencie la programmation comme pratique verbale?

la littérature est *cosa mentale*

what is art -> when is art

Aristote a deux défintions du language:

- *legein*, ordinaire, pour parler, informer, interroger, persuader -> **rhétorique**
- *poiein*, artistique, pour créer, qui ne se passe que par de la mimesis, de la simulation d'évènements imaginaires.

la simulation d'évènements concrets, pour aristote, n'est pas artistique. mais ils peuvent le devenir, pour genette, par le biais de la diction

donc la diction, c'est l'ouverture de la littérarité à d'autres textes, surtout quand il s'agit d'un conditionnel.