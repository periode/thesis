# recherches philosophiques

## ludwig wittgenstein

###  gallimard, 2004

---

le concept principal c'est celui du jeu de language,:

> l'ensemble formé par le language et ses activités avec lesquelles il est entrelacé (p. 31#7)

> la signification d'un mot, c'est son emploi dans le langage (p.50#43)

l'implication, c'est qu'il ne suffit pas d'avoir une définition claire du langage (propositions, énoncés, noms) mais aussi de son usage (contexte, intonation, pragmatique)

il pose aussi la question: _sur quels types d'exemples avons-nous appris un certain usage du langage?_ les communautés d'apprentissages sont donc importantes, moins pour l'ordinateur que pour les programmeurs.

p.70-80, il aborde la question de l'idéal et du but, on ne peut juger l'exactitude d'une proposition que par rapport à l'idéal de précision que l'on s'est fixé. Un idéal peut nous aveugler, de sorte que nous perdons de vue l'application effective du mot/de la proposition en question. p#445 il ya  aussi une question d'attente, d'expectation: dans le langage, l'attente et son remplissement entrent en contact.

**faire un ctrl+f pour _compréhension_ dans la version pdf**

p.100-105 exemples d'enfants et d'hommes qui lisent et analyse du processus de lecture (entre répétition, rappel de lectures précédentes et déchiffrage)

p.119#186, il fait une équivalence entre _compréhension_ et _intuition_, et amène donc une dimension un peu plus sensuelle de la chose

p.122#193

> Et il est vrai que le mouvement de la machine comme symbole est prédéterminé que celui d'une quelconque machine réelle donnée.

p.126#199:

> comprendre une phrase veut dire comprendre un langage. comprendre un langage veut dire maîtriser une technique.

encore une fois, de l'application plus que du concept. comme flusser, les abstraits existent, d'une manière ou d'une autre, et ce qui compte c'est la manifestation de cet abstrait dans un langage particulier.

p.194#503 une bonne raison est une raison qui parait _telle_ -> cela montre l'importance du ressenti, du sensible dans le jugement de qualité, même au niveau épistémologique.

p.#454 le signe ne montre que dans l'application qu'un être vivant fait de lui (usage et intentionalité)

p.205#527, la compréhension passe par la métaphore

p.206#531, il y a deux modèles de compréhension:

> ce qui fait que, étant donné une phrase, on la comprend parce qu'elle peut être remplacée par une autre phrase qui dit la même chose, mais aussi au sense où elle ne peut être remplacée par aucune autre (ndr: équivalence vs. identité)

p.209#569 sur le language en tant qu'outil: on peut faire de la physique avec des pieds et des pouces autant qu'avec des mètres et de centimètres, mais _un système peut être plus lent que l'autre_

tout signe isolé parait mort (similaire à ricoeur) (p.184#432)

