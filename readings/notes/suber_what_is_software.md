# what is software?

## peter suber

### https://legacy.earlham.edu/~peters/writing/software.htm

---

#### first

the strong thesis is that software is pattern *per se* (in the broad sense of anything structured, not particularly regular or so), or syntactical form (and thus matter?)

> Software patterns, then, are only contingently expressed in binary codes, but are essentially expressed as arrays of symbols —or texts.

>  A digital pattern is an organized array of differences.

interesting tension: between exact information and blurry meaning.

- one pattern in information is active operator: **software**
- another pattern in information is passive object: **data**


#### second

it is *pattern which is readable and executable by the machine*. that is, it abides by certain formal/syntactic rules.

but to go further, we must project *intent*

> This suggests that to understand software we must understand intentions, purposes, goals, or will, which enlarges the problem far more than we originally anticipated. [...] We should not be surprised if human compositions that are meant to make machines do useful work should require us to posit and understand human purposiveness. After all, to distinguish *literature* from noise requires a similar undertaking.

so is software a **human composition meant to make a machine do interesting work**? there could be a version where there is no human involved, just purpose (like bio computation) but that's beyond the scope of the phd

source code has a material component **because** to be interpreted by a machine it must have a physical form. can't be oral, e.g.

software itself, though, is both the immanerial *idea* and its material manifestation

he says that the *soft* in software is related to:

- ideality
- alterability

> Software has a dimension of pure pattern or meaning, and is in that sense soft.

it can actually be that you change parts of the source code and the software remains, semantically, the same. (difference between semantic pattern and syntactic pattern)

> If one is to set the time, one should think about time, not about gear ratios.

> software qua pattern is not a series of effective methods; it is the encoded expression of them. 