# the philosophy of software

## david berry

---

> The challenge is to bring software back into visibility so that we can pay attention to both what it is (ontology), where it has come from (through media archeology and genealogy) but also what it is doing (through a form of mechanology), so we can understand this 'dynamic of organized inorganic matter' (Stiegler 1998: 84). [source](http://tapoc.sourceforge.net/progress/2012/06/notes_for_berry-philosophy_of_software.html) p.4

> Code is not a medium that contains the other mediums, rather it is a medium that radically reshapes and transforms them into a new unitary form. (p.9)

definition: **textual and social practices of source code writing, testing and distribution**

> In a Clausewitzian sense, I want to think through the notion of 'absolute' code, rather than 'real' code, where real code would reflect the contingency and uncertainty of programming in particular locations. Absolute code would then be thinking in terms of both the grammar of code itself, but also trying to capture how programmers think algorithmically, or better, computationally. . . . Programmers have tended to need to continually use this hermeneutic circle of understanding 'the parts', 'the whole' and the 'parts in terms of the whole' in order to understand and write code (and communicate with each other), and this is striking in its similarity to literary creativity, to which it is sometimes compared

TOTAL CODE vs. ACTUAL CODE

> Code is therefore technical and social, and material and symbolic simultaneously. Rather, code needs to be approached in its multiplicity, that is, as a literature, a mechanism, a spatial form (organization), and as a repository of social norms, values, patterns and processes. (pp 36-37)

To this, I add that one must also consider how aesthetics allow for the grasping of those multiple domains at the same time

This means that code runs sequentially (even in parallel systems the separate threads run in an extremely pedestrian fashion), and therefore its repetitions and ordering processes are uniquely laid out as stepping stones for us to follow through the code, but in action it can run millions of times faster than we can think – and in doing so introduce qualitative changes that we may miss if we focus only on the level of the textual.

Understanding of code through programming practices via habituation, training, education, knowledge setups, structural constraints/IDEs/tools/builds, shared knowledge and socialization (combination of explicit and tacit knowledge)

Code as:

- engine (mechanical process, power)
- image (symbols, quicksort)
- medium of communication (voice, data, transfer)
- container (data, harddrive)

Towards a grammar of code: Weberian ideal-types of analytical categories to build grammar: data, code (operative symbolic language), delegated (source code), prescriptive (software), critical (epistemological), commentary.

The above forgets about poeticc/beautiful code

leaked documents: (70) The documents also showed where Microsoft employees were required to break programming conventions and perform 'hacks', or inelegant software fixes to get around stupid, restrictive or problematic bottlenecks in the existing codebase.

The Underhanded C Contest

temporality (the clock) of code and spatiality (network)

> Another curious feature of code is that it relies on a notion of spatiality that is formed by the peculiar linear form of computer memory and the idea of address space. As far as the computer is concerned memory is a storage device which could be located anywhere in the world. p.68

Bernard Stiegler interpretation of Heidegger, Simondon platform, and Wilfred Sellars phenomenology: materiality of code as it is tied to phenomena, whether prescriptively creating it or being part of it. materiality concretized in instrumentation

> As Heim (1987) explains: The writer has no choice but to remain on the surface of the system underpinning the symbols. . . . Digital entities can then be said to have a double articulation in that they are represented both spatially within our material universe, but also with the representational space created within the computational device – a digital universe. The computational device is, in some senses, a container of a universe (as a digital space) which is itself a container for the basic primordial structures which allow further complexification and abstraction towards a notion of world presented to the user. (p.138-139)

(151-152) To be computable, the stream must be **inscribed, written down**, or recorded, and then it can be endlessly recombined, disseminated, processed and computed. . . . The consistencies of the computational stream are supported by aggregating systems for storing data and modes of knowledge, including material apparatuses of a technical, scientific and aesthetic nature (Guattari 1996: 116).