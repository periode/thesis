# knowing and being

## michael polanyi

### various misc

---

[src](https://archive.org/details/knowingbeingessa00pola/page/134/mode/2up?view=theater)

polanyi seems to be originating postcritique, which is a turn in literary critique in which one moves away from hermeneutics/interpretation (vs. implication), to prefer affect, aesthetic, phenomenological, and yet some sort of distance. the connection to ricoeur, as the hermeneutics of suspicion (i.e. don't take it as face value, displyaing of smug knowingness) -> Rita Felski  "we should all heed Ricœur’s advice to combine a willingness to suspect with an eagerness to listen; there is no reason why our readings cannot blend analysis and attachment, criticism and love."

polanyi does deal a lot with gestalt vs. particularls. formalism, monadism is about dealing with particulars, when rather we get a holistic approach. he also says that a _elucidation of a comprehensive object_ and the mastering of a skill are both in this pendulum movement of dismemberment (lol what a phrase) and integration. a connection that polanyi offers is that of _visual perception_, circling back to mccullough and levitt.