# "Who Cares about Elegance?" The Role of Aesthetics in Programming Language Design

## Bruce J. McLennan

### [https://web.eecs.utk.edu/~bmaclenn/papers/Elegance.html]

---

the main analogy is **structural engineering**, with a parallel to Billington's [The Tower and the Bridge](https://books.google.de/books/about/The_Tower_and_the_Bridge.html?id=5MTrpQFvWE8C&redir_esc=y)

according to billington, there are 3 E's:

- efficiency (scientific issue, related to the measurement and use/waste of resources such as runtime memory usage, battery life, etc.)
- elegance (symbolical, trying to minimize, somehow, complexity. elegance is the answer to underdetermination)
- economy (social issue, trying to maximize social benefit vs. cost, so this means it's actually public-facing, not so much source-code facing (or can we even tell that from the source code?). different PLs shift priorities based on the socio-economic context)

> The design looks unbalanced if the forces are unbalanced, and the design looks stable if it is stable.  By restricting our attention to designs in which the interaction of features is manifest - in which good interactions look good, and bad interactions look bad - we can let our aesthetic sense guide our design

How do you acquire this vision? Practice.