# everyday aesthetics

## yuriko saito

---

the everyday aesthetics requires defamiliarization, the casting of an aura in order to be appreciated: is this what obfuscated code and code poetry are?

there is such a thing as "negative aesthetics" (ugly, gross, stinky, etc.)

at this point, there are kind of two degrees to aesthetics:

- grace, sublime, elegance (they all require some background, some expertise)
- pretty, clean, cute (spontaneous reaction, uncontrolled)

> the aesthetic dimension of active involvement

pragmatism is often the first theoretical foundation for such a field

this specific article is focused on _artification_, or the process of introducing artistic practice and aesthetics into areas and disciplines not usually associated with them. this process of artification helps with defamiliarization (see above re: code poetry)

p. 133 - humility among designers and artists, coming from a buddhist tradition, and echoing the egoless programming!

### chap 3 - aesthetics of distinctive characteristics

### iv - truth to materials

this "truth to materials" is based on Japanese aesthetics and developed in John Ruskin (arts&craft) (p.118). this is very relevant to PL.

> the aesthetic appreciation of a single object or a material for expressing its own characteristics

[[ideals#craft and beauty]]

### chap 4 - everyday aesthetic qualities

> clean, dirty, neat, messy, organized, disorganized

a lot of it is based on thomas leddy's argument, that these are important, but are nonetheless less complex than traditional aesthetic qualities. but on the other side, they are as, if not more, complex, because there are context-dependent, and have _pragmatic significance_ (they are operational)

are they also considered less complex because they've been carried out by people of lower status?

then she goes on an inquiry of the status of these qualities (drity is "out of place", etc.) p.156

and then these are actually related to _functionality_ (increased or decreased)

> our effort for clean-up is sometimes motivated by aesthetic considerations, without any functional ramifications.

and finally it's a reflection of personal character and moral values