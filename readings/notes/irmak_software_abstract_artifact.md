# software is an abstract artifact

## nurbay irmak

### https://philpapers.org/archive/IRMSIA.pdf

---

> I will argue that although it  is an artifact, software cannot be identified with any concrete object, that is, any object having  spatio-temporal location.

Strongly disagree. I take the opposite, materialist view, that software can be located. There are copies of software, there is an idea of software, but software's second requirement (per [suber](suber_what_is_software.md)) is that it should be materially implemented.

It is ths dual nature view (both abstract and concrete), as per Colburn, 2000, which makes software weird and confusing af.

His point is that software, like musical works, depend on humans for their existence (first because they create them with *only* their initial intentions).

also he has a very narrow definition of "purpose" -> games or digital audio have no purpose?