# the nature of everyday aesthetics

## tom leddy

---

chap 1 of the edited volume, includes sections on the environment (built or not) and food

apparently there is a considerable overlap between everyday aesthetics and environmental aesthetic, because it focuses on the _entire lived experience_, in a holistic manner (all of the senses are involved, so does that exclude software?)

just because we are fully engaged in some sort of activity in the everyday, doesn't mean we can't stand back and enjoy this activity from an aesthetic point of view!

everyday aesthetics might be correlated with Kant's notion of _agreeable_ (as opposed to the _beautiful_). kant says that "taste" does not come into play when it comes to the agreeable.

objection #1: aesthetics don't have to be completely disinterested

objection #2: the concept of beauty should not be excluded from everyday aesthetics

objection #3: there could still be an imaginative (cognitive) component in the appreciation of the agreeable

> he beautiful is primarily a matter of the play of imagination and understanding. (p.26)

it's also important to remember the corollary: the feeling of _displeasure_ when we see things we don't find beautiful

question about the adjective "right" (feels right, looks right, etc.), which refers to Goodman's "rightness of fit". there is also a practical sense of right, which creates a link (if ambiguous) between aesthetic and practical

qualities such as _ordered_ or _right_ are baseline in aesthetics: they come before more complicated ones, such as _symmetrical_ or _proportional_

> Beauty is the sensible shining of the idea (Hegel)

> Someone might argue that the domain of everyday aesthetics should be understood as completely subjective, and that Kant’s point about the agreeable is also appropriate here: there is no disputing tastes in everyday aesthetics. On the other hand, there are regions within everyday aesthetics in which taste does not seem that subjective. (such as code)