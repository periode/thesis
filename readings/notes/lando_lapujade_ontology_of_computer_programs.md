# TOWARDS A GENERAL ONTOLOGY  OF COMPUTER PROGRAMS

## Pascal Lando, Anne Lapujade, Gilles Kassel, Frédéric Fürst

---

[[understanding_code#understanding code]]

Ontologies are used for formal representation of domain knowledge.

This approach enables us to take into account th e “dual nature” of computer programs, which can be  considered as both syntactic entities (well-formed expressions in a programming language) and artefacts  whose function is to enable computers to process information.

There are two complexities to be mastered:

- conceptual complexity, modeling programs at different abstraction levels
- modeling complexity, adding on new modules to the previous one(s) designed

elements of a computer program:

- files (inscriptions on electronic support)
- computer language expressions
- datatypes (PL concepts) and algorithms (calculus steps, procedures)

there are temporal entities (program executions), physical entities (inscriptions on files), plural entities (code base), functional entities (program execution platforms) and dual nature (syntactic and functional entities—the programs themselves)

in this sense, __progranms are expressions__

but there is also a functional dimension, always looming in the background

conceptualization:

- concept: datatype
- proposition: action model: algorithm