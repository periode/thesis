# cutting code: on software and sociality
## adrian mckenzie

- sociological, STS


### chapter 1

software as social object/material and process/practice, involving:

- mutability
- contingency
- necessity

> software undergoes phase transitions.

*technical practices and contexts of software interlace with cultural practices and contexts*

formalism: software is just relation and operations, and the beauty of context-free grammars is that software works regardless of its purpose

an ontology is like a formal specification of a program, describing concepts and relationships, but anthropology and sociology always need "situated practices"

attribution of agency (who/what does what to whom/what) is a precondition of any social relation whatsoever. software has *secondary* agency, in that it extends the agency of the programmer/hacker/corporation.

code involes:
1. index (it indicates (or hides) something)
2. originators (producers)
3. recipients (humans/machines)
4. prototypes (simulations -i find this category a bit narrow, all a computer does is simulate)

but none of these are clearly delimited between one and the other

### chap 2

the big tensions is on __code between operation and expression__ (maybe the operation of an expression)

> Kittler diagnoses code as comething peoiple read and write yet cannot comprehend because it has been encrusted with a layered architecture that sequesters code in accessible interior spaces.

__temporal inaccessibility__: the software is too fast to understand, it's impossible to experience it as expression and operation at the same time.

he says that code poetry is kitsch art, the computing equivalent of dabbling in paint by numbers -> i think there is much more to that, that it is possible to write code-specific poems, rather than as refractions of human languages. it's just an introduction to the topic. good poetry (software art) is one which acknowledges its own execution.

how are software artists different from code poets? __the former don't pay attention to form, just to effect__ (is form and effect always separate?)

*Code may in a general sense be opaque and legible only to specialists, much like a cave painting's sign system, but it has been inscribed, programmed, written.  It is conditioned and concretely historical.  Whether or not non-human agents have had a 'hand' in its formulation, code remains not only a constructing force but also that which is constructed.*

Highly recursive. extreme programming treats "the program" as both the object and model of work.

**every programming language is already an ontology**

> Well-known code constructs such as loops, conditionnal tests, and data structures (arrays, queues, stacks, dictionaries) in popular programming languages __afford this concentration or intensification of movements__ (p.57)

### chap 3

> the orderings of social fields associated with software hinge on formatl properties of algorithms that often assume an immutable, general aura.

-> algorithms _naturalize_ a certain order, they organize informatic time, which results in abstraction. (and yet, these abstractions are still related to a concrete framework in real-life).

| SPACE | TIME |
|-------|----------------|
| DATA STRUCTURE | ITINERARY |

### chap 4 - linux kernel

p. 87: why is this piece of hardware code in the linux kernel considered ugly? because of the magic number, because it _imitates_ (literal display) rather than _represents_ (extracts an essence that is not exclusively dependent on a specific place and time). there's a tension between hardware and software.

RadioFreeLinux as an example of the hybridity of circulation modes.

### chap 5 - jvm

the virtual machine is the "ideal machine". it's mostly a discussion of the concept of virtual, which is less hyped today.

the VM influences the circulation of software, and its writeability

btw, Java is a lexical pastiche of C++ in order to maintain familiarity of developers with the language. but the Java APIs become a mess of their own -> code reading extends beyond the screen and into the docs

### chap 6

code as collective imagining is a response to software is losing its locality

### chap 7

because it deals with information, people think that software deals with abstraction.

the more code you write, the mode complex and messy (while you'd expect the opposite, in terms of adding more and more abstraction).

_extreme programming_ asserts the position of programmers as originators of software (as a local practice)

### conclusion

the status of code is distributed along agency, materiality and sociality

(sidenote: aesthetics relate to opacity)