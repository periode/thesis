# mathematical beauty, understanding and discovery

## carlo cellucci

---

Complements rota's definition of "enlightening", and therefore confirms aesthetics as an epistemic value
The biggest issue is clarifying what 'enlightening' is.

Zeki et. al, 2014, run some neuroscience experiments to examine mathematicians' reponses to the aesthetic; they conclude that beauty in mathematics is a thing. what kind of thing? that's not clear.

is mathematical beauty (a) independent from the observer, and intrinsic to the mathematical elements or (b) a projection of the observer?

Plato and Aristotle are strong proponents of the first one. Some aesthetic properties are:

- harmony that is at one a satisfcation to our aesthetic requirements, and an assistance to the mind. (Poincaré)
- simplicity, concrete specificity and unexpected or surprising intergation or connection of disaparate elements. (hersh and john-steiner)
- proportion, order, symmetry (plato)

For Kant, it's the second one: there is no intrinsic beauty, but only representations of beauty. They are beautiful because these representations strengthen both the understanding and the imagination.

Other, more contemporary representatives, are: Breitenbach, McAllister, Sinclair, Rota. "It is enlightenment, not truth, that the mathematician seeks."

Aesthetics do not need to be exclusively about perception, it can also just be pleasurable in the judgment given (e.g literature and perception?)

Breitenbach argues that "in searching for beauty, scientists aim for theories that provide understanding" (Breitenbach, 2013)

They say that understanding is "the recongition of the fitness of the parts to each other and to the whole" and that aesthetic harmony is essentially the same. Heisenberg: "The experience of the beautiful becomes virtually identical with the experience of connections either understood or, at least, guessed at.". Also is understanding immediate (insight, aha, enlightenment) or is it gradual (working through, skill based)

In the case of a mathematical demonstration, fitness of relations happens when it is clear what the whole idea of the demonstration is, and how each part's contribution is essential. _gestalt_. The fitness is good when the relation is considered _basic_: what does basic mean in source code? idiomatic? appropriate? straightforward? clear?

He gives the example of Atiyah, who proved things once, but didnt really get it, then proved it differently and it dawned on him.

A beautiful theorem is also one which "connects significant ideas" (hardy)

Example: Euler's identity: `e^(i*PI) = -1` as the great connection of relations (algebra, geometry, analysis)

And then also that beauty is a driver for discovery -> beneficial in the process, like ethics in programming. it allows to sift through the noise (compression?). it's a heuristic. In the end, the beauty in math is not an end in itself, rather it is instrumental.

That is, in the end, epistemology cannot be completely separated from aesthetics, and vice versa