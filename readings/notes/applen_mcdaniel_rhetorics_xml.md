# the rhetorical nature of xml

## applne, mcdaniel

---

We feel that one way of describing the work of symbolic-analysis is: 1) identifying what constitutes relevant and meaningful information, 2) breaking this information down into specific elements, 3) providing names for these elements, and 4) contextualizing these elements of information to best meet the rhetorical needs of their audiences.

A lot of it is about knowledge management, tacit knowledge, knowledge creation, etc.[[ideals#modes of knowledge]]

- Tacit Knowledge to Tacit Knowledge by Socialization
- Tacit Knowledge to Explicit Knowledge by Externalization
- Explicit Knowledge to Explicit Knowledge by Combination
- Explicit Knowledge to Tacit Knowledge by Internalization

Learning progresses from tacit ignorance, explicit ignorance, explicit knowledge, reaching tacit knowledge

xml, semantic web, etc.

description, naming, cognitive implications

Berners-Lee sees XML as the universal coding language that allows us to search for XML-tagged information based on the semantics used to tag the information, tags such as <barometric_pressure>.

weird setup between programming and markup?