# The Technology of Enchantment and the Enchantment of Technology

## Alfred Gell

---

the aesthetic approach should look at the art object __in and of itself__, but without falling into the trap of hallucination

-> considering art as a component of technology, because they are _beautifully made_, or _made beautiful_, therefore a certain level of _excellence_

the art system contributes to the conciliation of individual self-interests and collective existence (so called __propaganda of the status quo__)

> As a technical system, art is orientated towards the production of the social consequences which ensue from the production of these objects. 

> Art is a technically appropriate pattern for its intended use of dazzling and upsetting the spectator.

This means that there is an _impressive_ effect rooted in technical excellence upon the psyche of the receiver. And this technical excellence creates, in turn, a magical halo-effect of "technical difficulty". It is a display of artistry/technicity only attributable to magical means ("their becoming, rather than their being").

Simmel argues that the value of an object is in proportion to our difficulty in acquiring it, and this translates into the "halo-effect of resistance", or:

> the difficulty I have in mentally encompassing their coming into being as objects in the world accessible to me by a technical process which, since it transcends my understanding, I am forced to construe as magical. 

> The point I wish to establish is that the attitude of the spectator towards a work of art is fundamentally conditioned by his notion of the technical processes which gave rise to it, and the fact that it was created by the agency of another person, the artist. 

But he also only applies this reasoning to the work of art, and not to the "merely beautiful"

In non-western societies, art has two roles: political rituals (power + supernatural forces) & ceremonial/commercial exchange

in the definition of technical virtuosity must be included considerations of aesthetics: the style, the ability to come up with appropriate forms, is a kind of technical facility that involves mastery.

And then he wraps up with a connection to magic, specifically __magic as the ideal technology__:

> All productive activities are measured against the magic-standard, the possibility that the same product might be produced effortlessly, and the relative efficacy of techniques is a function of the extent to which they converge towards the magic-standard of zero work for the same product. [...] Just as money is the ideal means of exchange, magic is the ideal means of technical production.

Magic represents the technological domain in an enchanted form.

> "we are fascinated because we are essentially at a loss to explain how such an object comes to exist in the world."