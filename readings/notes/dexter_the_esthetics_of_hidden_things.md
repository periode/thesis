# The Esthetics of Hidden Things

## Scott Dexter

---

It is about the fantasy of digital humanities:

> The signal defining characteristic of the modern computing device is that its hardware provides very little constraint on what it will do. Most of those constraints are provided instead by code produced by extremely fallible humans – this structure is exactly why ethicist James Moor characterises computers as ‘malleable’, and loads much ethical complexity onto that term. Yet most accounts of the limits – and potentials – of computing deny, more or less vociferously, the role of the embodied human in realising these limits and potentials. 

looks at three arcs that all have a machine-human tension and all have a "show me the code" dynamic

- turing paper
- mechanization of computer memory
- rise of OOP


### turing machine

> A few years ago, when very little had been heard of digital computers, it was possible to elicit much incredulity concerning them, if one mentioned their properties without describing their construction. (Turing 1950: 448)  

turing did not abstract the computation machines available at his time; rather, he mechanized the practice of existing human computers


### memory

he goes through a history of technology as memory device, and how the properties of the computer have made computing memory completely unrelated to human memory (hence, the term memory applied to computers is only a metaphor -> it's really just a dynamic array)

it reminds me of kittler in `protected mode`

### oop

that's the dominant metaphor in PL today

then her argues that the object based model of computations and a sort of social order deepened with C++ (everything private by default, untouchable)


### conclusion

the esthetics of hiding is not to remove, but to make some other mark. not a vertical tactic but a horizontal one