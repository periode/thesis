# the philosophy of architecture

## roger scruton

---

somewhat of an excluding point of view, mostly focused on italian churches.

his point is nonetheless that architecture is about practical understanding. but really what he means by understanding is the understanding of the rules and conventions upon which this building is built.

and also about the fact that aesthetic value of architecture is derived from __appropriateness__ (which, for some people, mean "optimal solution", p. 228), because "_the way a form is produced may persist as a characteristic of a form's appearance"_. p.219

and that appropriateness cannot be confined to just aesthetic judgment, but becomes altruistic.

which brings the __question__: what is a "significant detail" in sourec code? the significant detail is that which is the counterpart of structure, where the individual has more freedom for expression. in source code, that detail might actually be linguistic.

> A style is not the invention of one man only, and has value only when it is recognizably 'right' to others besides oneself. Only then does style fulfill its role in giving order to otherwise nebulous choices, of situating primitives preferneces in a framework of enduring possibilities. Style ennobles choices, giving them a significance that otherwise they lack.

The significance of style is social. If it's well-done, it means that someone cared.

> Convention, by limiting choice, makes it possible to 'read' the meaning in the choices that are made [...] for style is used to 'root' the meanings which are suggested to the aesthetic understanding, to attach them to the appearance from which they are derived.

(for instance, ruby on rails: you are in a rails application, and that reduces the range of activities that are done by this application)

"the subject of a representational work of art is also the subject of the thoughts of the man who sees or reads it with understanding: to enjoy the work is therefore to reflect upon its subject."

more than representative, architecture can be __expressive__: expressive of sadness, expressive of cleanliness, etc. expression is more like a display of atmosphere, an abstract presentation of character, reference without predicate.

if he talks a lot about facades and visual effect, the issue is that we do not SEE all of source code at once, maybe the directory structure, at most.

In terms of architecture as language, it requires _grammar_ and _intention_, which is not so much the case in arch -> the patterns is a more useful way to thing about it. because there is some sort of 'syntax': way things are being built together.
