# the psychological study of programming

## b.a. sheil

### https://dl.acm.org/doi/10.1145/356835.356840

---

in general, it just talks about how the empirical methodology is quite bad to figure out real results.

[schneideman](https://www.sciencedirect.com/science/article/abs/pii/S002073737780014X) shows that it is _hard_

---

from Xerox PARC

the point is to focus on "performance" -> utility/functionality

> As practiced by computer science, the study of programming is an unholy mixture of mathematics, literary criticism and folklore.

[Shneiderman's book: Software Psychology](https://www.cambridge.org/core/journals/applied-psycholinguistics/article/abs/software-psychology-human-factors-in-computer-and-information-systems-ben-shneiderman-cambridge-mass-winthrop-1980-pp-xiv-320/02C299AEFDDF6D301CBCB2B5AD1C6E57)

research on programming is on:

- notation (PL, control structures) -> __the most relevant__
- practices (tools, comments, flowcharts) -> _less relevant_
- tasks (learning, coding, debugging)
    - Mayer, 1979 looked at learning programming, and students seem to learn semantics based on models that are mechanical
- management (the human/company part)

it has a ref on how GOTO has been _measured_ to be harmful -> https://bpspsychub.onlinelibrary.wiley.com/doi/abs/10.1111/j.2044-8325.1977.tb00363.x?casa_token=uGyYhgbhc6cAAAAA:h0Lrb_ES-vWSkeJ3pEljgyITd7OuGpdOqL6pNOhIp-rOZRM-SXENjVpOjterrXxHu-yhR4jONtqwFQ

there is also a large practice effect that is observed (the more you do, the better you become)

> The syntactic constructs [of a PL] are appropriate to that approach, but they are not themselves that approach. Therefore there is no reason to  believe that their presence or absence will, by itself, have any significant impact. Either the programmer understands the structured approach to
programming, in which case her code will reflect it {whether or not structured control constructs are available), or the programmer does not, in which case the presence of syntactic constructs is irrelevant.

1. it's hard to find correlation between constructs and languages
2. the errors go away with practice
3. programmers have organized knowledge bases

https://dl.acm.org/doi/abs/10.1145/359763.359800 -> statically typed languages are marginally less error-prone (makes sense)