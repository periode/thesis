# la métaphore vive

## paul ricoeur

### éditions du seuil, 1975

---

---

#### préface

à la base, la métaphore est une théorie de la substitution -> il veut démontrer que c'est faux, que la métaphore se nourrit dans les deux sens.

3e étude: du mot à la phrase

4-5e étude: rhétorique de la métaphore dans le structuralisme

6e étude: introduction du concept de la ressemblance (imagination productive, air de famille, etc.)

7e étude: métaphore et référence: rapport entre goodman et black, rapport entre métaphores (art) et modèles (sciences)

8e référence: la métaphore en tant qu'outil (pertinent) des sciences philosophiques (et donc cognitif?)

#### 3e étude

l' **énoncé métaphorique** (et non plus simplement la métaphore), c'est toujours *la production de sens* (p. 87)

départ de l'opposition entre sémiotique (les signes) et la sémantique (la phrase) p.89

le linguiste de la langue par des unités différentielles (tokens, in PL) et finit à la langue

le linguiste du discours commence par le discours, et voit comment le séparer en unités (human programmer) p. 91

**les couples du discours**:

- évènement (temporaire, actualisé) <--> sens (atemporel, répété)
- fonction identifiante (description, spécifique, identifiable) <--> fonction prédicative (action, universel)
- locution (intended) <--> illocution (unintended)
- sens (ce qui est dit, relations de signes intra-linguistiques, sémiotique) <--> référence (ce sur quoi on parle, phrases des choses dont on parle, sémantique)
- référence à la réalité <--> référence au locuteur
- paradigmatique (flexions, dérivations) <--> syntagmatique (contexte)

la rhétorique est une étude de la (mé)compréhension et des remèdes apportées à celle-ci (cf. bogost). il faut *une connaissance des critères de la compréhension*. par exemple:

- vivacité
- clarté
- beauté
- précision
- expressivité

> à cet égard, le language poétique et le language technique constituent deux pôles d'une même échelle: à une extrémité règnent les significations univoques ancrées dans les définitions; à l'autre, le sens ne se stabilise en de hors du "mouvement entre significations" (p.103)

la rhétorique traite peut etre que des problemes de surface, mais la métaphore touche à des profondeurs, parce qu'elle effectue des *transactions entre contextes* (code-domain and problem-domain?). ces changements de contextes sont eux-memes des occasions de percevoir, penser, sentir une chose en termes d'une autre (i.e. action cognitive), elle offre un *insight*

**consitution de la métaphore**: le mot est le *focus*, le reste de la phrase est le *frame* (p.111) c'est son *contenu cognitif*

la métaphore est soit connaissance, soit plaisir, soit banalité (elle peut etre plein de choses!)

rappel: *la signification explicite d'un mot est sa désignation/dénotation, sa signification implicite, sa connotation*

> la littérature, précisément, nous met en présence d'un discours ou plusieurs choses sont signifiées en même temps, sans que le lecteur soit requis de choisir entre elles. [...] "une oeuvre littéraire est un discours qui comporte une part importante de significations implicites" (p.118)

beardsley parle de tension entre métaphore qui crée l'ambiguité et métaphore qui crée de la plénitude. et elle *transforme une propriété réelle ou attribuée en un sens*. -> propriété matérielle? cette transformation, néanmoins, est celle du lecteur.

**IMPORTANT**

> la signification, en ce sens, c'est la projections d'un monde habitable (p.119)

re: habitability

**END IMPORTANT**