# making sense

## simon penny

---

chap 9

p. 122 -> the archeology of knowledge, foucault (overall move to post-modernism)

gilbert ryle, the concept of mind

agre "a theory of cognition based on formal reason works best with objects of cognition whose attributes and relationships can be completely characterized in formal terms" -> maturana "the map/territory problem"

information is relativistic and relational

refs on embodied thought: william james, jakob von uexkull, john dewey, whitehead, ryle, schopenhauer

extended mind hypothesis of clark and chalmers (1998)

p. 185 -> _implementation details_ is a phrase that stands for an entire body of disciplinary rationalizations to justify the disembodiment  of AI (Herbert Simon, _The Science of the Artificial_, 1996)

> the meaningful is not in our mind or brain, but is instead essentially worldly. The meaningful is not a model—that is, not representational—but is instead objects embedded in their context of references.

knowledge derives from active exploration (p.207)