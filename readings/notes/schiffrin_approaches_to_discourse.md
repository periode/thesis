# approaches to discourse

## deborah schiffrin

### blackwell publishing, 2008

---

discourse analysis switches from a focus on self (intention) to interaction (social construction of what is said), to a focus on semiotic systems shared (language, culture, society).

#### chap 2 - defining discourse analysis

two broad approaches:

- formalist/structuralist
- - kinda modern, code, grammar, universal, uniform
- - mental phenomenon, chomsky
- functionalist/interactive
- - kinda postmodern, ethnographically appropriate analysis, speech-act communities
- - social phenomenon, halliday

and they differ on the unit of language (e.g. the sentence) and on the focus (e.g.  stylistic or social functions)

structural analyses focus on the way different units function in relation to _each other_, but they disregard the functional relations with the context of which discourse is a part of. (this is a problem, and i don't want to stick to pure structure). rather, they focus on __units__. those units (whether form based, morpheme, or meaning based, referential and conjunctive). coherence remains the responsibility of the interpreter's knowledge of the state of affairs.  (p. 24)

this unit-based approach does help to capture the sorts of internal  does help to capture the sorts of internal dependencies that we expect texts to have. p. 27

in terms of unit, it's important to identify the question or initial prompt, in order to then put it in relation with the response/statement/body of text, since this question also has implications. (p.29)

> What we need to capture in a defintion of discourse is the idea that discourse analysis imposes its own set of phenomena, its own problems and puzzles—and can discover its own regularities—in addition to those that it "inherits" from lower-level parts of discourse and those based in the way language is a social practice "determined by social structures" p. 39

> We can sugest that discourse arises not as a collection of inherently contextualized units of language use. (p.39)

the approach of the author is not so much to focus on language structure or on language use, but rather on _utterances_, units of language production that are inherently contextualized. These have pragmatic and semantic goals: how does the organization of discourse, and the meaning and use of particular expressions and constructions within certain contexts, allow people to convey and interpret the communicative content of what is said? how does one utterance influence the rest of communication?


#### chap 6 - pragmatics

pragmatics is the study of signs to interpreters: how particular discourse units are interpreted in particular ways by particular people (who can share common imaginaries/context) -> turning syntax into semantics (p.191)

__the relationship of signs to their users__

speaker meaning: what is the relationship of the signs used (their literal, conventional meaning), to what the speaker intends to say?

this is resolved by the _cooperative principle_, which assumes that the implied speaker meaning (implicature) will be resolved by the listener, _in a mutually accepted direction_ (p.194)

Givón: the references used by the speaker are established by said speaker, and therefore the speaker controls, through the _referential intent_, the grammar of reference. (whether it is synchronous or asynchronous communication)

markers:
- definite
- indefinite
- explicit
- implicit

This work of references is understood to happen in sequences, in which meaning develops through first occurences, second occurences, etc.

> Stories are useful texts in which to analyze referring sequences. In telling a story, a speake constructs a story world in which a limited number of entities act and interact with one another in a defined location and for a limited period of time. (p. 204)

> A first-mention is interpreted relative to textually- and contextually-provided background assumptions about shared knowledge: first-mentions are relevant to information based in H's knowledge of context.

It is based on CP, which assumes that he hearers can how to organize and use information offered in a text, along with background knowledge of the world (including knowledge of the immediate social context)