# the language of programming: a cognitive perspective

## Fedorenko, Evelina; Ivanova, Anna; Dhamala, Riva; Bers, Marina Umaschi  (2019). 

### doi:10.1016/j.tics.2019.04.010

---

---

it used to be that programming was always grouped with math and STEM, but there could be an alternative:

> The key idea is this: when you learn a programming language, you acquire a symbolic system that can be used to creatively express yourself and communicate with others. The process of teaching programming can therefore be informed by pedagogies for developing linguistic fluency.

> Whereas the technical meanings of [syntax and semantics] in linguistics and CS differ, their usage highlights that both natural and programming languages rely on meaningful and structured representations

the process is the same. in the comprehension, you start by units and you build a big picture. in generation, you start with big picture and end with unit

it has also been shown that using unintuitive identifiers makes it hard to comprehend, thus still linking to language (at least on a *surface* level)

similarities:

- ML people use NLP techniques on code
- had been shown that knowing programming helps in acquisition of reading ability
- kids' sequencing abilities improve dramatically after receiving coding intervention ([related study](https://link.springer.com/article/10.1007%2Fs40692-019-00147-3))