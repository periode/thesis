# the mathematical unconscious

## seymour papert

---

He starts from Poincaré to highlight the functional role of aesthetics.

This is actually a good point: there is a difference between a functional role and an epistemic role; an epistemic role can be a functional role. What are other functional roles? Heuristic. Social. Pleasurable (I guess pleasurable is often tied to aesthetics. The one from Caillois: the thrill. The thrill of aesthetics).

He sides with Poincaré on the fact that there is no "purely cognitive" theory of mathematical thinking.

For him, the back and forth between conscious and unconscious is facilitated by aesthetics.

This is the piece which describes the non-mathematicians' pleasure at finding a representation of the proof that `sqrt(2)` is irrational.

Concepts of mathematical, nonmathematical, extramathematical.

> Thus the functional exploits the aesthetic.

Gestalt vs. atomistic: these are two comparative aesthetics, with different values for different individuals.

But, ok, what do we do with the fact that there is not really a sequential aspect? Then we reunite it with the concept of __refactoring__