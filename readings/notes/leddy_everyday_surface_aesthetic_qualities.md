# everyday surface aesthetic qualities: "neat", "messy", "clean", "dirty"

## thomas leddy

---

it is an entire class of qualifiers, related to surface insofar as they "do not heavily influence underlying form or substance". but I say they might affect perception thoseof.

can be applied metaphorically and can be applied literally (identify the delimitation)

> To say that something can be cleaned implies that there is something underlying that is worthy of cleaning.

e.g. "messy" is not always a negative trait, it is first and foremost qualitiative

these qualities are most often discussed in everyday settings (though they do exist in more formal art settings)

one of the main functions of cleaning is the revelation of underlying form or structure

> why not just say that some aesthetic qualities are found primarily in art, and others primarily in everyday life?

this might be because the ESAQ are rather rudimentary, rather than refined

aesthetic concepts are (according to Sibley):
- often based on, or appeal to, non-aesthetic concepts
- perceptual
- determined by taste pereceptiveness
- not conditions not rule-governed

the author then goes on to prove that each of the ESAQ fit the above. hence they are aesthetic qualities indeed.

in cleanness, one can appreciate the clean result, but also the _process of cleaning_

then he proves that ESAQ are also aesthetic by fititng it into Beardsley's symptoms of the aesthetic (object directedness, felt freedom, etc.)

and then finally goodman, focusing on __exemplification__

(exemplification is _possession + reference_)

his main point is that cleanliness exemplifies, and i would add that it does so even further when it is coupled with an actual language of art.

but how precisely? the configuration of discreete symbol systems can be so arranged as to _reveal structure_ in a somewhat first-level, obvious way

and finally he makes the point of having these ESAQ developed at a beginner-level, and might afterwards develop into expert-level