# models of programming

## ashton wiedersdorf

### [link](https://lambdaland.org/posts/2021-09-25_models_of_programming_draft2/)

---

one mode of programming is _imperative_, giving instructions to the computer one at a time. good but tedious and detail prone

another one is using functional programming to build up models of the problem at hand ("the fundamental nature of the problem")

he likes _mathematically-sound models_. what does that mean? essentially lambda?

this book is like sicp, [how to design programs](https://htdp.org/)