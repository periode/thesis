# thèse nadja cad

## nadja guaudiliière

---

### chap 4

billi hillier, [space is the machine](https://discovery.ucl.ac.uk/id/eprint/3848/1/SpaceIsTheMachine_Part1.pdf), developed the concept of space syntax

and what about shape grammars?

ingénieurs (bureau d'études techniques):

- frei otto
- cecil balmond
- ove arup

code source made available from the exhibition [scriptedbypurpose](http://scriptedbypurpose.net/)—also there is casey reas and marius watz as exhibiting artists

> les pratiques computationelles se placent sour le patronage de Peter Eisenman ou de Bernard Tschumi, architectes chez qui le diagramme occupe une place majeure

question re: "genware" -> what is it ? source code documentation?

> Mais plus largement, Sanchez décrit également la difficulté à faire correspondre complètement l'intention architecturale initiale et le résultat produit par l'algorithme.

encore un exemple de la tension intention/implémentation

ref chap 1: qu'est-ce qu'une architecture pertinente?

_heuristics programming_ pour le cas de l'archi computationelle, mais aussi possiblement pour la code poetry?

### chap 5

genres: agent-based, growth systems, genetic algorithms, physics simulations -> more or less beautiful implementation?

apprently architects also like OOP

Maya -> MEL language?

check forums rhino script and forums mcneel

ezio blasetti, tobias schwinn, kyle steinfeld, iain maxwell -> programmeurs

il y a un parallèle entre:

- bazar et cathédrale (eric raymond)
- grasshopper et generative components


also issues with interfaces! they make it easier? harder?

mais aussi intéressant de voir l'hybridisation des modes d;interfaces: dans rhino, tu peux cliquer sur des elemnts visuels, mais aussi taper des mots directement. le concept de couches qu'elle développe se rapproche de mon concept d'évolution spatiale