# neuroscience of aesthetics

## anjan chatterjee and oshin vartanian

### annals of the ny academy of sciences

---

this paper demonstrates the sensory-motor/emotion-valuation/meaning-knowledge systems

*an aesthetic judgment is broadly defined as evaluative appraisal of objects*

- efficient causes: understanding the trigger (does the face cause an aesthetic reaction?)
- - is code beautiful? actually yes, it can be subject to aesthetic judgment
- final causes: an explanation regarding the purpose of the aesthetic effect/experience
- - beautiful code can demonstrate *various kinds of understanding*
- formal causes: that explain the relation between efficient cause and final cause
- material causes: explanations of the substrates that give rise to it
- - the actual manifestations/signs/concrete evidence

> According to fluency theory,79,80 positive aesthetic experiences are driven by processing ease (p.177)

wtf is fluency theory? (graf and landwehr, fluency based aesthetics)

> These studies suggest that what we attend to in the course of aesthetic interactions with artworks is strongly affected by our knowledge of compositional strategies, stylistic conventions, and practices.
> In other words, the extent to which we are able to distill the semantic properties of artworks beyond merely their sensory qualities affects the engagement of neural systems in the service of aesthetic experiences.

difference between aesthetic **judgment** (from the process of understanding) and aesthetic **emotion** (from the ease of acquisition/process)

> Our integrated view builds on models that frame aesthetic experiences as the products of sequential and distinct information-processing stages, each of which isolates and analyzes a specific component of a stimulus (e.g., artwork)

aka different steps (look, understand, enjoy)

aesthetic responses in maths are devoid of sensations?

model of aesthetic experience (Leder et al):

- perception
- implicit classification
- explicit classification
- cognitive mastering
- evaluation

> One of the most reliable findings to emerge from empirical aesthetics is that expertise and formal training in the arts influence aesthetic experience, quantifiable both in terms of subjective ratings and viewing patterns measured by eye tracking.