# strategies of discourse comprehension

## teun a van dijk & walter kintsch

---

general framework for understanding discourse

another book by on the authors relating understanding, discourse and knowledge:

DISCOURSE AND KNOWLEDGE. A SOCIOCOGNITIVE APPROACH. (Cambridge University Press, 2014).  This book offers a multidisciplinary study of the relations between discourse and knowledge, premised on the assumption that discourse can only be produced or understood on the basis of vast amounts of  socially shared sociocultural knowledge, and that most of that knowledge is in turn acquired through text and talk. After a theoretical chapter presenting a theory of 'natural' knowledge as a task of epstemology, the other chapters review and discuss the relations between discourse and knowledge in cognitive and social psychology, sociology, anthropology, linguistics and discourse studies. 

---

### overview of the model

levels of language use:

- morphology
- syntax
- semantics
- pragmatics

(but these various models interact in intricate ways)

strategic analysis depends not only on textual characteristics, but also on characteristics of the user (e.g. their goals, world knowledge); as such, the standards of beauty applied might also depend on these characteristics? even though it's always about the same material.

strategies are the procedural knowledge we have about understanding discourse.

understanding is the semantic (but also actionable) representation of the input discourse in episodic memory.

the difference between **intentional** (meaning, what it is conceptually) and **extensional** (referential, what it is practically)

strategies (things deploy to better realize the goal: understanding):

- micro
  - proposition
  - local coherence
- macro
  - gist/upshot/theme/etc.
  - are flexible and have a heuristic character
  - schematic: they are superstructures that are conventional/culturally variable (language paradigms?)
  - **stylistic** strategies: the strategic use of stylistic markers to infer many properties of the speaker (programmer) or the social context (use-context). in particular, there are *rhetorical* strategies, used to enhance the effectiveness of discourse and communication/expressivity.

> Rhetorical strategies do not lead to the construction of semantic representation, but they help in this process. Figures of speech may attract attention to important concepts, provide more cues for local and global coherence, suggest plausible pragmatic interpretations (e.g., a promise versus a threat), and will in general assign more structure to elements of the semantic representation, so that retrieval is easier. 

**questions**:
- are there different strategies from a listener point of view, and a speaker point of view?
- are there non-verbal cues in programming?

*production* strategies aka undertstanding of a discourse also needs to be activated.

### existing empirical research

the top down approach is proven:

> The most basic result that is of importance for us here is that the perception of letters is influenced by our knowledge about words; that the recognition ofwords is influenced by the sentence context in which they are presented; and that sentence processing itself is determined by the status of the sentence in a text

good readers use context more efficiently than poor readers

coherence is just argument repetition (the more repeated things in both propositions, meaning the overall discourse is more coherent the more things they share) (p.65)

#### the notion of strategy

the notion of strategy is a cognitive strucutre (like a plan) about the ideal way to reach a particular goal. (notions of (mental) action, goal, optimality)

funny, they define the relationship strategy<>tactics as the exact opposite of de certeau

types of strategies:

- sociocultural strategies (larger context)
- communicative strategies (specific, localized act—reading this specific piece)
- local comprehension strategies (e.g. paragraph by paragraph), aka **propositional strategies**, building up a database of meaningful propositions from (declarative) sentences.
- local coherence strategies (starting to extract the macro structure)
- macrostrategies (extracting the gist)
- schematic strategies (figuring out which format/archtype this is part of)
- knowledge use/search strategies (recalling things from the past)


macrostructures usually have a dependence on macrorules: the meaning of the parts influences the meaning of the whole. but that's not the case in software hacking for instance (magic numbers?)

#### use of knoweldge

so it's obvious that we need world-knowledge in order to understand a specific text, but how is that knowledge represented?

- models
- concept acquisition through semantic decomposition in levels of cognitive units
- AI: frames and scripts (also goffman, no?)