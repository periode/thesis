# Buildings, Beauty, and the Brain: A Neuroscience of Architectural Experience

## Alex Coburn, Oshin Vartanian, and Anjan Chatterjee

### upenn

---

> For millennia, ancient Eastern construction practices like the Indian vaastu shastra and the Chinese feng shui offered concrete guides to creating spatial harmony and aesthetic coherence in the built environment.

architectural beauty and coherence exist

neuroscientifical studies in artistic experience/aesthetic perception have started, but haven't quite found conclusions yet.

however, three systems:

- sensori-motor
- knowledge/meaning
- emotion/valuation

how does the architectural aesthetic experience differ from the visual aesthetic? and from the coding?

> Grouping, a fundamental Gestalt principle, describes the process by which the visual system orders repeated, statistically correlated information in a scene, like alternating columns and archways in an architectural colonnade or organized pat- terns of blue and yellow hues dispersed throughout a stained glass window

> Grouped features (e.g., of color or form) trigger synchronized action potentials among associated neurons responsible for processing those features (Ramachandran & Hirstein, 1999; Singer & Gray, 1995)

paterns with more symmetries enable more efficient recognition

> More recent evidence suggests that the relationship between complexity and aesthetic preference varies as a function of how the former is conceptualized (e.g., amount, variety or organization of elements within a scene; Nadal, Munar, Marty, & Cela-Conde, 2010)

the above could explain how different types of complexity (one-liner, obfuscation, PLs), have different followings