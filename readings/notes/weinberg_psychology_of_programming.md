# the psychology of programming

## gerald weinberg

### [link](http://tapoc.sourceforge.net/progress/2014/02/notes_for_weinberg-psychology_of_computer_programming.html)

---

programming is a human performance

(5-6) Programming is, among other things, a kind of writing. One way to learn writing is to write, but in all other forms of writing, one also reads.

(6-7) We shall need a method of approach for reading programs, for, unlike novels, the best way to read them is not always from beginning to end.

there are programming language limitations and programmer limitations

(12) in most cases, we do not know what we want to do until we have taken a flying leap at programming it.
(12) Specifications evolve together with programs and programmers. Writing a program is a process of learning—both for the programmer and the person who commissions the program.

### what makes a good program?

- specifcations (asymptotic perfection)
- schedule (longer dev time but better result)
- adaptability
- efficiency

(22) When we ask for efficiency, we are often asking for “tight” coding that will be difficult to modify. . . . Asking for efficiency and adaptability in the same program is like asking for a beautiful and modest wife.

### How can we study programming?

- introspection
- observation
- experiment
- psychological measurement
- behavioural science data

### programming as social activity

assemblages, groups, etc. with formal and informal organization, physical and networked environment

error and ego -> egoless programming

(59) he raised the point that he had worked throughout as he always did in his own group—always with an eye to making the program clear and understandable to the person or people who would ultimately have to read it. This insight indicates that all the advantages of egoless programming are not confined to the detection of errors, though that was perhaps the earliest and strongest motivation for adopting the technique. In fact, it might be useful to examine our four factors in good programming in light of what effect this method would have for them.

(61) To a large extent, we behave the way we see people behaving around us, so a functioning programming group will tend to socialize new members to its philosophy of programming.

the rest is a lot of organizational/management topics

### programming as individual activity

Division of individual differences into categories of personality, intelligence, training, experience.

(122) Although some professional programmers may indeed be no more than hacks camouflaged by esoteric obscurities and some amateurs might be able to gain a deep appreciation of programming through the writing of a single short program, there is a difference between professional and amateur

(122) Because the amateur will be the user of his own program, he has the choice of doing his thinking either before or after programming. (that's not the case for the professional one)

(125) Indeed, it is a homily that the difference between the professional and the amateur programmer lies in the superior past experience of the professional. But one could also contend that an equally important difference lies not in the programs each has previously written, but in those he will write in the future.

(127) The moral of this tale—and a hundred others like it—is that each program has an appropriate level of care and sophistication dependent on the uses to which it will be put. Working above that level is, a way, even less professional than working below it. If we are to know whether an individual programmer is doing a good job, we shall have to know whether or not he is working on the proper level for his problem.

Strata -> space -> architecture

(132-133) Programming is often described as a process moving from problem definition through analysis to flow diagramming, then coding, followed by testing, and finishing with documentation. Although this rough view contains some truth, it distorts the truth in several ways. First of all, the actual sequence is not so fixed, because, for example, documentation may precede testing, coding, flow diagramming, and even analysis. Secondly, not all steps need to be present, as when we are recoding a program for a new machine or language. Thirdly, it need to be a sequence at all—and, in actual practice, rarely is.

### personality factors

he argues one can find personalities revealed in the code

### intelligence or problem-solving abilities

Psychological set and distance can be impediment to error location activities; syntax highlighting and other visual cues of the interface help counteract by offering machine response.

(162) For certain types of error location activities, psychological set proves a major impediment. Numerous experiments have confirmed that the eye has a tendency to see what it expects to see.

(162) Related to the concept of “set” is the concept of “distance.” Not all misreadings are equally likely, regardless of the set of the reader. I can be a reader with a given skills, the distance involved in my (mis)readings will have different levels of impact.

(164) The whole idea of a comment is to prepare the mind of the reader for a proper interpretation of the instruction or statement to which it is appended. If the code to which the comment refers is correct, the comment could be useful in this way; but if it happens to be incorrect, the set which the comment lends will only make it less likely that the error will be detected.

Key to intelligent behavior involves flexibility manipulating assumptions and applying formulas to solve problems.

(165) It will always be difficult to appreciate how much trouble we are not having, just as it will always be difficult to appreciate a really good job for problem solving. Once the problem solution has been shown, it is easy to forget the puzzlement that existed before it was solved. This is why aesthetics come to the background?

(167) Memory helps a programmer in many ways, not the least of which is by enabling him to “work” on problems when he does not have all his papers in front of him. Different levels of programming (see: [the programmer's brain](https://www.manning.com/books/the-programmers-brain))

Good programmers are made, not born, so emphasize creating them rather than selecting them; we can make good programmers by adjusting personality, work habits and training.

Love of programming is the biggest motivator.

### learning programmin

(189-190) For beginners, or amateurs, the difficulties with syntax, keying, and such auxiliary operations as job control usually far outweigh any deeper difficulties that might arise from subtle semantic points or from the problem itself.

how to learn programming? "know thyself"