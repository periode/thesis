# understanding: art and science

## catherine z. elgin

### philosophy and the arts: midwest study of philosophy

---

To highlight, underscore, display or convey involves reference as well as instantiation. An item that, at once refers to and instantiates a features may be said to exemplify that feature. e.g. a class cna exemplify encapsulation; a sample problem worked out in a textbook exmplifies reasoning strategies to be used in the course.

XXXX exemplifies code's capacity to YYYY

Therefore, an example affords epistemic access to the features it exemplifies. But not always! something can be an instance of something else, but not be telling/provide no epistemic access to.

The features a symbol exemplifies depends on its function (or, more precisely, its functional context): a symbol can perform a variety of function: a piece of code in a textbook might exemplify an algorithm, while the same piece of code in production software might be seen as a liability, or as a boring section in a code poem.