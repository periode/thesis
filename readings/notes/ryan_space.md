# space

## marie laure ryan

### living handbook of narratology

---

all narratives imply a world with spatial extension, even when spatial information is withheld. so what kinds of processes are there?

- bakhtin's polysemic concept of chronotope: how configurations of time and space are represented in language and discourse (bakhtin, "Forms of Time and the Chronotope in the Novel", 1937).

- fauconnier's mental spaces

starts  __the spatial extension of the text__. spatial extensions range from 0 dimension (oral narratives), 1 dimension (tv tickers), 2 dimension (printed page, digital page), 3 dimension (theater). of particular interest is the fact that the digital page is accessed by the hypermediated display.

the one she focuses on strongly is __the spatial form of the text__: it de-emphasizes temporality and causality through compositional devices such as fragmentation, montage of disparate elements, and juxtaposition of parallel plot elements. Particularly, in digital media:

> When the notion of space refers to a formal pattern, it is take in a metaphorical sens, since it is not a system of dimensions that determines the physical position, but a network of analogical or oppositional relations perceived by the mind. It is the synchronic perspective necessitated for the perception of these designs and the tendency to associate the synchronic with the spatial that categorizes them as spatial phenomena.

Readers gather spatial information (e.g. informed by chronotopes) into a cognitive map or mental model (fauconnier) of narrative space.

> Mental maps, in other words, are both dynamically constructed in the course of reading and consulted by the reader to orient himself in the narrative world.

Regarding the thematization of space:

> Architecturally, as well as plot-functionally, narrative space can be described in terms of the partitions, both natural and cultural, that organize it into thematically relevant subspaces: walls, hallways, political boundaries, rivers and mountains, as well as in terms of the openings and passageways that allow these subspaces to communicate [...]
