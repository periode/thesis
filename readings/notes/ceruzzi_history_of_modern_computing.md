# a history of modern computing

## paul ceruzzi

---

### chap 7 - personal computers

it started with the pdp 10 and the feeling of personal, intimate use that it gave

> The assertion that hackers created modern interactive computing is about half-right.

early computers (with intel 8008 chip) required you to write in FORTRAN

the real moment was 1974 with appearance of the altair.

it, like UNIX, became popular in part thanks to their open, non-licensed design

and then the sotware was BASIC (considered a broken, toy language), but which had interesting the interesting feature of `USR`, to write machine language and `PEEK` and `POKE` to write bytes directly to memory

second wave was the commodore PET and Apple II

as for UNIX, the students who were able to modify early versions of it, as they entered the professional world, started evangelizing about it -> the conflux of academia, commerce and hack