# aesthetics in mathematical inquiry

## nathalie sinclair

### Mathematical Thinking and Learning, 2004

---

Nathalie Sinclair: evaluative role, generative role, motivational role. the generative role is the one that is guiding and activates the process of inquiry: generates new ideas and insights (both positively and negatively, i.e. when something looks bad and needs to be fixed because we know there's a way that it can look good)

> The aesthetic capacity of the student relates to her sensibility in combining information and imagination when making purposeful decisions, regarding meaning and pleasure. This is a use of the term aesthetic drawn from interpretations such as Dewey.

She connects it with Dewey's theory of inquiry (1938) and with Polanyi's personal knowledge (psychological approach to aesthetics). For Dewey, the heuristic function of the aesthetic depends on the inquirer's qualitative apprehensions, and operates on vague suggestions of relations and distinctions rather than on firm propositional knowledge. These apprehensions give rise to qualitative thought, which is concerned with ideas, concepts, categories and formal logic. They give what Dewey has called _qualitative unity_ to objects or phenomena that are externally disparate and dissimilar. Such unity allows human beings to abstract discernible patterns in conjunction with the ideas and the concepts at hand.

#### evaluative aspect of aesthetics

> In fact, it is only in relation to actual mathematicians with actual interests and values that mathematical reality is divided up into the trivial and the important.

It is a set of shared values amongst mathematicians: styles and conventions, and idiomaticity. That can explain the diversity of aesthetic ideals at the social level, rather than as the technical level (i.e. programming languages are not the defining parameter of an aesthetic tendency)

Speaking to students in particular, there is an affective response that is clearly involved.

In terms of the aesthetic dimension of mathematical value judgments, the emphasis placed on the aesthetic qualities of a result implies a belief that mathematics is not just about a search for truth, but also a search for beauty and elegance. Asgain, different preferences indicate different value systems.

> Mathematics can tell a good story, one that may evoke feelings such as insight or surprise in the reader by appealing to some of the narrative stratagems found in good literature

This above is a similar argument to that used by Matz in order to characterize beautiful code.

#### generative aspect of aesthetics

she gives the example of papert, and his examination of non-mathematicians who find some sort of aesthetic pleasure in trying to figure out what's up with `sqrt(2)` is an irrational number. they did find pleasure, even though their skill level did not suggest that they found pleasure in the mathematical object in itself, but rather in its aesthetic presentation (as sinclair suggests, by getting rid of the _ugly_ sqrt symbol).

> The phase of playing is aesthetic insofar as the mathematician is framing an area of exploration, qualitatively trying to fit things together, and seeking patterns that connect or integrate.

Wiener:

> the mathematician's power to operate with temporary emotional symbols and to organize out of them a semipermanent, recallable language. If one is not able to do this, one is likely to find that his ideas evaporate due to the sheer difficulty of preserving them in an as yet unformulated shape.

The above relates to the psychology and beacon: find yourself somewhere, somewhere of (aesthetic and cognitive) quality and grasp on to it.

Mathematicians believe that some structures are of significance (symmetry, order closure), but so what are the significant structures in source code?