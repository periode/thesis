# forms of time and chronotope in the novel

## mikhail bakhtin

---

- bakhtin's polysemic concept of chronotope: how configurations of time and space are represented in language and discourse (bakhtin, "Forms of Time and the Chronotope in the Novel", 1937). There are particular chronotopes in particular genres, yet time and space are always tightly connected:

> In the literary artistic chronotope, spatial and temporal indicators are fused into one carefully thought-out, concrete whole. Time, as it were, thickens, takes on flesh, becomes artistically visible; likewise, space becomes charged and responsive to the movements of time, plot and history.

But this guy is strongly marxist, he looks at it historically (Greek Romance), his idea of different times and genres and can be productively adapted into writing time/reading time/execution time (the process of refactoring going from writing to reading, and the process of building going from reading to executing). Bakthin also differentiates the reader time and author time.

> A literary work's artistic unity in relationship to an actual reality is defined by its chronotope. Abstract thought can consider time and space separately, but a living artistic experience cannot.

In conclusion:
> What is the significance of all these chronotopes? What is most obivous is their meaning for _narrative_. They are the organizing centers for the fundamental narrative events of the novel.

The chronotopes also give flesh to the narrative events (e.g. 'where is a variable declared?'), due to the "special increase in density and concreteness of time markers that occurs within welll-delineated spatial areas". it functions as the primary means of materializing time in space, and it emerges as a center for concretizing representation.
