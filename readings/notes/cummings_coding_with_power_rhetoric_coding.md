# Coding with power: Toward a rhetoric of computer coding and composition

## cummings

### Cummings, Robert E. "Coding With Power: Toward A Rhetoric Of Computer Coding And Composition." Computers And Composition 23.(n.d.): 430-443. Web. 4 Jan. 2013.

---

deals with composition teachers (article: *writing teachers writing software* by paul leblanc)

*From Codex to Code: Programming and the Composition Classroom*: the benefits of examining coding as a writing practice, but it remains difficult

(432) How does the programmer write for an audience that can both operate the program as user and read the intent of the code that creates the program? this implies a **meta-cognitive sway**, the way writing, once done, affects thinking.

(433) The quality of refining an idea is one that coders often cite as a compelling and rewarding aspect of their work. Their texts also speak back to their processes of composition, as revealed in the reflections on coding in some standard texts. . . . The act of applying the logic of a programming language to a problem refines that problem, positions it in a new light, and reveals the biases or faults of the thinking that first framed the issue as a problem.

Figures of rhetorical triangle and coding triangle: Writer-Text-Reader and Coder-Program-Machine. can there be an overlap?

(436) While the student programmer is much more likely to view the machine as the audience for his or her writing, as it is the machine that must first react to the text, the professional coder is more likely to envision the software user as his or her audience.

Ong audience construction applied to code readers: According to Ong, the writer constructs his or her fictional audience to substitute the immediate response of a physically present audience. The coder must perform the same act, but instead of gauging human reaction, she or he anticipates the machine's reaction. . . . These performances to the coder's previous texts create a library of responses within his or her memory, and the coder draws upon them just as the writer draws upon feedback received from prior texts, whether or not the coder's memories were emotionally charged (like most traditional writers, coders often rebuff the idea that the rejection of their text is an emotional experience). Additionally coders, like readers of fiction, are affected by other programs they have observed.

(439) If Kittler “relates phonography, cinematography, and typing to Lacan's axiomatic registers of the real, the imaginary, and the symbolic” (p. xxvii), would the programmer's conception of his or her code not represent a commingling of these three historically contingent discursive networks? The coding process, when viewed as an act of writing, captures this “registering of the real”—without intervening authorial intent—which Kittler associated with all three technological media constructs. When the coder conceptualizes his or her desired outcome and then renders those concepts in program code, the act of programming invokes Kittler's typewriter—the reduction of imagination into barest linguistic signs.

If composition teachers were to select a programming language for inclusion into the pedagogy of teaching writing, it would need to be a stable and robust language that is freely accessible to all and that also minimizes risk of obsolescence while maximizing the applicability of the programming knowledge in other classes. Python?