# four types of textual space and their manifestations in digital narratives

## marie-laure ryan

---

1. the spatial form of the text emphasizes internal relations between parts of the text at the expense of temporal progression and of the traditional narrative effects of suspense, curiosity and surprise.

It's a mode of organization in 20th century novels, such as Ulysses, where the novel can be read as fragments. But it's a tricky reading, so "spatial form can only be appreciated only retrospectively or, because of the limitations of memory, on a second or third reading.". On the first reading, you can keep the narrative meaning, but at the expense of the depth of the text.

> But an even more medium-specific type of spatial form resides in the architecture of the underlying code that controls the navigation of the user through a digital text.

> When the choices are so numerous that the authors does not control the long-rage succession of elements, interest must shift from following a temporal development to the detection of spatial relations.