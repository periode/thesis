# aesthetics in software engineering

## bruce maclennan

### taken from [full publication](https://www.sciencedirect.com/science/article/pii/B9780444516671500422)

---

#### aesthetics for engineers

software doesn't have a long aesthetic tradition (which is where my research project comes in)

two analogies:

- structural engineering: software is large, complex, made by teams, for a useful purpose
- mathematics: abstract and formal

he draws on his 3 Es and 3Ss framework again - [source](mclennan_elegance_software_design.md)

i'm not looking at it from an end-user perspective, but from a mid-user (reader of code)

one reason we also need aesthetics is because software programs are complex *and* the tools to analyze those are still limited (testers, code verifiers, etc.). this complexity sets our intuition adrift and analytical tools don't help

there are a lot of different ways to solve a software engineering problem, so how to pick the correct solution? he states `function follows form`. **those designs make the interaction of forces manifest**

> Since aesthetic judgment is a highly integrative cognitive process, combining perception of subtle relationships with conscious and unconscious intellectual and emotional interpretation, it can be used to guide the design process by forming an overall assessment of the myriad interactions in a complex software system.

**i need to prove/show that analyzing an elegant system is easier than analyzing an inelegant one**

there is also the **ethical** set of values, a subset of engineering values in the design space. based on those engineering values, one can start making design decisions. This ethical aspect also plays in the fact that there is a *shared sense of elegance*

> ike cathedrals and scientific theories, large software projects are the result of the efforts of many people, and aesthetic standards provide criteria by which individual contributions can be objectively evaluated (Heisenberg, 1975).

correctness is required, elegance is desired

#### asoftware engineering as "platonic technology"

we can analyze in terms of *form* and *matter*. matter is the hardware, and the form is the software. because there is a clear distinction between hardware and software, this can qualify as platonic technology. at first, what matters most is the form, not the matter.

> All arts have their formal and material characteristics, but software engineering is exceptional in the degree to which formal considerations dominate material ones. (but how about literature? this doesn't have material constraints)

so if there's a lack of material embodiment, one might assume that the perceptual qualities have less to do with it (then maybe we should either (1) find other kinds of qualities or (2) redefine what matter means)

discussions of the aesthetic of mathematics:

- curtin, 1982
- farmelo, 2002
- king, 2006
- wechsler, 1988

Greek classical notion of beauty *to kallon* includes excellence and manifest fitness to purpose

2 approaches:

- beauty as the harmony, consistency and order (i.e. conformity) of the parts in relation to each other and in relation to the whole. it has a structural character, and appeals to the intellect. the parts all have essential and complimentary roles. if the parts have to conform to one another, how do we *see* that, in sensual ways?
- gestalt (the whole as congruent form).

> n the context of software engineering these archetypal forms would be, for example, complete procedures, operations, or patterns of interaction that are innate or deeply ingrained in the viewer’s unconscious mind.

> aesthetic rules are applied within an unformalizable context of practices and concerns, which constitutes the background to the principles, which are in the foreground p.12