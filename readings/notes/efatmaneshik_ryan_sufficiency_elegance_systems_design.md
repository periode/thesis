# On the Definitions of Sufficiency and Elegance in Systems Design

## mahmoud efatmaneshik, michael ryan

---
 
 The sufficiency of the solu-  tion and then its elegance depends very heavily on the context   of the problem boundary—it makes no sense to discuss sufficiency or elegance without first defining that boundar
 
---

design and elegance is a universally recognized goal for system design

elegance and sufficiency are separate properties of a system

- sufficiency is binary (it is sufficient or not)
- elegance is the lest complex sufficient solution (question of relative complexity)

complexity is then a matter of size (of problem domain) and perception (perceived complexity)

OED: pleasantly ingenious and simple

__elegance has both quantitative/practical and qualitative/abstract/romantic aspects__

which one do i choose?

abstract:
- grace
- style
- pleasing
- crative

practical:
- simple
- neat
- parsimonious

assumption: __simple is beautiful__

how does one make something complex, simple? (open question by pierre)

the rest is an almost-mathematical demonstration of sufficiency as being the one which satisfies __only__ each of the problems element within the probem domain

subjective elegance of a system reduces with time as the subject learns about the problem. so over time, as the problem is deemed more and more simple, it's harder to find a notion of elegance in a solution

and then he gives three examples:
- cutting a straight line into paper
- shaving nose hair
- fractioned satellites system