# Understanding understanding art

## Catherine Z. Elgin

### Épistémologie de l'Esthétique, ed Vincent Granata and Roger Pouivet. Presses Universitaire de Renne (2020), 139-150

---

she argues for something distinctive about art as a branch of epistemology

> An understanding, as I use the term, is a systematic, interconnected network of commitments in reflective equilibrium that is grounded in fact, is duly responsive to evidence, and enables non-trivial inference, argument, and perhaps action pertaining to the phenomena it bears on (see Elgin2017).

Works of art are subject to interpretive indeterminacy, and they do not relate to truth, but rather they point out a direction towards understanding.

> Aesthetics then is the branch of epistemology that explains how interpretively indeterminate symbols advance understanding.