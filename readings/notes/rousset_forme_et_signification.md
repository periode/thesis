# forme et signification

## jean rousset

---

il parle beaucoup d'oeuvres, peut etre un peu trop, mais il accentue beaucoup le fait que la forme soit signifiante

le lecteur *s'installe* dans l'oeuvre, transforme dans une lecture totale le livre en un réseau simulatné de relations réciproques.

> Il y a longtemps qu'on s'en doute: l'art réside dans cette solidarité d'un univers mental et d'une construction sensible, d'une vision et d'une forme.