# comité de suivi - notes

---

j'étudies des mondes qui partagent un même objet, et ces mondes sont en interférences.

**QUESTION**

qui sont mes destinataires? philosophes de l'art? practiciens?

une bonne approche ne serait pas de traiter les quatre champs séparéments, mais de les *entrelacer*, d'explorer les "effets retours", les solidarités des pratiques au-delà des différences des acteurs

**MÉTHODO**

ne pas négliger la diachronie (évolution au fil du temps), en prenant en compte l'évolution des interfaces, de la visualité.

historiquement, retracer la question du fond et de la forme, peut-être jusqu'à l'art oratoire (discours destiné à produire des effets) de la grèce antique, et le statut du beau (beau vs. productivité, beau vs. vrai)

en termes de biblio, rajouter Kenneth Goldsmith (...), trouver un ouvrage de synthèse sur la code poetry, et katherine hayles, my mother was a computer.

**CRITIQUE**

poibeau trouve que ma définition du code est trop ambigue:

- **qu'est-ce que j'aborde? quel est l'objet de mon étude?**
- de quoi est-ce que je veux parler en priorité? et en secondaire? (e.g. les impensés esthétiques des pratiques de programmation)

il faut que je me positionne face aux software studies.

**CONCLUSION**

il faut se concentrer sur la chose que je veux montrer, sur l'argument que je veux démontrer: une thèse que je vais formuler, un argument que je vais prouver en procédant scientifiquement.

les jurys évaluent une capacité à argumenter, à soutenir ce que je veux dire, ce que je veux montrer (et cela part d'une perplexité, d'une insatisfaction avec la littérature actuelle: prendre une position dialogique), mais aussi **limiter le domaine**