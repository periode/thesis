# notes

## todo

- [x] write summary
  - [x] french
  - [x] english
- [x] confirm date
- [x] write members

## envoyer lien

- olivier michel
- clemens ortrun
- nicole claude

## attendants

- pierre
- antonio
- nick
- alexandre
- yves
- winnie
- baptiste
- aleth
- olivier
- edouard
- justyna
- heloise (?)
- titien
- ida (?)
- thibault
- vadim
- aurore
- tomas
- celine
- gabriel
- arnaud
- carla
- edgar
- nadja
- nadja + 1
- nadja + 2
- benjamin
- maxime
- farid
- tobi
- christian
- celestin
- nico
- irem
- romain
- pauline
- sarah

## jury

### shortlist

- [x] [antonio somaini (Paris-3)](http://www.univ-paris3.fr/m-somaini-antonio-176677.kjsp) (HDR)
  - [x] ENVOYÉ antonio.somaini@sorbonne-nouvelle.fr
- [x] baptiste mélès (pas de HDR)
  - [x] ENVOYÉ baptiste.meles@normalesup.org
- [x] winnie soon
  - [x] SENT
- [x] yves citton (HDR)
  - [x] ENVOYÉ yves.citton@gmail.com
- [x] nick montfort
- [x] alexandre gefen (HDR)

## requirements

- [x] parité (3 F, 4 H)
- [x] au moins la moitié de HDR
- [x] la moitié externes à Paris-3

## pré-rapporteurs

- [ ] deux personnes
- [ ] externes, HDR, peuvent être membres du jury
- [ ] demander plus de détails pour ce que doivent faire les pré-rapporteurs

## lettres

### fr

J'espère que vous allez bien,

Je suis Pierre Depaz, doctorant sous la direction d'Alexandre Gefen (Paris-3) et Nick Montfort (MIT), et je vous écris aujourd'hui au sujet d'une potentielle participation à mon jury de thèse, prévu en Décembre 2023.

Le sujet de ma thèse est celui de la place de l'esthétique dans les textes de code source; particulièrement, je m'intéresse au fait que ces textes invitent un jugement esthétique au croisement de la littérature, de l'architecture et des mathématiques, et qui est étroitement impliqué dans la compréhension de ces textes.

## en

I hope you are doing well,

My name is Pierre Depaz, and I'm a doctoral student working on the aesthetics of source code, under the direction of Alexandre Gefen (Paris-3) and Nick Montfort (MIT). I am writing today to ask whether you would be interested to be part of my jury defense, which would take place in late December 2023 in Paris.

My dissertation focuses on the role of aesthetics in the understandings of source code; particularly, I look into how source code texts relate to the contingent aesthetics of literature, architecture and mathematics, and how aesthetics are related to cognitive processes in the reading and writing of code. You can find a summary of the work attached to this email.

Given your existing body of work in similar research areas, I would be honored if you would agree to be part of the PhD jury.

I'm at your disposal if you have any questions, and am looking forward to hearing from you,
Best,
