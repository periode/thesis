# questions

## actual questions

### alexandre gefen

- quid d'une histoire culturelle de la programmation? et des langages de programmation?
- il y a aussi le rôle des frameworks, des IDEs
- étude intertextuelle des codes? comment un extrait peut naviguer, d'exemple à texte de programmes.
- il aurait été intéressant de s'intéresser aux styles cognitifs? programmation fonctionnelle, déclarative, etc.
- il aurait fallu se positionner d'avantage dans le rapport entre la philosophie esthétique  et la cognition, surtout au niveau esthétique (jean-marie schaeffer, etc.)
- __surtout__ il aurait fallu se poser la question du point de vue de la machine, questionner le rapport à la performance, considérer la machine comme un actant

complètement d'accord avec toutes les questions, mais c'eut été sortir des limites de cette thèse, et ce sont des super pistes de projets à venir.

### nick montfort

in the context of LLMs: "the birth of the computer is the death of the programmer"

- how do you reconcile the DRY principle of programming with the repetitive and expansive output of the generated?

when there is an articulation between the very concise code and the very expansive output is when software art happens: forkbomb, all the names of god, etc.

- how come people do not want to make their code public, due to the fear that it is ugly ?

because we don't read often other people's code, and therefore we assume that other people's code is always better than one's own. so that might be a switch in pedagogy: maintaining rather than creating from scratch. and because the aesthetics of code are often linked to the skill of the writer, people are also afraid to be judged when putting it out there.

### yves citton

- why did you not include people such as jodi/rbyn in source code aesthetics?

because i consider them as being _source code fetishism_ rather than actual source code. it's interesting what they represent, but i don't think they represent what programmers work with

- habitability: what are the structures that the end-users inhabit?

i didn't push this far, but this is definitely an interesting question and this work is a prelude to answering it. by separating from what is the consequence of source code, and what is the consequence of shareholder value, we could identify a bit more clearly those structures that are afforded by the digital.

- what about a different conception of aesthetics? you seem to be very rooted in a 18th century definition of aesthetics. some additional references could have been brian massumi, mark hansen and the sublime, a bit more about materiality, luciana parisi about the uncomputable, etc.

i would rather focus on the very fundamental definition of aesthetics, even though this was a hard path to tread, in order to include very broad elements in the corpus, instead of trying to retro-fit a particular aesthetic theory ontop of something that we still don't know the kind of aesthetics that it has.

### winnie soon

she found the architectural reference very good

- how do you deal with the tension between textual aesthetics and performative aesthetics?

i was not really ready for this question, but probably said something along the lines of having to imagine the performance of the computer in a human imagination is a kind of performance, the score of which is made of lines of source code

- what are the roles of rules / scores in the aesthetics?

i said that there were two kinds of rules, based on lorraine dalston (social and normative). the social rules are the ones that frame the human, and the normative are the ones that frame the computer. the role of aesthetics is to reconcile these imaginations at the bodily level

- more specifically, what is the relationship between rules and generativity?

there is a difference between rules that create a computational explosion which you understand, and rules which create a computational explosion which you do not understand. in other words, you can setup a base set of rules, through aesthetic choices (such as the metaphors of neighbors in the game of life) in order to make sense of the data output that is going to come as a result of execution

- how do you deal with the temporality and speed of the computer?

once again, the role of aesthetics is to make the unfathomable (i.e. the speed of execution of the computer). in the case of live-coding, the performance happens in the synchornization of those different flows, and the white flash of the line re-compiling is the attempt of syncing between human, music and machine

### baptiste mélès

classique: coquilles, listings, software heritage, etc.

- the ethics part seem to be more of an intuition than properly backed up
- one useful framework would have been __Jakobson's model of language__

- did i learn something about backwards metaphors? did software reveal sth about arch, lit, math?

it showed me that one can indeed inhabit a text, and live in it, rather than just linearly read it

- wouldn't it have been more obvious to say that all code poems are oulipians insofar as they are poems written in the strict context of rules?

absolutely, good point.

- the discussion of kant is quite allusive, and the word beautiful is quite improper

agreed, but i wanted to focus on the interested/disinterested, and the agreeable would have indeed been a better way to start. but ultimately, there is also no (or very few) instances of beautoful source code. the only way we can think of it is the following reasoning: we have all seen ugly code, therefore beautiful code exists.

### somaini

- mostly on LLMs: can prompt engineering be subject to aesthetics of source code?

it's too early to say but one can probably make an educated guess that, when it's more formalized as a discipline and practice, then, in the spectrum between natural language aesthetics and machine language aesthetics, it will probably fall on the machine language aesthetics since, in the end, you're always talking to a machine

- no mention of the sublime?

the sublime is that which is beyond cognition and i was interested in cognition. but it could be tied back to the idea mentioned by nick: the sublimed might also be the articulation between understanding the rules and experiencing the output

---

## what's the relationship between function and aesthetics? is it parallel / is it hierarchical?

## who are we talking about? just programmers? for whom is it beautiful? does this extend to anyone else? how widely does it transpose?

## justify the reason why the focus on code and not so much on programmers?

## what were my hypotheses? how did they turn out? (surprised? not surprised?)

## what about error? ambiguity?

## read about pragmatics discourse analysis

## read about computational definitions of aesthetics (CS data mining)

## read about winnie and queering code/aesthetic programming

## read about aesthetics of the everyday (katya mandoki)

## rapport aux LLMS?

question du style: est-ce qu'il s'agit du personnel ou du collectif? dans certains cas l'esthétique du LLM peut être appréciée puisqu'il s'agit de l'impersonnel

il peut y avoir une autre forme d'heuristique: quand on programme, on doit savoir ce qu'on veut, et il existe des représentations différentes (sketches, UML, etc.)

dans le cas des LLMs, afin de les faire générer du code, il n'y a qu'une interface: le texte, donc il faut représenter différents niveaux d'abstraction à travers le même système.

enfin, il y a une forme d'aliénation: le plaisir intrinsèque de l'écriture d'un code que l'on considère plaisant est celui que l'on a développé. il y a expérience esthétique mais localisée autre part (en soi pour la prog, fascination pour autrui pour le LLM)

une dernière piste serait alors le développement d'un language spécifique une fois que les fonctionnements internes des LLMs sont éclaircis: __c'est un medium où il y a bien moins de controle et de précision, et donc il faudrait attendre que le prompt engineering devienne une vrai ingénierie__

different kind of aesthetic, like functional programming and sql queries? but the hunch here is that it currently tends to veer towards maximalism (more description), and i'm not sure the statistical approach allows for minimalism

## pertinence d'une approche esthétique au "prompt engineering"?

question de la connaissance du matériau, et de la connaissance du problème. question de l'intention et du purpose

## quid des projets tels que rybn? jodi? mezangelle?

sorte de fantasme du code, justement comme substrat et comme impensé mezangelle en particulier propose une sorte de créolisation entre l'humain et la machine

il y a aussi ce côté de ressenti psychologique subjectif ("l'impression du code" avec ses valeurs associées) alors que l'esthétique du code va aussi tendre à devoir gérer une sorte d'exactitude de sous-systèmes... jodi c'est la représentation d'énormément de différents systèmes (rendering engines, networks, etc.)

cette esthétique est une sorte de _source code fetishism_

## qu'est-ce que le code _fait faire_ ? quelle est la poétique?

parallèle avec le béton (archi vs. affordance matérielle), ssb vs. ipfs

mais c'est aussi compliqué de pouvoir établir une corrélation entre la manière dont le code source est écrit, et les effets qu'on lui impute.

mais effectivement commencer à réfléchir à un standard esthétique du côté de l'ordi, il se peut que ca se résume à : uniformité et vitesse.

il y a un parallèle à faire avec rancière: l'esthétique donne à voir et à cacher, rend sensible ou pas -> transforme tout en vecteur.

the procedural rhetoric -> print flat, code deep

## programmation de la langue naturelle ou de la machine

Une  comparaison plus serrée entre les façons dont un texte linguistique « programme » une  interprétation humaine et les façons dont un code source « programme » une opération  machinique – ce que le programme fait faire et comment il le fait faire – sont passionnantes,  mais à peine effleurées par la réflexion.

première différence: le _quoi_ de la machine est évident, le _comment_ est plus compliqué. en language naturels, les deux s'équilibrent: le comment devient plus évident, et le quoi n'est pas toujours apparent.

notamment sur la question de la validité d'interprétations. la machine a une interprétation correcte, et l'humain peut en avoir plein d'autres. (a source code has n+1 semantics, 1 being the machine interpretation, and n being the number of humans reading the source code)

l'interprétation humaine peut être _suggérée_

## rapport entre beau, agréable, sublime, rapport entre beauté et fonction

je me suis mal exprimé, c'est justement une extension du beau, john dewey, art as experience, the sound of the train, the movement of the engine.
