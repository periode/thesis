# soutenance

1. etre concis et synthétique
2. ne pas surestimer l'audience
3. ne pas trop charger les slides (1 slide = 1 message)
4. be clear in your message, and in your transitions
5. transitions are both between slides and between parts
6. maximiser sa contribution

## questions

le jury risque de m'amener vers des questions dont ils sont experts, donc faire attention à leurs dernières publications

## todo

- be very confident, learn the script, play yourself back, aim for 150w/min -> __1700__
- [ ] a bit more code examples (on metaphorical fields of references and findings)
- [ ] add more about my hypotheses
- [ ] change style of references (check projector)
- [x] separate slide for function and ethics
- [x] pare down the complicated sentences
- [x] mentionner plus l'habitabilité
- [x] add quotes for each example of code (engineer, hackers, scientists, etc.)
- [x] put the function part as a finding rather than an implication
- [x] add references on all slides (bottom right), since it's hard to distinguish between my contributions and the ideas of others
- [x] double-down on the gap of the research (why was i doing this? why is it useful?), around slide #5
- [x] beef up the empirical approach: add numbers, tables, sample size (representative or not?)
  - [x] adjectives
  - [x] commentaries
- [x] what kind of pronouns? between 'I' or 'we', it's better to go for impersonal ('we' as in 'the proper ways academics would do it')
- [x] include the 4 frameworks (architecture, lit, math, craft) earlier
- [x] add a bit more examples of code
- [x] add quotes of programmers/tables
- [x] put the beautified/minified example at the very beginning
