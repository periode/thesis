# understanding beauty

[[outline/outline]]

## aesthetic means of understanding

[[contini_goodman_art_cognition_education]], summary of goodman and project zero

[[carroll_aesthetic_experience_revisited]]  theory (affect, axiom, content)

[[chatterjee_vartanian_neuroscience_of_aesthetics]] psychology/cognition, fluency theory

[[fauconnier_turner_conceptual_blending]]

[[goodman_the_status_of_style]], along with[[lopes_goodman_symbol_theory]]

[[goody_logic_of_writing]], goody comes here because he is dealing at the same higher level of the symbol system (orality vs. literacy), and then combine it with [[hayles_speech_writing_code]]

[[ong_orality_literacy]]

davis how to make analogies in a digital age

## understanding in literature

classical rhetoric has:
1. invention
2. disposition
3. memory
4. elocution
5. action

[reading machines](https://www.unl.edu/english/books/reading-machines)
[scripting reading motion](../readings/notes/portela_scripting_reading_motions.md)
[reading machines]

[[ricoeur_metaphor]]
[[ricoeur_metaphore_vive]]
[this is a nice summary]](https://aaaaarg.fail/upload/paul-ricoeur-metaphor-and-the-main-problem-of-hermeneutics.pdf)
the richest one: a connection to psychological theory and explicit connection to goodman [link](https://aaaaarg.fail/upload/paul-ricoeur-the-metaphorical-process-as-cognition-imagination-and-feeling.pdf)

pierre fontanier, les figures du discours

cognition, metaphor

- beardsley [[beardsley_aesthetic_experience#15 - the metaphorical twist]]

- burke and the terministic screen [[burke_language_as_symbolic_action]] and the scientistic language vs. dramatic language / wittgenstein and the formal-infomal

this section oscillate between the scientific reading/writing of a text and the dramatistic (sic) reading/writing of a text as a useful means of apprehending the dichotomy between formal and informal language plays in source code r/w. additionally, the concept of *terministic screen* (not so far from goffman's frame analysis) will help  us apprehend under which shifting social modes of examination code is being judged.

- barthes and the readerly

this section takes on barthes distinction between the readerly and the writerly text and sets it against the practice of open-source software and the tradition of craftsmanship in order to highlight that pleasure/life is being taken from reading *readable* source code. by looking at additional approaches that barthes has to writerly texts, those which leave room for interpretation, we will be able to identify similarities in source code, first in code structure (modularity), then in code text (conceptual and reader distance) and finally in meta-text, around the **comment**.

- voleshov and social aesthetics

this section concludes our framework construction by enriching it with a relational component, which, while at least implicit in previous contributions, is here expanded on a more social level. it's hard to separate an exploration of source code aesthetics while side-stepping the heightened features of the social group/s that programmers constitute. it might be possible to consider a piece of source as a "distributed *énonciation*", both towards the computer and the fellow human, but also a "delayed *énonciation*", because it isn't as immediate. and because it isn't immediate, there needs to be an adaptation of the concept of *presence* in craftsmanship; presence, a requisite in craftsmanship, is manifested through aesthetics. (`<-` that indeed tells us *why* we need aesthetics, but doesn't tell us *what* these aesthetics are. a closer reading of craftsmanship/aesthetics sources would however have more weight in light of this necessity for presence).

this could also be related: **communities of practice**, by Jean Lave and Etienne Wenger [link](https://www.learning-theories.com/communities-of-practice-lave-and-wenger.html), which imples an **ethos**. this is taken from [this post](https://queue.acm.org/detail.cfm?id=3380777)

transition avec l'architecture: conception de l'_architectural literature_ [bouchardon valeur heuristique de la littérature](../readings/notes/bouchardon_valeur_heuristique_de_la_litterature_numerique.md)

[[genette_fiction_diction]]

[[gefen_extension_du_domaine_de_la_litterature]], broader level (see also his contribution in [[lavocat_interpretation_litteraire_sciences_cognitives]])

[[lakoff_turner_more_than_cool_reason]]

[[mace_styles_critiques_de_nos_formes_de_vie]]

rousset: forme et signification

[[portela_scripting_reading_motions]]


Going back to research in contemporary literary studies can start laying out threads of an answer. Jérôme Pelletier uses Carl Plantinga to define emotional responses in the face of aesthetic objects as dual: either one has an emotional response to the artefact itself (surface), of an emotional response to what it represents (deep). In the context of reading fiction, the reader is helped in their understanding by looking out for \emph{guides} or \emph{props}\footnote{Currie, 1990 and the role of imagination}, which are similar to the \emph{beacons} emphasized by Détienne. A notable difference is that the guides are suggested, implied, left as traces for the reader to subtly construct (as in the case of the cap metaphor in \emph{Madame Bovary}), rather than explicitly stated throughout the program text (usually most obviously in the form of comments). However, we've seen previously that the use of comments is, by most programmers, not considered to be an aesthetic feature of an inspected source code, hinting at the fact that (useful) subtlety, might be a desired attribute of beautiful code.

To conclude this section, then, we turn to Jerome Bruner, who considers that art allows us to \emph{"reading in others' minds"}, to anticipate what a writer has been intending for us to understand through their text, eithe program or narrative. 

## understanding in mathematics

enlightenment (i get a revelation when i understand)

beauty is an attribute of truth

lakoff & Nunez: embodied mathematics

[[rota_phenomenology_mathematical_beauty]]

this section explores beauty from the angle of formal logic, `TODO`. the conclusions could either be formal (like when cramer talks about the symmetrical, repetitive patterns of middle-age monks, or commentaries of mathematicians on `e^(i*PI) + 1 = 0`), or conceptual. if they're conceptual, they don't need to come in to the framework right now.

http://emis.matem.unam.mx/journals/NNJ/conferences/N2004-Diaz.html - beauty in math and arch

beauty in math: https://en.wikipedia.org/wiki/A_Mathematician%27s_Apology

Abstraction in programming is the process of identifying common patterns that have systematic variations; an abstraction represents the common pattern and provides a means for specifying which variation to use. An abstraction facilitates separation of concerns: The implementor of an abstraction can ignore the exact uses or instances of the abstraction, and the user of the abstraction can forget the details of the implementation of the abstraction, so long as the implementation fulfills its intention or specification. (Balzer et al. 1989)

détour par le style de gilles gaston-granger [[granger_essai_philosophie_style]]

## understanding in architecture

habitability (building is clear to move around)

landscape cognition, with kevin lynch stating that landscape legibility is "the ease with which each parts can be recognized and organized into a coherent pattern". Weisman (1981) adds to that the legibiliy of an environment "facilitates the process of wayfinding"

hart and moore (1973) define spatial cognition as "the knowledge and internal or cognitive repsresentations of the structure, entitites, and relations of space (in thoughts)"

this perception is a process of interpretation modulated by emotional responses to sites, perceived meanings, and physiological reactions

from [Landscape Design and Cognitive Psychology](https://www.sciencedirect.com/science/article/pii/S1877042813013293)

also berléant & carlson seem to have written quite a lot on [urban aesthetics](https://philpapers.org/rec/BERTAO-15). for instance, carlson offers the method of "functional fit" as an alternative, which treats the city as a system that must be assessed on the efficiency of its different components to work together, and there the appreviation of human environments is based on "the functions they perform". All elements should grow organically (like alexander said)

sullivan (building is clear function) / 

alexander vs. eisenman

alexander = softdev, habitable vs. eisenman = hack, subversion

design thinking book: The concept of good in architecture is one which says, among other things, that all architecture must engage its audience. It must foster understanding and be intelligible. This is clearly not the only concept necessarily at work, but it is one that would have broad adherence, across positions.

**Compression** is the characteristic of a piece of text that the meaning of any part of it is “larger” than that particular piece has by itself. This characteristic is created by a rich context, with each part of the text drawing on that context—each word draws part of its meaning from its surroundings.
**Piecemeal growth** is the process of design and implementation in which software is embellished, modified, reduced, enlarged, and improved through a process of repair rather than of replacement

[[coburn_vartanian_neuroscience_of_architectural_experience]] cognition-wise there's not a lot of research yet

[[downton_knowledge_architecture_science]], about bottom up knowledge, rather than strictly top down

[[gabriel_alexander_search_beauty]]

lacaton & vassal: establishment of new aesthetics based on new needs

industrial architecture as a parallel with professionalization software developers?

__conclusion__

[[gandesha_aesthetic_dignity_of_words]], praxis in language as a means of knowing, based on adorno's philosophy
[[wilken_card_index]], information architecture