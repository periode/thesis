# outline

## introduction

see [tex file](../redaction/introduction.tex)

## 1. [aesthetic ideals](./ideals.md)

we start our journey here. basically, we look at what programmers say when they talk about beautiful code.

we see there are different kinds of lexical fields of beauty, also by including examples, which might be devoid of context for now

still, we see that there are specific domains that are being referred to: science, architecture, literature

but we also see that all these kinds might relate to understanding. so we turn to this next

## 2. [understanding code](./understanding_code.md)

this section is focused on the specificities of underdstanding code

### 2.1 understanding

first we define understanding as trhe construction of mental models in a multidimensional conception of cognition

references, between objective and subjective:
- simon penny and representation -> mind-body duality
- lakoff & turner -> mental models
- gregory chaitin -> data compression and simplicity of rules
- wittgenstein (formal, absolute understanding vs. felt, contextual understanding)

### 2.2 understanding computers

so what's up with understanding? turns out code is tricky to understand because it has to talk to machines

#### 2.2.1 the symbol machine

computers are mechanical devices dealing with meaning

#### 2.2.2 the psychology of programming

how it can be an intimate affair between human and machine

### 2.3 understanding humans through computers

#### 2.3.1 the metaphors of code

how metaphors help us to deal with the idiosyncracies of computation

### 2.3.2 implementation and communication

the different stages in developing algorithm and actually writing it

the issue that the reader needs to understand both abstraction and implementation (ideally)

the different means to ease cognitive friction:

- documentation
- comments
- literate programming
- programming

and so we conclude that the essential interface to understanding humans and machines within a context of computation are programming languages.

## 3. [programming languages](./programming_languages.md)

we see how programming languages deal with understanding between computers and humans, and how they have to provide an interface to deal with _meaning_.

> A theorem, however, is (see above) only useful if we can apply it under a minimum number of clear conditions. In the same way the usefulness of a subroutine (or, in a language, a grammatical instruction) increases as the chance decreases, that it will be used incorrectly. From this point of view we should aim at a programming language consisting of a small number of concepts, the more general the better, the more systematic the better, in short: the more elegant the better [src](https://www.cs.utexas.edu/users/EWD/transcriptions/MCreps/MR34.html)

### 3.1 what are programming languages?

### 3.2 how is meaning expressed through programming languages?

### 3.3 the interface between abstract and concrete, ideal and material

we also look into language-dependent features, and language-independent features.

so we conclude on language as a material to embody a theory of semantico-spatial cognition

## 4. beauty and understandings

### 4.1 [the role of beauty in understanding](./understanding_beauty.md)

here, we look at how literature, architecture and maths deal with beauty and understanding

now that we have stronger concepts, let's dive back into code

#### 4.1.1 in literature

#### 4.1.2 in architecture

#### 4.1.3 in sciences

### 4.2 [aesthetic manifestations in source code](./concrete.md)

here is where we deploy our theory.

(see checkin #2/3)

### 4.3 case studies

#### 4.3.1 UNIX v7

#### 4.3.2 TeX

#### 4.3.3 Code Poems

## conclusion

the aesthetics of code is the symbolic progression from word to structure to idea, with each of these configurations happening at different moments and different levels of expertise in programmers, and assigning different roles to the lexical tokens visible on the screen.

and then poetics as an opening