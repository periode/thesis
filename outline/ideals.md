# aesthetic ideals

## intro

the purpose of this specific chapter is to set the stage, and answer the following questions:

- how are programmers? is it a coherent whole or are they separate in several communities/practices? how do i define a community of practice?
- then, in a second time, what kinds of beauty do programmers aspire to? that involves a certain kind of high level lit review, of the words that come back here and again
- finally, i show that these ideals of beauty are more intimately related to craft, and modes of knowledge

[[outline/outline]]

[[kintsch_von_dijk_strategies_understanding]]

[[kitsch_van_dijk_towards_model_text_comprehension]]

## the practice of programmers

this section focuses on the applied practice of describing things to the computer. who does it, how they do it, and how they relate to it. it would particularly focus on the historical/sociological aspect of it, to start highlighting that there might be one concept of computing, but the reality of interacting with and thinking in terms of computation is not unidimensional. it establishes different categories of people writing source code: researchers, academics (teachers+students), professionals, hackers, amateurs and artists.

Engelbart’s NLS expanded the view of programmers from business analysts and artiﬁcial intelligence researchers to any information worker. Smalltalk originally focused on children as programmers. Hypercard was developed and distributed at Bill Atkinson’s insistence that “end users” need programming capabilities. (from [this](https://dl.acm.org/doi/epdf/10.1145/2384592.2384594)). And then they mention diy hacker and maker culture, beginner friendly arduino, max for live and youtube studio (__I must define which ones I include, which ones I exclude, and why__ maybe the definition I want is one where there is a clear definition between programmer and end user?)

---

a short history of programmers:

> The legacy of an earlier tradition of celebratory accounts of “great men,” pioneering machines, and important “firsts” is still very much with us, particularly in popular and journalistic histories. (Nathan Esmenger 2007, Power to the People, Toward a Social History of Computing)

also mention that there is a parallel history of soviet computing (james cortada article) but the domination of the US renders this moot (argue more specifically why)

essentially there is academia/government

then business (IBM, Fortran, personal calculators, visicalc, lotus 123)

then hobbyists (basic)

then academics (history of research, to some extent -> could it be said that universities are a bit less prone to fundamental research today now that they need to do marketable stuff? is this even relevant?)

there might also be a history of artists? check references from software art class

---

programming links technology and mathematics in a very specific way

ref: lammers, programmers at work

[[petre_blackwell_programmers_mental_imagery]]

I argue that aesthetics can help grasp software's multitude.

There's also this blog post about all the voices in source code. All the programmers that have given insight, all the copy paste from SO, etc.

see black, p. 101 for a reference to software management literature

[[hayes_cultures_od_code]]

[[cox_mclean_coding_praxis]]

### software developers

[[knuth_art_of_programming_vol_1]]

[[martin_clean_code]]

[[mcbreen_software_craftsmanship]]

### hackers

the underhanded c code contest

[[graham_hackers_and_painters]]

[[mateas_montfort_box_darkly]]

### scientists

### artists

[[paloque_berges_poetique_des_codes]]

## ideals of beauty

this section extracts the features that are recurring in the discourses around beauty in source code. it does so by looking at how practical examples and theoretical statements either converge or diverge and how such statements are modulated by the aforeidentified communities. the common point identified, via the subjectivity of writing code, is the concept of the *craft*. 

beauty as a lack of ambiguity, as efficient achievement of an aim (aim of frustration/clarity/imagination)

- beardsley: cognitive gratification under ideal circumstances

### lexical field in programmer discourse

- minimalism
	- https://vimeo.com/47364930 -> concise code is code as literature, because he says one of the issues is that there are just too many lines of code that one can wrap its head around. so there's a need for shrinking down content
- simplicity
	- Syntactic simplicity, or elegance, measures the number and conciseness of the theory's basic principles. Ontological simplicity, or parsimony, measures the number of kinds of entities postulated by the theory. code is syntactic simplicity because wrngles together complex concepts (e.g. perl, one liners), or code is ontological simplicity, because all is within computation (e.g. lisp)

[[chandra_geek_sublime]]

[[cramer_words_made_flesh]]

[[mitchell_art_of_computer_graphics_programming]]

### functional beauty

this first approach, by comparing both source and comment at the same time (taking texts which are explicitly described as being beautiful), explicitly highlights the requirements for source code to be beautiful. 

[[pugh_beauty_simplicity]]

[[pineiro_aesthetics_of_code]]

[[cox_the_aesthetics_of_generative_code]]

[[mattt_as_we_may_code]] -> this is somewhat almost architecture, brings a hint

[[mclennan_aesthetics_software_engineering]]

[[pressman_software_engineering_practicioners_approach]]

### literary beauty

this second approach contrasts with the functional component of the first one, but nonetheless stands in relationship with it. the creative beauty, by defying traditional beauty standards, does help us highlight, through deviance, what the norm is. these texts on "creative beauty" include the classical perl poetry, code poems, IOCC, code poetry contest, etc.

[[black_art_of_code]]
[[raley_code_surface_code_depth]]

[[sondheim_codework]]
[[ward_authorship_of_generative_code]]
[[knuth_literate_programming]]

### mathematical beauty

[[mclennan_elegance_software_design]]

beautiful proofs in geometry?

Elegance: Through a single lens, it communicates the problem it solves and the machinery of its solution.

[[fuller_software_elegance]]
[[spinellis_reading_writing_code]]
[[iverson_notation_as_tool_for_thought]]

## craft and beauty

this subsection also allow us to introduce the concept of *craftsmanship* and integrate it within a larger tradition of sennett/de certeau/rancière, and connect the practice of programmer to a longer history of craft, a history which in itself has beauty standards `TODO`

https://dannorth.net/2011/01/11/programming-is-not-a-craft/

[[sennett_the_craftsman]]

the zen of python could tie in to that tradition

[this](http://www.csharplearningsquare.com/2017/07/eight-golden-rules-for-better.html)  includes practical tips and software craftsmanship

"to grok" -> an expression which means "to grasp vaguely, to have an intuitive understanding of"

[[jones_reckoning_with_matter]]

## modes of knowledge

this section focuses on "you will know when you see it"

Implementation as: **THIS GOES WITH GOODMAN**

- individuation (from homo sapiens to delia derbyshire)
- instantiation (from turing machine to macbook)
- exemplification (from red to a specific acrylic)
- reduction (from complex weather model to weather forecast)

__explicit__

__tacit__

- koans
- alan perlis on programming

practice is *synthetic method*, a method which regroups, which puts together.

[[freeman_science_as_a_craft]]

[[mills_intellectual_craftsmanship]]

craft might also start to connect to the _aesthetics of the everyday_. particularly, thomas leddy has an article called "Everyday surface aesthetic qualities: neat, messy, clean, dirty" that might be very good