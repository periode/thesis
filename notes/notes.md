# notes

## admin

rendre la thèse à la sorbonne tout début septembre (checker avec secrétariat)

potentiels membres du jury:

1. antono somaini
2. alexandra saemmer
3. baptiste mélès
4. dominique cardon
5. yves citton
7. nick montfort
8. alexandre gefen


- camille paloque- bergès (n'a pas de HDR)

- pascal mougin
- nick montfort
- alexandre gefen


demander au secrétariat des thèses:
- règles de composition du jury (au vu de la codirection)
- combien de personnes? paris-3 ou ed120?
- calendrier pour soumettre

checker les membres de la 71e section Infocom

possible dates:
- semaine du 6 novembre
- semaine du 27 novembre
- semaine du 11 décembre (possibly for nick on december 15th or ideally )


## tex

`make4ht -x thesis.tex  "fn-in"` for html export (one should be able to add bibliography with [this](https://tex.stackexchange.com/questions/358565/can-htlatex-be-used-with-biblatex-these-days))
http://www.softpanorama.org/People/Knuth/taocp.shtml

## dump

dont forget to do architecture and pavillions: \footnote{In architecture, such technical and artistic feat for its own sake, devoid of any reliable social use, is manifested as pavillions.}

france culture cours du collège de france  de francois chatelet: la condition necessaire du savoir c'est la sensibilité (tout se donne dans la spatio-temporalité)

1. passivité informatrice
2. entendement actif
3. raison, qui remonte jusqu'a la cause originelle, le principe

[computer architecture as a mix between text, building, and math](https://www.youtube.com/watch?v=kFT54hO1X8M)

three theories for computer science: [acm](https://dl.acm.org/doi/10.1007/s11023-007-9060-8), the conceptual object one could be interesting/promising

> flow state needs an immediate feedback in order to be achieved: literary writing does not have that, but computer writing, with IDEs, linters, etc. does have immediate feedback (with execution speed, so i guess it changes over the years as well) (clemens)

from thought to practice: conceptual art aims at communicating concepts, ideas. experimental art don't know what they aim at but are motivated by an aesthetic criteria. (richard gabriel, keynote science is not enough) maybe this connects to the tension between having to be understood by humans and understood by computers? [[table_of_contents#understanding code]]

the distance calculation in the interpreting step of programming language compilation to resolve a variable (i.e. scoping) and the fact that one uses shorter variable names based on inner scope, is such example of a connection between literary and architecture

minimalism is not the same thing as simplicity and clarity

> There are two well-known sides to programming: (1)  mastery of the domain; and (2) mastery of the formalisms by  which intentions become executable by computers (we call  these programming languages). Crista Lopes in [this panel](https://dl.acm.org/doi/10.1145/2814189.2818719)

## rewrite

~~should i also specify in the intro that i'm dealing with aesthetics and not art? isn't that obvious enough already?~~

- include a fuller discussion of both rhetorical code studies (brock) and critical code studies (marino)

## to read

- history of software development:
	- https://aaaaarg.fail/upload/martin-campbellkelly-from-airline-reservations-to-sonic-the-hedgehog-a-history-of-the-software-industry-1.pdf
	- https://doc.lagout.org/science/0_Computer%20Science/0_Computer%20History/A%20History%20of%20Modern%20Computing,%202nd.pdf
- chap 2 of sicp
- [sketches of thought](https://mitpress.mit.edu/books/sketches-thought), for the understanding code part

- rousset: forme et signification
> L’art réside dans cette solidarité d’un univers mental et d’une construction sensible, d’une vision et d’une forme. Saisir des significations à travers des formes.

[insightuful code](https://www.hillelwayne.com/post/cleverness/)

[heisenberg - the meaning of beauty in the exact sciences](https://inters.org/heisenberg-beauty-natural-science)

[the programmer's brain (preview)](https://www.manning.com/books/the-programmers-brain) -> However, your brain does a lot more while you are reading the BASIC program. You are mentally trying to execute the code, to understand what is happening. That process is called **tracing**—the mental compiling and executing of code, and differentiates one from novice or expert

[software design decoded](https://www.amazon.com/Software-Design-Decoded-Experts-Think/dp/0262035189)

[semiotics of programming](https://dl.acm.org/doi/book/10.5555/1805903)

[scripting culture](https://ebookcentral-proquest-com.proxy.library.nyu.edu/lib/nyulibrary-ebooks/detail.action?docID=697611) architectural design and programming
