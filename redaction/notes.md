# redaction notes

## general

write from the perspective of the reader (we don't want to hear about the research process, we want to hear about the important things), but also keep in mind that the reader will have *different expectations than me*. in general, the first draft is for the writer and then progresses further to end up on a final draft made for the reader.

express the significance of the problem, the rationale for the thesis. we don't know

find a way to express the big picture/problem without having all of the full context

breaking things down in parts forces us to treat all of them equally

every part of the thesis is then, to some extent, a re-iteration of those three things: context (e.g. literature review), problem and response

**metadiscourse**: the linguistics strategies deployed to manage the evolving relationship between writer, reader and text

**motivation**: i should write an informal document explaining why i'm down for this research

## introduction

### quick trip

basically an introduction to the introduction (abstract, with bit of context, bit of problem, bit of response)

the big picture

### context

establish a research territory:
what the audience needs to know in order to understand the problem you are going to confront. background material which should be familiar to the target audience; a refresher or primer on the topic—e.g. "over the past two decades, research in the field has focused on..."

in my case, doesn't have to be too extensive because the literature review will come later. but i should at least mention **the best in the field**, the most important references.

this is where the **key definitions** happen

and the **boundaries**

once i've established that, I can then move on to the gaps, which are going to lead naturally to my restatement of the problem and proposed solution. or maybe the gaps could already be in there

### restatement of the problem

this doubles down on establishing the specific *focus* of the thesis

establishing a niche: what isn't yet well understood, what i would like to understand/reveal/explain/explore/reinterpret/contest) and why it will matter to have done so. eg. "[topic] is still poorly understood/misinterpreted. knowing more about this topic will be helpful because of XXX

double-down on **establishing significance**: the significance is the requisite for the problem

### response

occupying the niche: in order to adress this problem, i will...

elaborate on the response

### roadmap

how the thesis will proceed

#### the aesthetics of source code: idealistic. what are they? what are people saying about it? going through other aesthetics (lit, arch, craft, maths).

- - the practice of programmers: how they play with understanding: software developers, artists, hackers, scientists (data + academia).
- - establish ideals. see how those ideals relate to other domains (e.g. clarity in [...ideals])
- - establish how these ideals relate to the broader relationship to understanding and the modes of knowledge we've seen before
- - CONCLUSION: people do a lot of different things with code. dominant aesthetic vs. dominated aesthetic, glitch etc.. can we see a relation between those ideals and what source code is made of? for that we turn to what source code is made of

#### the stakes of source code as understandable text: why is it so hard? how is it affected by external factors? do aesthetics have anything to do with it (literary and tacit)?

- - the issue with source code as understanding (human and machine, software domain and problem domain). surface-structure vs. deep-structure. goals of source code
- - issues of understanding in text: sophism, rhetoric, literature and cognition (metaphor)
- - other kinds of understanding: tacit knowledge, habitability
- - CONCLUSION: understanding is key, and it happens through writing nice code

#### the aesthetics of source code: practical. what is source code made of? structure, syntax, vocabulary. how does this relate to those ideals?

- - empirical study, review of what people say across communities. establish typology of tokens
- - see how the tokens can relate to those ideals (structure, syntax, vocabulary)
- - case studies
- - CONCLUSION: aesthetic tokens exist at different levels, and under different modalities. so it's still very different, polymorphous. and yet, there is still one thing in common: programming languages.

so far we've seen one context: use and intent of programmers. there is another context: tools. ("choosing the best tool for the job")

#### programming languages, programming matters. why it is necessary to take languages into account.

- - machine communication (ast)
- - human communication (esolang)
- - the hybridity of it all: spatial, kinetic instructions, thought-stuff
- - variability and stability: what is the same across codebases, what changes. it's always a tradeoff
- - CONCLUSION: PLs allow for a single way to hold these complexities (problem domain, software domain) together. cf. simondon

#### conclusion

the aesthetics of code as a dynamic, kinetic one (what are we dealing with = lit? then what do they do = arch?). craft and math as modulators. expressions and statements. code as layers, with the nature changing as we get deeper into the machine. the paradox of thought-stuff. language as formal, mechanical exploration

### wrap up

intended readership

connect back to the wider world

clarify between source code and program text