\documentclass{report}

\usepackage{fontspec}
\usepackage{newunicodechar}
\usepackage{graphicx}
\graphicspath{{./images/}}
\usepackage[export]{adjustbox}
\usepackage{listings}
\usepackage{epigraph}
\usepackage{authblk}
\usepackage[normalem]{ulem}
\usepackage[bottom]{footmisc}

\usepackage{float}
\usepackage[leqno]{amsmath}
\usepackage[natbibapa]{apacite} 
\usepackage[]{natbib}
\setlength{\bibsep}{10pt}

\usepackage{xurl}
\usepackage{hyperref}
\usepackage{hyphenat}

\usepackage[style=iso]{datetime2}

\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\itshape}

\usepackage[]{minted}
\setminted{frame=single,fontsize=\footnotesize,baselinestretch=1,breaklines=true,breakanywhere}
\usemintedstyle{tango}
\usepackage{caption}
\newenvironment{code}{\captionsetup{type=listing}}{}
\newcommand{\spacer}{\vspace{1\baselineskip}\centerline{\rule{0.13334\linewidth}{.4pt}}\vspace{1\baselineskip}}
\newcommand{\spacersmall}{\vspace{1\baselineskip}}
\providecommand*{\listingautorefname}{Listing}

\defaultfontfeatures{Mapping=tex-text,Scale=1.00}
\setmainfont{Bespoke Serif}
\setmonofont[UprightFont =*,ItalicFont =*,BoldFont=*,BoldItalicFont=*]{MesloLGS NF}
\newfontfamily\apl{APL385 Unicode}
\newfontfamily\noto{Noto Serif}
\newfontfamily\notocjk{Noto Sans CJK JP}
\newunicodechar{⌿}{{\apl ⌿}}
\newunicodechar{τ}{{\noto τ}}
\newunicodechar{ε}{{\noto ε}}
\newunicodechar{χ}{{\noto χ}}
\newunicodechar{ν}{{\noto ν}}
\newunicodechar{η}{{\noto η}}
\newunicodechar{ϕ}{{\noto ϕ}}
\linespread{1.50}
\sloppy

\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true,frame=single,captionpos=b,belowcaptionskip=2em,aboveskip=2em}

\begin{document}
\title{The role of aesthetics   in understanding source code}
\author{Pierre Depaz\linebreak\linebreak under the direction of\linebreak Alexandre Gefen (Paris-3)\linebreak and Nick Montfort (MIT)}
\affil{Université Sorbonne Nouvelle}
\affil{ED120 - THALIM}
\affil{Comparative Literature Doctorate}
% \date{Last updated: \today}
\date{}
\maketitle

\shipout\null

\renewcommand{\abstractname}{Summary}
\begin{abstract}
    This thesis investigates how the aesthetic properties of source code enable the representation of programmed semantic spaces, in relation with the function and understanding of computer processes. By examining program texts and the discourses around it, we highlight how source code aesthetics are both dependent on the context in which they are written, and contingent to other literary, architectural, and mathematical aesthetics, varying along different scales of reading. Particularly, we show how the aesthetic properties of source code manifest expressive power due to their existence as a dynamic, functional, and shared computational interface to the world, through formal organizations which facilitate semantic compression and spatial exploration.
    \linebreak
    \linebreak
    \textbf{Keywords}: Aesthetics, Source Code, Programming, Cognition, Epistemology
\end{abstract}

\renewcommand{\abstractname}{Résumé}
\begin{abstract}
    Cette thèse examine comment les propriétés esthétiques du code source permettent la représentation d'espaces sémantiques programmés, et leur implication dans la compréhension de la fonction de processus computationels. Se basant sur un corpus de programmes-textes et des discours les accompagnant, nous montrons en quoi l'esthétique du code source est contingente d'autres domaines esthétiques littéraires, architecturaux et mathématiques, tout en demeurant dépendante du contexte au sein duquel circulent les programmes-textes, et se transformant à différentes échelles de lecture. En particulier, nous montrons que les propriétés esthétiques du code source permettent une certaine expressivité, en vertu de leur appartenance à une interface linguistique partagée et dynamique permettant de calculer le monde. Enfin, nous montrons comment une telle interface, organisée formellement, favorise la compression sémantique et l'exploration spatiale.
    \linebreak
    \linebreak
    \textbf{Mots-clés}: Esthétique, Code source, Programmation, Cognition, Épistémologie
\end{abstract}

\renewcommand{\abstractname}{Zusammenfassung}
\begin{abstract}
    Diese Arbeit untersucht, wie die ästhetischen Eigenschaften von Quellcode die Repräsentation von programmierten semantischen Räumen ermöglichen, im Zussammenhang mit der Funktion und dem Verständnis von Computerprozessen. Durch die Untersuchung von Programmtexten und den sie umgebenden Diskursen wird aufgezeigt, wie die Ästhetik von Quellcode sowohl von dem Kontext abhängt, in dem sie geschrieben wird, als auch mit anderen literarischen, architektonischen und mathematischen Ästhetiken kontingent ist und sich auf verschiedenen Leseebenen unterscheidet. Insbesondere wird gezeigt, wie die ästhetischen Eigenschaften von Quellcode ihre Ausdruckskraft dadurch manifestieren, dass sie eine dynamische, funktionale und gemeinsam genutzte Computerschnittstelle zur Welt darstellen, und zwar durch formale Organisationen, die semantische Kompression und räumliche Erkundung erleichtern.
    \linebreak
    \linebreak
    \textbf{Stichtwörter}: Ästhetik, Quellcode, Programmierung, Kognition, Erkenntnistheorie
\end{abstract}

\shipout\null

\include{acknowledgments.tex}

\shipout\null

\include{foreword.tex}

\shipout\null

\tableofcontents

\pagebreak

\shipout\null

\renewcommand{\textflush}{flushright}
\setlength{\epigraphwidth}{0.5\textwidth}
\epigraph{To me, programming is more than an important practical art.\linebreak \linebreak It is also a gigantic undertaking in the foundations of knowledge.}{Grace Hopper}

\pagebreak

\shipout\null

\include{introduction.tex}
\shipout\null
\include{ideals.tex}
\include{understanding.tex}
\shipout\null
\include{beauty.tex}
\shipout\null
\include{programming.tex}
\shipout\null
\include{conclusion.tex}

\bibliographystyle{apacite}
\raggedright
\urlstyle{same}
\bibliography{thesis.bib}
\addcontentsline{toc}{section}{Bibliography}

\listoflistings
\addcontentsline{toc}{section}{Listings}

\listoffigures
\addcontentsline{toc}{section}{Figures}

\pagebreak

\include{closing.tex}

\end{document}
