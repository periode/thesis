# agathe keller

## sanskrit et algorithmes

---

[[table_of_contents#understanding in literature]]

il y a deux adjectifs qui qualifient ces vers:

- leger, clair, concis
- lourd, large

question de la compacité (a quel point quelque chose est compact) et généralité.

les auteurs jouaient aussi avec les mots afin de diriger vers la solution

dans les sutras, il y a le sutra de base (la procédure) et puis le commentaire (herméneutique) qui permet de dire *à quoi* s'applique ce procédé