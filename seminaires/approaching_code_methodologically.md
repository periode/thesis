# approaching code - workshop - 02.07 - 03.07

role of representation?

stuart hall, the role of representation: https://www.sagepub.com/sites/default/files/upm-binaries/55352_Hall_ch_1.pdf

dangers! metaphors at work: https://www.exeter.ac.uk/media/universityofexeter/internationalexeter/documents/iss/Wyatt_danger-metaphors_%283%29.pdf

mckenzie: performativity: https://www.lancaster.ac.uk/staff/mackenza/papers/code_performativity.pdf