# Alberto Naibo - Algorithmes au prisme de la géométrie

## CulturIA

---


part de la différence entre "effectivement calculable" et "algorithmique"

les algorithmes glissent toujours entre deux plans:
- fonction abstraite (qui n'a pas d'impact sur/dans le monde)
- fonction en tant qu'object (LISP, JS)

et un algorithme pose toujours la question de l'interprétation: qui doit être faite par un agent humain. il fait aussi un parallèle entre démonstration mathématique et algorithme -> aussi parce que cette interprétation est nécéssaire, et parce que c'est une classe de problèmes similaires.

d'ailleurs, l'IA moderne (RNN, ML, etc.) ne performe pas bien du tout sur des démonstrations mathématiques.