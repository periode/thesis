# jdzb cultural AI


## julien pierre - nantes

-> they made a __bestiary__ of AI, with a lot of typologies, criteria, etc. which is quite interesting


## futres & fictions panel

designability of agents as social catalyst -> ai is less and less of a metaphor

4 categories from their typology
- machine
- infrastructure
- buddy
- human

design as materialized narratives

interesting people: [simone shu-yeng chung](https://cde.nus.edu.sg/arch/staffs/simone-shu-yeng-chung-dr/)

- rachel hill: AI poetics
- angelica cabrere torecilla: brain-computer interfaces, a critical study from a fictional text
- elena knox: waseda university
