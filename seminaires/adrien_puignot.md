# forgot the name

## adrien puignot, cnrs

séminaire adrien puignot: principe de la _sémiotique sociale_, i.e. la fabrique de sens globale.

_architexte_: L’ensemble des catégories générales, ou transcendantes -types de discours, modes d’énonciation, genres littéraires, etc.- dont relève chaque texte singulier (genette). En gros, la méta-catégorie des catégories. Ce qui gouverne le texte. L'outil d'écriture n'est pas neutre, il impose des formes éditoriales et des formats d'écriture au scripteur. Il permet aussi la recomposition des "traces" de lecture (échange, co-construction, démantèlement)

_infra-ordinaire_: infra-ordinaire en tant qu’observation attentive de « ce qui se passe chaque jour et revient chaque jour, le banal, le quotidien, l’évident, le commun, l’ordinaire, l’infra-ordinaire, le bruit de fond, l’habituel. l’attention portée aux détails du réel apparaît comme une constante qui finira par devenir une théorie rassemblant la pensée et la pratique littéraires de l’auteur