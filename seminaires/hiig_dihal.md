# cultural representations of AI

## kanta dihal

the californian feedback loop is the compound of hollywood, silicon valley, academia and tropes by mixing the fictional references to the real states (the example she takes is transcendance (2014), with cameos by elon musk and morgan freeman, and used by stephen hawking for AI policy)

the whiteness of AI

and then the developments of soviet russia, soviet china, maoist china, then post-maoist china, and then singapore (tech-utopia-death), and then japan (with an intense aspect of animism)

jp: they claim animism was re-invented as a cultural model during the robot boom, because the more general tech boom was deliberately about the differentiation from the west

south korea: shocked by alpha go (go, but also the fact that the computer was done not in south korea), afraid of falling behind

## decolonizing ai

- absence, resistance, re-imagining

egypt: nothing has happened ai fiction wise, so we're starting from scratch (e.g. djinns)

brazil: strong resistance

indigenous perspectives: afro-futurism



are AI narratives really the only ones? is there multiplicity, or is there a multiple number of perspectives given one particular AI?

AI Narratives __start__ with antropomorphism


## tradition

question raised: the mind-body-object tradition as it exists in india and japan. in india they also delve in post-colonialism
