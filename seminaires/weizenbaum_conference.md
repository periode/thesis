# weizenbaum conference - practicing sovereignty

## [link](https://www.weizenbaum-conference.de)

there is no digital sovereignty w/o sovereignty (and it boils down to hardware and supply chains)

### renata avila pinto - open knowledge as a design principle for a sovereign tech future

she actually makes the parallel between shitty gafam internet and building/housing (the building is about to collapse and the users have to wear clunky PPE while the landlords are not footing the bill)

the community aspect: inclusion in broader terms (different forms of knowledge included)
the governance aspect on common assets was completely neglected: example of how they put all face pictures on creative commons, and they were subsequently used for AI training for unrelated purposes.
the technology aspect

federated forms of knowledge: collaboration with opensyllabus!

possible digital future:
- open
- local/federated
- sustainable
- feminist
- generative

### claude draude - reconfiguring power relations in socio-technical systems design

"socially-desirable systems design"

computing artifacts must comply with the formal essence of computing, but are also situated. the point is to give those equal weights in the design process.

__diffraction__: rethink differences beyond essentializing binaries

example: UN Women Campaign Google Autocomplete

an important part is "participation": who has a say, who benefits, and how?

participating:
- in IT design
- in the worlds through IT
- involuntary

bias happens through:
- data
- algorithms and models
- use
- application domain

design approaches:
- norms & values
- participation
- what are the vulnerable groups of a given project (i.e. cosyl)

algorithmic systems are made of:
- people -> mapping all shareholders
- place -> reproduction of structural inequalities?
- power -> how emancipating is the system?
- participation -> when is participation happening/not happening?

making different __agential cuts__, beyond representation and towards performativity

references:
- [gerd links softdev with intersectionality](https://www.gerd-model.com/)
- [paper for diversity in hci](https://link.springer.com/chapter/10.1007/978-3-030-78092-0_3)
- [new materialist researches](https://www.uni-kassel.de/forschung/en/iteg/veranstaltungen/nmi-2021)

### Ariel Guersenzvaig: - health and AI

is health a value-free notion? is it purely descriptive? is it just statistical normality?

ref: sorting things out, bowker and starr

defining things like health, education, security, is a __dialogical approach__ between what something is, and what something should be. and a conception allows us to offer __justifying reasons__ over a value-judgment, in order to retain epistemic agency

### adam greenfield - keynote day 2

paroles, paroles, parolesA

### hiig - scholar plus one

govern inclusively:
- community-building and community-bridging
- mutual relaiance & care
- other forms of commoning

scale small:
- horizontal support structures
- multi-stakeholder ecologies
- cooperation instead of competition

finance creatively:
- resilience instead of sustainability
- diversifying income streeams
- common infrastructures

Funding:
- long-term and robust funding and subsidies as part of strategy
- funding structures by the community
- extending funding to margins (small journals, etc)

coaching:
- peer-to-peer consulting
- streamlining workflows to increase efficiency (not getting bbogged down by software/administration)
- analyzing business models and benchmarking

networking:
- representing the interests of scholar-led publishers
- creating awareness for the labor of editors
- regular networking events

### open hardware

what is open hardware? it is good documentation:

- bill of materials
- source files
- assembly instructions
- licence and standards

### Open Research Knowledge Graph

__digitalize knowledge, not documents!__

it's interesting how they focus on machine- and human-interpretation of content, with a crowd-based approach


Linked scholarly research:
- ORCID
- Dimensions.ai
- OpenAir
