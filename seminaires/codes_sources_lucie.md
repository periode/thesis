# categories techniques et représentations dans l'intelligence artificielle: de l'invention à l'usage

## lucie conjard

---

### contexte théorique

diade entre terreur et fascination

dimension eschatologique

présence remarquable dans la sphère publique

si un mythe s'accroche à cette technique en particulier, il faut s'attacher à savoir pourquoi

il y a des manifestations spécifiques à l'IA qui sont surtout là pour faire du style et de la comm

dans la matérialité d'une écriture, on peut voir les catégories centrales d'une société (pas sûr...)

que signifie la manière dont l'IA est développée, pour la société qui la développe? -> réaliser le __sens social__ de ces dispositifs de création de l'IA

### terrain en trois parties

- inventeurs (chercheurs), concepteurs (développeurs), utilisateurs (managers en domaine industriel)
