# culturia - sébastien plutniak

## épistémologie de l'archéologie - IA et histoire de l'IA

jean-claude garding: travaux entre archéologie et informatique dans leurs aspects cognitifs.

david l. clarke - analytic archeology - livre sur le tournant archéologique et de se "scientificisation"

"concepts": inféodé à un critère d'efficacité de productions nouvelles, inféodé aux objets, à ses propriétés.

SNARK - Symbolic _____ 

que ce soit à travers des graphes ou des arborescences, il s'agit de simuler le savoir de l'historien

représenter le raisonnement: le service du méthodologue __vs.__ simuler le raisonnement: l'effacement du méthodologue

"entre modèle et récit, les flottements de la troisième voie"