# computer generated narrative's wild ride since the 60s

## sorbonne, 19.12.23

---

seminar based on the upcoming publication: _output: an anthology of computer generated narrative_

PAULINE, PROTEUS, etc. all systems which were research-based, peer-reviewed, therefore findable and interesting prototypes

stratchey's love letters were also about making fun of cis-gendered love, already implying people might steal jobs

the first character in computer generated narrative is ELIZA

stories are narratives that have _a point_, particularly in a given cultural framework (do you need intent in order to make a point?)

DAYDREAMER is another system that is currently on GitHub (based on psychological research on daydreaming). TAILOR, by TONY SMITH was research on planning systems. GRANDMOTHER, by Imogen Casebourne, was based on story grammars.

There was also some sort of speech generator from Poland
